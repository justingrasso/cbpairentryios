//
//  ViewController.swift
//  CBPAirEntry
//
//  Created by Kannan Seenivasagam on 6/13/19.
//  Copyright © 2019 Kannan Seenivasagam. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import VideoToolbox
import SwiftyPlistManager
import NetworkExtension
import SwiftyJSON
import MapKit



class AirEntryVC: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    @IBOutlet weak var headRollLbl: UILabel!
    @IBOutlet weak var headRollSlider: UISlider!
    @IBOutlet weak var headYawLbl: UILabel!
    @IBOutlet weak var headYawSlider: UISlider!
    @IBOutlet weak var headPitchLabl: UILabel!
    @IBOutlet weak var headPitchSlider: UISlider!
    @IBOutlet weak var captureDistanceLbl: UILabel!
    @IBOutlet weak var captureDistanceSlider: UISlider!
    @IBOutlet weak var distanceBetweenEyesLbl: UILabel!
    @IBOutlet weak var distanceBetweenEyesSlider: UISlider!
    @IBOutlet weak var eyeRollLabl: UILabel!
    @IBOutlet weak var eyeRollSlider: UISlider!
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var showConfigSwitch: UISwitch!
    @IBOutlet weak var scannerOff: UISwitch!
    @IBOutlet weak var optoutImgView: UIImageView!
    @IBOutlet weak var showConfigLbl: UILabel!
    @IBOutlet weak var scannerOffLbl: UILabel!
    
    
    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var isFaceCaptured = false;
    var isFaceVisible = false;
    var start: CFAbsoluteTime = CFAbsoluteTimeGetCurrent();
    var startStatus: CFAbsoluteTime = CFAbsoluteTimeGetCurrent();
    var currentExpression: Expression? = nil
    var isExpression: Bool? = true;
    var expressionsToUse: [Expression] = [SmileExpression(), EyebrowsRaisedExpression(), EyeBlinkExpression(), JawOpenExpression()];
    let ms: UInt32 = 100000
    var cameraNode: SCNNode!
    var isSubmissionInProgress: Bool? = false;
    var scanResult:String?;
    var imgData: [Data] = [];
    var lastRefData: [Data] = [];
    var imgRefTimer: Timer!
    var globalMessage:String?
    var isSubmiting: Bool? = false;
    var uidString:String?
    
    var srcCnt  = 0;
    var tobecnt = 0;
    
    var starttime: DispatchTime = DispatchTime.now();
    
    var reqStartTime:String!
    var reqEndTime:String!
    var logmsgtype:String!;
    var logmsgStr:String!
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad();
        let width = UIScreen.main.bounds.size.width
        let height = UIScreen.main.bounds.size.height
        self.uidString = UUID().uuidString;
        let imageViewBackground = UIImageView(frame: CGRect(x:0, y:0, width: width, height: height))
        
        ////
        NotificationCenter.default.addObserver(
            forName: UIAccessibility.guidedAccessStatusDidChangeNotification,
            object: nil,
            queue: .main
        ) { (notification) in
            if !UIAccessibility.isGuidedAccessEnabled {
                // show something since I'm not in guided access
                DispatchQueue.main.async {
                    self.showConfigSwitch.isHidden = false;
                    self.showConfigLbl.isHidden = false;
                    self.scannerOff.isHidden = false;
                    self.scannerOffLbl.isHidden = false;
                }
            }else{
                DispatchQueue.main.async {
                    self.showConfigSwitch.isHidden = true;
                    self.showConfigLbl.isHidden = true;
                    self.scannerOff.isHidden = true;
                    self.scannerOffLbl.isHidden = true;
                }
            }
        }
        
        ////
      
        imageViewBackground.image = UIImage(named: "bluebackground.png")
        
        // you can change the content mode:
        imageViewBackground.contentMode = UIView.ContentMode.scaleAspectFill
        
        self.view.addSubview(imageViewBackground)
        self.view.sendSubviewToBack(imageViewBackground)
        self.scannerOff.isOn = false;
        self.optoutImgView.isHidden = true;
        
        SwiftyPlistManager.shared.getValue(for: "HEAD_ROLL", fromPlistWithName: "Config-MWAA") { (headRoll, err) in
            if err != nil {
                 self.getRemoteConfigValues();
            }else{
                debugPrint("NOT NULL[headRoll]::",headRoll as Any)
                self.populateConfigValues();
            }
        }
        guard ARFaceTrackingConfiguration.isSupported else { fatalError() }
        // Set the view's delegate
        sceneView.delegate = self
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = false;
        mainStackView.isHidden = true;
        setupCamera();
       /* if let camera = sceneView.pointOfView?.camera {
            camera.wantsHDR = true
            camera.wantsExposureAdaptation = true
            camera.exposureOffset = -1
            camera.minimumExposure = -1
            //camera.focusDistance = 5
            //camera.fieldOfView = 50
            //camera.focalLength = 50;
            debugPrint("FOCUS DISTANCE::",camera.focusDistance)
            debugPrint("FOCUS LENGTH::",camera.focalLength)
            debugPrint("camera.fieldOfView::",camera.fieldOfView);
           
            //camera.fieldOfView
        }
    */
    }
    func setupCamera() {
        // 1
        cameraNode = SCNNode()
        // 2
        cameraNode.camera = SCNCamera()
        // 3
        cameraNode.position = SCNVector3(x: 0, y: 5, z: 10)
        // 4
        self.sceneView.scene.rootNode.addChildNode(cameraNode)

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        startSession();
        imgRefTimer = Timer.scheduledTimer(timeInterval: GlobalConstants.APP_PASS_RESET_INTRVL, target: self, selector: #selector(resetLocalAlbum), userInfo: nil, repeats: true);
    }
  

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
        //RocWrapper.loadRocLicense();
        
        
        debugPrint("Step 1")
   /*     if let data1 = UIImage(named: "cbimage-3")!.jpegData(compressionQuality: 1){
            
            /*    let array = data1.withUnsafeBytes {
             [UInt8](UnsafeBufferPointer(start: $0, count: data1.count))
             }
             
             let base64String = data1.base64EncodedString(options: NSData.Base64EncodingOptions.endLineWithCarriageReturn);
             //imgData[0] = Data(base64Encoded: base64String)!
             */
            imgData.append(data1);
            //imgData.append(Data(base64Encoded: base64String)!)
            srcCnt = data1.count;
            debugPrint("srcCnt::",srcCnt)
            //tobecnt = data1.count;
        }
        */
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Pause the view's session
        sceneView.session.pause()
        // NotificationCenter.default.removeObserver(GuidedModeListener as Any)
    }
    func startSession() {
        
        guard ARFaceTrackingConfiguration.isSupported else { return };
        let configuration = ARFaceTrackingConfiguration();
        configuration.isLightEstimationEnabled = true;
        sceneView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
        DispatchQueue.main.async {
            self.optoutImgView.isHidden = true;
            self.sceneView.isHidden = false;
            self.start = CFAbsoluteTimeGetCurrent();
            self.isSubmissionInProgress = false;
        }
    }
    
    // MARK: - ARSCNViewDelegate
    
/*
    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
     
        return node
    }
*/
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        if let arError = error as? ARError {
            switch arError.errorCode {
            case 102:
                self.startSession();
            default:
               self.startSession();
            }
        }
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        DispatchQueue.main.async {
            self.startSession();
        }
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        debugPrint("SCNSceneRenderer");
        guard let _ = anchor as? ARFaceAnchor,
            let device = MTLCreateSystemDefaultDevice() else {
                return nil
        }
        let faceGeometry = ARSCNFaceGeometry(device: device)
        let node = SCNNode(geometry: faceGeometry);
      /*  node.geometry?.firstMaterial?.transparency = 0.0;
        node.renderingOrder = -1
        let shape = SCNBox(width: 0.15, height: 0.15, length: 0.001, chamferRadius: 0)
        let material = SCNMaterial()
        material.isDoubleSided = false;
        material.diffuse.contents = UIImage(named: "focus")
        material.transparencyMode = .aOne
        shape.materials = [material]
        node.addChildNode(SCNNode(geometry: shape))
 */
        node.geometry?.firstMaterial?.fillMode = .lines;
        //node.geometry?.firstMaterial?.transparency = 0.0;
         return node;
        
       
        //faceGeometry?.update(from: shape);
        //return SCNNode(geometry: shape);
        
    }
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        
        guard let faceAnchor = anchor as? ARFaceAnchor else {
            debugPrint("no face found..");
            isFaceCaptured = false;
            return;
        }
        guard let faceGeometry = node.geometry as? ARSCNFaceGeometry else {
            debugPrint("No face found...");
            isFaceCaptured = false;
            return;
        }
        faceGeometry.update(from: faceAnchor.geometry);
        self.updateFeatures(for: node, using: faceAnchor);
        
    }
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {

        if let frame = self.sceneView.session.currentFrame{
            for anchor in frame.anchors {
                if let node = sceneView.node(for: anchor){
                     let faceIsVisible = sceneView.isNode(node.presentation, insideFrustumOf: (sceneView?.pointOfView?.presentation)!)
                    
                    if ((!faceIsVisible) ){
                        //debugPrint("No face Found.....")
                        isFaceCaptured = false;
                    }else{
                        //debugPrint("Found Found.....")
                        self.isFaceVisible = true;
                    }
                }
            }
        }
    }
    func convert(length: Int, data: UnsafePointer<Int8>) -> [Int8] {
        
        let buffer = UnsafeBufferPointer(start: data, count: length);
        return Array(buffer)
    }
    func updateFeatures(for node: SCNNode, using anchor: ARFaceAnchor) {
        
        if((isFaceVisible) && (!isSubmiting!)){
            
            
          /*  let anchorPosition = anchor.transform.columns.3
            let cameraPosition = sceneView.session.currentFrame!.camera.transform.columns.3
            let cameraToAnchor = cameraPosition - anchorPosition
            let distance1 = length(cameraToAnchor);
           */
            let distance = node.convertPosition(node.position, to: sceneView.pointOfView);
            let length: Float = sqrtf(distance.x * distance.x + distance.y * distance.y + distance.z * distance.z);
            //debugPrint("FACE DISTANCE::",length)
           
            //debugPrint("Distance Between EYES PX::",distanceBetweenEyes);
            //debugPrint("Expression:::",isExpressing(from: anchor));
            debugPrint("STEP 1[distance]::",length, "::LOW::",CBPConstants.sharedInstance.CAPTURE_DISTANCE_LOW, "::HIGH::",CBPConstants.sharedInstance.CAPTURE_DISTANCE_HIGH);
            if((length > CBPConstants.sharedInstance.CAPTURE_DISTANCE_LOW) && (length < CBPConstants.sharedInstance.CAPTURE_DISTANCE_HIGH)){
                debugPrint("STEP 2[distance]::",length);
                if let frame = self.sceneView.session.currentFrame {
                     debugPrint("STEP 3[distance]::");
                    
                  DispatchQueue.main.async {
                    let projectionMatrix = frame.camera.projectionMatrix(for: .portrait, viewportSize: self.sceneView.bounds.size, zNear: 0.001, zFar: 1000)
                    let viewMatrix = frame.camera.viewMatrix(for: .portrait)
                    
                    let projectionViewMatrix = simd_mul(projectionMatrix, viewMatrix)
                    let modelMatrix = anchor.transform
                    let mvpMatrix = simd_mul(projectionViewMatrix, modelMatrix)
                    
                    // This allows me to just get a .x .y .z rotation from the matrix, without having to do crazy calculations
                    let newFaceMatrix = SCNMatrix4.init(mvpMatrix)
                    //let faceNode = SCNNode()
                    node.transform = newFaceMatrix
                    let rotation = vector_float3(node.worldOrientation.x, node.worldOrientation.y, node.worldOrientation.z);
                    let yaw = -rotation.y*3
                    let pitch = -rotation.x*3
                    let roll = rotation.z*1.5
                    
                    let pitchinDeg = (Double(pitch) * 180.0 / Double.pi).rounded();
                    let yawinDeg = (Double(yaw) * 180.0 / Double.pi).rounded();
                    let rollinDeg = (Double(roll) * 180.0 / Double.pi).rounded();
                    
                   /* debugPrint("AR ROLL-RAW::", rollinDeg);
                    debugPrint("AR YAW-RAW::", yawinDeg);
                    debugPrint("AR PITCH-RAW::", pitchinDeg);
                    debugPrint("\n=========================================\n");
                    
                    let lookAtPoint: simd_float3 = anchor.lookAtPoint;
                    //debugPrint("lookAtPoint[X]::",lookAtPoint.x, " Y::", lookAtPoint.y, " Z::",lookAtPoint.z);
                    */
                    debugPrint("STEP 2[pitchinDeg]::",pitchinDeg,"::yawinDeg::",yawinDeg,"::rollinDeg",rollinDeg);
                    debugPrint("HEAD_PITCH_LOW::",CBPConstants.sharedInstance.HEAD_PITCH_LOW,"::HEAD_PITCH_HIGH::",CBPConstants.sharedInstance.HEAD_PITCH_HIGH);
                    debugPrint("HEAD_YAW_LOW::",CBPConstants.sharedInstance.HEAD_YAW_LOW,"::HEAD_PITCH_HIGH::",CBPConstants.sharedInstance.HEAD_YAW_HIGH);
                    debugPrint("HEAD_ROLL_LOW::",CBPConstants.sharedInstance.HEAD_YAW_LOW,"::HEAD_ROLL_HIGH::",CBPConstants.sharedInstance.HEAD_ROLL_HIGH);
                    if( ((pitchinDeg >  CBPConstants.sharedInstance.HEAD_PITCH_LOW) && (pitchinDeg <  CBPConstants.sharedInstance.HEAD_PITCH_HIGH)) && ((yawinDeg >  CBPConstants.sharedInstance.HEAD_YAW_LOW) && (yawinDeg <  CBPConstants.sharedInstance.HEAD_YAW_HIGH)) && ((rollinDeg >  CBPConstants.sharedInstance.HEAD_ROLL_LOW) && (rollinDeg <  CBPConstants.sharedInstance.HEAD_ROLL_HIGH)) ){
                    //if( ((pitchinDeg >  CBPConstants.sharedInstance.HEAD_PITCH_LOW) && (pitchinDeg <  CBPConstants.sharedInstance.HEAD_PITCH_HIGH)) && ((yawinDeg >  CBPConstants.sharedInstance.HEAD_YAW_LOW) && (yawinDeg <  CBPConstants.sharedInstance.HEAD_YAW_HIGH)) && ((rollinDeg >  CBPConstants.sharedInstance.HEAD_ROLL_LOW) && (rollinDeg <  CBPConstants.sharedInstance.HEAD_ROLL_HIGH)) && (self.getEyeRoll(from: anchor))){

                        let elapsed = CFAbsoluteTimeGetCurrent() - self.start
                        debugPrint("ELAPSED[START]::",elapsed,self.isFaceCaptured);
                        
                        if(self.isFaceCaptured){
                            debugPrint("READY TO CREATE BINARY");
                            
                            let cimage = CIImage.init(cvPixelBuffer: frame.capturedImage);
                            let imageOptions: [String : Any] = [CIDetectorImageOrientation:5,
                                                                CIDetectorSmile: true,
                                                                CIDetectorEyeBlink: true]
                            let accuracy = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
                            let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: accuracy)
                            let faces = faceDetector?.features(in: cimage, options: imageOptions);
                            
                             if let face = faces?.first as? CIFaceFeature {
                                
                                var rectblur = face.bounds
                                rectblur.size.height = max(face.bounds.height, face.bounds.width)
                                rectblur.size.width = max(face.bounds.height, face.bounds.width)
                                rectblur = rectblur.insetBy(dx: 60, dy: 80);
                                let cropedimgblur = cimage.cropped(to: rectblur);
                                var croppedImageBlur:UIImage = self.convert(cmage: cropedimgblur);
                                croppedImageBlur = croppedImageBlur.rotated(by: Measurement(value: 90.0, unit: .degrees))!;
                                //let isBlurry = OpenCVWrapper.checkImageBlurry(croppedImageBlur);
                                //Uncomment the previous line
                                 let isBlurry = "NO";
                                //let isBlurry = OpenCVWrapper.checkImageBlurry(croppedImage);
                                //OpenCVWrapper.checkEyeGlasses(croppedImage);
                                /*
                                 let cbpformatter = DateFormatter();
                                 cbpformatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
                                 let cbpreqts = Date();
                                 let reqtime = cbpformatter.string(from: cbpreqts);
                                 */
                                let cbpreqts = Date();
                                let cbpformatter = DateFormatter();
                                cbpformatter.dateFormat = "MM-dd-yyyy'T'HH:mm:ss";
                                if(isBlurry == "NO"){
                                    self.reqStartTime = cbpformatter.string(from: cbpreqts);
                                    var rect = face.bounds
                                    rect.size.height = max(face.bounds.height, face.bounds.width)
                                    rect.size.width = max(face.bounds.height, face.bounds.width)
                                    rect = rect.insetBy(dx: -200, dy: -200);
                                    let cropedimg = cimage.cropped(to: rect);
                                    var croppedImage:UIImage = self.convert(cmage: cropedimg);
                                    croppedImage = croppedImage.rotated(by: Measurement(value: 90.0, unit: .degrees))!;
                                    
                                    if let data1 = croppedImage.jpegData(compressionQuality: 0.5){
                                        let base64String = data1.base64EncodedString(options: NSData.Base64EncodingOptions.endLineWithCarriageReturn);
                                        debugPrint("DATA::",base64String);
                                        
                                        self.start = CFAbsoluteTimeGetCurrent();
                                        self.isFaceCaptured = false;
                                        self.isSubmissionInProgress = true;
                                        
                                        let nanoTime = DispatchTime.now().uptimeNanoseconds - self.starttime.uptimeNanoseconds;
                                        let timeInterval = Double(nanoTime) / 1_000_000_000;
                                        if(timeInterval >= 10){
                                            self.lastRefData.removeAll();
                                            self.starttime = DispatchTime.now();
                                            self.uidString = UUID().uuidString;
                                        }
                                        
                                        //self.buildImageRequest(base64String: base64String, imgSize: data1.count);
                                        if(self.lastRefData.count > 0){
                                            
                                           /*  let array = self.lastRefData[0].withUnsafeBytes {
                                               return  [UInt8](UnsafeBufferPointer(start: $0, count: self.lastRefData[0].count))
                                            }
                                           let array1 = data1.withUnsafeBytes {
                                                [UInt8](UnsafeBufferPointer(start: $0, count: data1.count))
                                            }
                                          */
                                            var byteBuffer: [UInt8] = [];
                                            self.lastRefData[0].withUnsafeBytes {
                                                byteBuffer.append(contentsOf: $0)
                                            }
                                            let array = [UInt8](UnsafeBufferPointer(start: byteBuffer, count: self.lastRefData[0].count));
                                           
                                            byteBuffer = [];
                                            data1.withUnsafeBytes {
                                                byteBuffer.append(contentsOf: $0)
                                            }
                                            let array1 = [UInt8](UnsafeBufferPointer(start: byteBuffer, count: data1.count));
                                             //Un comment this
                                             let faceMetaData:NSDictionary = RocWrapper.faceCompare(array, original: array1, tobeimagelen: self.lastRefData[0].count, origimagelen: data1.count) as NSDictionary;
                                            let similarity = (faceMetaData["similarity"] as! NSString).floatValue;
                                            let sunglasses = (faceMetaData["sunglasses"] as! NSString).floatValue;
                                            let eyeglasses = (faceMetaData["eyeglasses"] as! NSString).floatValue;
 
                                            /*let similarity = 0.50
                                            let sunglasses = 0.50
                                            let eyeglasses = 0.50
                                           */
                                            debugPrint("Match Found[similarity]......",similarity)
                                            if(similarity > 0.90){
                                                
                                                debugPrint("Match Found......");
                                                self.scanResult = "TAKEN";
                                                self.performSegue(withIdentifier: "statussegue", sender: self);
                                                
                                            }else{
                                                self.lastRefData.removeAll();
                                                self.scanResult = "";
                                                 self.globalMessage = "";
                                                if((sunglasses < 0.8) && (eyeglasses < 0.8)){
                                                    self.isSubmiting = true;
                                                    self.doSubmitAirEntry(base64String: base64String, imgSize: data1.count) { verifyresult, error in
                                                       debugPrint("verifyresult::",verifyresult)
                                                        
                                                        if error != nil {
                                                            debugPrint("verifyresultww[NOMATCH]::")
                                                            self.scanResult = "NOMATCH";
                                                            self.globalMessage = "Request Failed.";
                                                        }else{
                                                            
                                                            if let code = verifyresult.value(forKey: "Code") {
                                                                debugPrint("statuscode::",code);
                                                                let status = "\(code)";
                                                                if let message = verifyresult.value(forKey: "Message") {
                                                                    self.globalMessage = "\(message)";
                                                                }
                                                                if(status == "SUCCESS" ){
                                                                    self.scanResult = "MATCH";
                                                                    self.lastRefData.append(data1);
                                                                    self.starttime = DispatchTime.now();
                                                                    self.uidString = UUID().uuidString;
                                                                }
                                                                if(status == "STOP_CAPTURE" ){
                                                                    self.scanResult = "STOP_CAPTURE";
                                                                    self.lastRefData.append(data1);
                                                                    self.starttime = DispatchTime.now();
                                                                    self.uidString = UUID().uuidString;
                                                                }
                                                                if(status == "RETAKE_PHOTO" ){
                                                                    self.scanResult = "NOMATCH";
                                                                    self.starttime = DispatchTime.now();
                                                                }
                                                            }else{
                                                                debugPrint("status::[NOMATCH]")
                                                                self.scanResult = "NOMATCH";
                                                                self.globalMessage = "Bad Response.";
                                                            }
                                                        }
                                                        self.isSubmiting = false;
                                                        self.performSegue(withIdentifier: "statussegue", sender: self);
                                                    }
                                                }else{
                                                    self.scanResult = "EYEGLASS";
                                                    self.performSegue(withIdentifier: "statussegue", sender: self);
                                                }
                                            }
                                            
                                        }else{
                                            
                                          /*  let currentFace = data1.withUnsafeBytes {
                                                [UInt8](UnsafeBufferPointer(start: $0, count: data1.count))
                                            }
                                         */
                                            var byteBuffer: [UInt8] = [];
                                            data1.withUnsafeBytes {
                                                byteBuffer.append(contentsOf: $0)
                                            }
                                            let currentFace = [UInt8](UnsafeBufferPointer(start: byteBuffer, count: data1.count));
 
                                             //Un Comment This
                                             let faceMetaData:NSDictionary = RocWrapper.faceMetaData(currentFace, origimagelen: data1.count) as NSDictionary;
                                            debugPrint("No Reffrence Image Found......",faceMetaData)
                                            let sunglasses = (faceMetaData["sunglasses"] as! NSString).floatValue;
                                            let eyeglasses = (faceMetaData["eyeglasses"] as! NSString).floatValue;
                                           
                                           
                                           /* let sunglasses = 0.50
                                            let eyeglasses = 0.50
                                            */
                                            
                                            if((sunglasses < 0.8) && (eyeglasses < 0.8)){
                                                self.lastRefData.removeAll();
                                                self.scanResult = "";
                                                self.globalMessage = "";
                                                self.isSubmiting = true;
                                                self.doSubmitAirEntry(base64String: base64String, imgSize: data1.count) { verifyresult, error in
                                                    if error != nil {
                                                        debugPrint("verifyresultww[NOMATCH]::")
                                                        self.scanResult = "NOMATCH";
                                                        self.globalMessage = "Request Failed.";
                                                    }else{
                                                        
                                                        if let code = verifyresult.value(forKey: "Code") {
                                                            debugPrint("statuscode::",code);
                                                            let status = "\(code)";
                                                            if let message = verifyresult.value(forKey: "Message") {
                                                                 self.globalMessage = "\(message)";
                                                            }
                                                            if(status == "SUCCESS" ){
                                                                self.scanResult = "MATCH";
                                                                self.lastRefData.append(data1);
                                                                self.starttime = DispatchTime.now();
                                                                self.uidString = UUID().uuidString;
                                                            }
                                                            if(status == "STOP_CAPTURE" ){
                                                                self.scanResult = "STOP_CAPTURE";
                                                                self.lastRefData.append(data1);
                                                                self.starttime = DispatchTime.now();
                                                                self.uidString = UUID().uuidString;
                                                            }
                                                            if(status == "RETAKE_PHOTO" ){
                                                                self.scanResult = "NOMATCH";
                                                                self.starttime = DispatchTime.now();
                                                            }
                                                        }else{
                                                            debugPrint("status::[NOMATCH]")
                                                            self.scanResult = "NOMATCH";
                                                            self.globalMessage = "Bad Response.";
                                                        }
                                                    }
                                                    self.isSubmiting = false;
                                                    self.performSegue(withIdentifier: "statussegue", sender: self);
                                                }
                                            }else{
                                                
                                                self.scanResult = "EYEGLASS";
                                                self.performSegue(withIdentifier: "statussegue", sender: self);
                                                
                                            }
                                           
                                        }
                                        
                                    }
                                }else{
                                   debugPrint("BLURRY IMAGE....");
                                   // self.isFaceCaptured = true;
                                   self.isFaceCaptured = false;
                                   self.start = CFAbsoluteTimeGetCurrent()
                                }
                            }
                        }else{
                           
                            let elapsed = CFAbsoluteTimeGetCurrent() - self.start;
                            debugPrint("ELAPSED TIME[ELSE]",elapsed);
                            if(elapsed >= 20){
                                self.start = CFAbsoluteTimeGetCurrent()
                            }else if(elapsed >= 1.5){
                                self.isFaceCaptured = true;
                                self.start = CFAbsoluteTimeGetCurrent()
                            }
                        }
                    }
                  }
                }
            }else{
                DispatchQueue.main.async {
                    self.scanResult = "CLOSER";
                    self.performSegue(withIdentifier: "statussegue", sender: self);
                }
            }
        }
    }
    enum APIError: Error {
        case runtimeError(String)
    }
    func doSubmitAirEntry(base64String: String, imgSize: Int, completion: @escaping (_ snapshot: NSDictionary, _ cerror: Error? ) -> Void) {
       
        let cbpcapreqts = Date();
        let cbpformatter = DateFormatter();
        cbpformatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        let capturedate = cbpformatter.string(from: cbpcapreqts)
        
        let photoDetails:NSMutableDictionary = NSMutableDictionary();
        photoDetails.setValue(self.uidString, forKey: "VendorPersonId");
        photoDetails.setValue(base64String, forKey: "Photo");
        photoDetails.setValue(CBPConstants.sharedInstance.SVC_TOKEN, forKey: "Token");
        photoDetails.setValue(self.reqStartTime,         forKey: "CaptureTime");
        photoDetails.setValue(capturedate,         forKey: "SendTime");
        
        
        do {
            
            let requestData: Data = try JSONSerialization.data(withJSONObject: photoDetails, options: JSONSerialization.WritingOptions.prettyPrinted);
            if let string = NSString(data: requestData, encoding: String.Encoding.utf8.rawValue) {
                debugPrint("cbpTransJson:",string)
            }
            
            let airEntryRequest : NSMutableURLRequest = NSMutableURLRequest();
            //let airEntryUrlStr:NSString = NSString(format: "%@", GlobalConstants.AIR_ENTRY_VERIFY_URL+"/identify");
            let airEntryUrlStr:NSString = NSString(format: "%@", GlobalConstants.AIR_ENTRY_VERIFY_URL+"/entry?dummy="+capturedate);
            airEntryRequest.url = URL(string: airEntryUrlStr as String);
            airEntryRequest.httpMethod = "POST";
            airEntryRequest.addValue("Keep-Alive", forHTTPHeaderField: "Connection")
            airEntryRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
            airEntryRequest.addValue("application/json", forHTTPHeaderField: "Accept")
            
            airEntryRequest.httpBody = requestData;
            let config = URLSessionConfiguration.default;
            config.timeoutIntervalForRequest = TimeInterval(GlobalConstants.REQ_TIMEOUT);
            config.timeoutIntervalForResource = TimeInterval(GlobalConstants.REQ_TIMEOUT);
            
            //config.timeoutIntervalForRequest = 0.001;
            //config.timeoutIntervalForResource = 0.001;
            
            /*let session = URLSession(
                configuration: config,
                delegate: VeriscanSSLPinner(),
                delegateQueue: nil)
            */
            let session = URLSession(configuration: config);
            let semaphore = DispatchSemaphore(value: 0);
            
            let airEntryRequestTask = session.dataTask(with: airEntryRequest as URLRequest, completionHandler: {
                (airEntryRequestRespData, airEntryRequestResponse, error) in
                if(airEntryRequestResponse != nil){
                    let statusCode = (airEntryRequestResponse as! HTTPURLResponse).statusCode
                    debugPrint("statusCode::",statusCode);
                    
                    let cbpcapreqtes = Date();
                    self.reqEndTime = cbpformatter.string(from: cbpcapreqtes);
                    
                    if(statusCode == 200){
                        if let airEntryRequestRespData = airEntryRequestRespData, error == nil{
                            do{
                                let airEntryJson = try JSON(data: airEntryRequestRespData);
                                debugPrint(airEntryJson);
                                
                                if airEntryJson["Result"] == JSON.null {
                                
                                    self.saveLog(startDate: self.reqStartTime, endDate: self.reqEndTime, msgType: "ERROR", msgStr: "\(airEntryJson)");
                                    let statusDict:NSMutableDictionary = NSMutableDictionary();
                                    statusDict.setValue("RETAKE_PHOTO", forKey: "Code");
                                    statusDict.setValue("Request Failed", forKey: "Message");
                                    completion(statusDict, nil)
                                    
                                }else{
                                    let code = airEntryJson["Result"];
                                    debugPrint("Result::",code.stringValue)
                                    debugPrint("Status::",airEntryJson["Status"]);
                                    if airEntryJson["Status"] == JSON.null {
                                        
                                        self.saveLog(startDate: self.reqStartTime, endDate: self.reqEndTime, msgType: "ERROR", msgStr: "Invalid Response Status Element is Missing.");
                                        let statusDict:NSMutableDictionary = NSMutableDictionary();
                                        statusDict.setValue("RETAKE_PHOTO", forKey: "Code");
                                        statusDict.setValue("Request Failed", forKey: "Message");
                                        completion(statusDict, nil)
                                        
                                    }else{
                                        
                                        let statusObj = airEntryJson["Status"];
                                        if ((statusObj["Code"] != JSON.null) &&  (statusObj["Message"] != JSON.null)){
                                            completion(airEntryJson["Status"].dictionary! as NSDictionary, nil)
                                        }else{
                                           self.saveLog(startDate: self.reqStartTime, endDate: self.reqEndTime, msgType: "ERROR", msgStr: "Missing Status Code/Message.");
                                           let statusDict:NSMutableDictionary = NSMutableDictionary();
                                           statusDict.setValue("RETAKE_PHOTO", forKey: "Code");
                                           statusDict.setValue("Request Failed", forKey: "Message");
                                           completion(statusDict, nil)
                                        }
                                    }
                                    
                                }
                                
                            }catch  {
                                debugPrint("error trying to convert data to JSON");
                                self.saveLog(startDate: self.reqStartTime, endDate: self.reqEndTime, msgType: "ERROR", msgStr: "\(error.localizedDescription)");
                                completion([:], error);
                                
                            }
                        }
                    }else{
                        
                        self.saveLog(startDate: self.reqStartTime, endDate: self.reqEndTime, msgType: "ERROR", msgStr: "Time Entry Failed:[" + String(statusCode) + "]");
                        completion([:], APIError.runtimeError("CBP Entry Failed:[" + String(statusCode) + "]"));
                       
                    }
                }else{
                    
                    let cbpcapreqtes = Date();
                    self.reqEndTime = cbpformatter.string(from: cbpcapreqtes);
                    self.saveLog(startDate: self.reqStartTime, endDate: self.reqEndTime, msgType: "ERROR", msgStr: "Request Timed out");
                    completion([:], APIError.runtimeError("Request Timed out"));
                }
                semaphore.signal();
            })
            airEntryRequestTask.resume();
            _ = semaphore.wait(timeout: DispatchTime.distantFuture);
            
            
        } catch let error as NSError {
            let cbpcapreqtes = Date();
            self.reqEndTime = cbpformatter.string(from: cbpcapreqtes);
            debugPrint("Failed to load: \(error.localizedDescription)");
            self.saveLog(startDate: self.reqStartTime, endDate: self.reqEndTime, msgType: "ERROR", msgStr: error.localizedDescription);
            
        }
        
    }

    func JSONStringify(_ value: AnyObject,prettyPrinted:Bool = false) -> String{
        
        let options = prettyPrinted ? JSONSerialization.WritingOptions.prettyPrinted : JSONSerialization.WritingOptions(rawValue: 0)
        
        
        if JSONSerialization.isValidJSONObject(value) {
            
            do{
                let data = try JSONSerialization.data(withJSONObject: value, options: options)
                if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                    return string as String
                }
            }catch {
                
                debugPrint("error")
                //Access error here
            }
            
        }
        return ""
    }
    
    func convert(cmage:CIImage) -> UIImage
    {
        let context:CIContext = CIContext.init(options: nil)
        let cgImage:CGImage = context.createCGImage(cmage, from: cmage.extent)!
        let image:UIImage = UIImage.init(cgImage: cgImage)
        return image
    }
    func getEyeRoll(from: ARFaceAnchor) -> Bool {
      
        if let eyeLookInLeft = from.blendShapes[.eyeLookInLeft],
           let eyeLookInRight = from.blendShapes[.eyeLookInRight],
           let eyeWideLeft = from.blendShapes[.eyeWideLeft],
           let eyeWideRight = from.blendShapes[.eyeWideRight],
           let eyeLookOutLeft = from.blendShapes[.eyeLookOutLeft],
           let eyeLookOutRight = from.blendShapes[.eyeLookOutRight],
           let eyeLookUpLeft = from.blendShapes[.eyeLookUpLeft],
           let eyeLookUpRight = from.blendShapes[.eyeLookUpRight] {
            debugPrint("eyeLookInLeft.floatValue::",eyeLookInLeft.floatValue,
                       "\neyeLookInRight.floatValue::",eyeLookInRight.floatValue,
                       "\neyeWideLeft::",eyeWideLeft.floatValue,
                       "\neyeWideRight.floatValue::",eyeWideRight.floatValue,
                       "\neyeLookOutLeft.floatValue::",eyeLookOutLeft.floatValue,
                       "\neyeLookOutRight.floatValue::",eyeLookOutRight.floatValue,
                       "\neyeLookUpLeft.floatValue::",eyeLookUpLeft.floatValue,
                       "\neyeLookUpRight.floatValue::",eyeLookUpRight.floatValue
                         )
            
            if( (eyeLookInLeft.floatValue < CBPConstants.sharedInstance.EYE_ROLL_HIGH)   &&
                (eyeLookInRight.floatValue < CBPConstants.sharedInstance.EYE_ROLL_HIGH)   &&
                (eyeWideLeft.floatValue < CBPConstants.sharedInstance.EYE_ROLL_HIGH)     &&
                (eyeWideRight.floatValue < CBPConstants.sharedInstance.EYE_ROLL_HIGH)    &&
                (eyeLookOutLeft.floatValue < CBPConstants.sharedInstance.EYE_ROLL_HIGH)  &&
                (eyeLookOutRight.floatValue < CBPConstants.sharedInstance.EYE_ROLL_HIGH) &&
                (eyeLookUpLeft.floatValue < CBPConstants.sharedInstance.EYE_ROLL_HIGH)   &&
                (eyeLookUpRight.floatValue < CBPConstants.sharedInstance.EYE_ROLL_HIGH))   {
                return true;
            }
            
        }
        return false;
    }
    private var applogs = [AppLog]()
    func saveLog(startDate:String, endDate: String, msgType: String, msgStr: String){
       
        let appLog = AppLog(entity: AppLog.entity(), insertInto: context);
        appLog.transstart = startDate;
        appLog.transend   = endDate;
        appLog.messagetype = msgType;
        appLog.message = msgStr;
        appDelegate.saveContext();
    }
    func deleteAppLogs(){
       do {
        
            applogs = try context.fetch(AppLog.fetchRequest());
            debugPrint("applogs.count::",applogs.count)
            if(applogs.count > 0){
                
                for applog in applogs
                {
                    context.delete(applog);
                    
                }
                appDelegate.saveContext();
            }
        } catch let error as NSError {
          debugPrint("Could not fetch. \(error), \(error.userInfo)")
        }
        
    }
    func isExpressing(from: ARFaceAnchor) -> Bool {
       
        if let smileLeft = from.blendShapes[.mouthSmileLeft], let smileRight = from.blendShapes[.mouthSmileRight] {
            debugPrint("smileLeft.floatValue > 0.2 || smileRight.floatValue",smileLeft.floatValue , smileRight.floatValue);
            if(smileLeft.floatValue > 0.2 || smileRight.floatValue > 0.2){
                return true;
            }
        }
        if let leftBlink = from.blendShapes[.eyeBlinkLeft], let righBlink = from.blendShapes[.eyeBlinkRight] {
             debugPrint("leftBlink.floatValue > 0.2 || righBlink.floatValue > 0.2",leftBlink.floatValue > 0.2 , righBlink.floatValue > 0.2)
            if(leftBlink.floatValue > 0.2 || righBlink.floatValue > 0.2){
                return true;
            }
            
        }
        if let leftbrowDown = from.blendShapes[.browDownLeft], let righbrowDown = from.blendShapes[.browDownRight] {
            debugPrint("leftbrowDown.floatValue > 0.2 || righbrowDown.floatValue > 0.2",leftbrowDown.floatValue > 0.2 , righbrowDown.floatValue > 0.2)
            if(leftbrowDown.floatValue > 0.2 || righbrowDown.floatValue > 0.2){
                return true;
            }
            
        }
        if let browInnerUpDown = from.blendShapes[.browInnerUp] {
            debugPrint("browInnerUpDown.floatValue > 0.2",browInnerUpDown.floatValue > 0.2)
            if(browInnerUpDown.floatValue > 0.2){
                return true;
            }
            
        }
        if let browOuterUpLeft = from.blendShapes[.browOuterUpLeft], let browOuterUpRight = from.blendShapes[.browOuterUpRight] {
            debugPrint("browOuterUpLeft.floatValue > 0.2 || browOuterUpRight.floatValue > 0.2",browOuterUpLeft.floatValue > 0.2 , browOuterUpRight.floatValue > 0.2)
            if(browOuterUpLeft.floatValue > 0.2 || browOuterUpRight.floatValue > 0.2){
                return true;
            }
            
        }
        if let cheekPuff = from.blendShapes[.cheekPuff] {
            debugPrint("cheekPuff.floatValue > 0.2",cheekPuff.floatValue > 0.2)
            if(cheekPuff.floatValue > 0.2){
                return true;
            }
           
        }
        if let cheekSquintLeft = from.blendShapes[.cheekSquintLeft], let cheekSquintRight = from.blendShapes[.cheekSquintRight] {
            debugPrint("cheekSquintLeft.floatValue > 0.2 || cheekSquintRight.floatValue > 0.2",cheekSquintLeft.floatValue > 0.2 , cheekSquintRight.floatValue > 0.2)
            if(cheekSquintLeft.floatValue > 0.2 || cheekSquintRight.floatValue > 0.2){
                return true;
            }
           
        }
        if let eyeLookDownLeft = from.blendShapes[.eyeLookDownLeft], let eyeLookDownRight = from.blendShapes[.eyeLookDownRight] {
            debugPrint("eyeLookDownLeft.floatValue > 0.2 || eyeLookDownRight.floatValue > 0.2",eyeLookDownLeft.floatValue > 0.2 , eyeLookDownRight.floatValue > 0.2)
            if(eyeLookDownLeft.floatValue > 0.2 || eyeLookDownRight.floatValue > 0.2){
                return true;
            }
            
        }
        if let eyeLookInLeft = from.blendShapes[.eyeLookInLeft], let eyeLookInRight = from.blendShapes[.eyeLookInRight] {
            debugPrint(" eyeLookInLeft.floatValue > 0.2 || eyeLookInRight.floatValue > 0.2", eyeLookInLeft.floatValue > 0.2 , eyeLookInRight.floatValue > 0.2)
            if(eyeLookInLeft.floatValue > 0.2 || eyeLookInRight.floatValue > 0.2){
                return true;
            }
            
        }
        if let eyeLookOutLeft = from.blendShapes[.eyeLookOutLeft], let eyeLookOutRight = from.blendShapes[.eyeLookOutRight] {
            debugPrint("eyeLookOutLeft.floatValue > 0.2 || eyeLookOutRight.floatValue > 0.2",eyeLookOutLeft.floatValue > 0.2 , eyeLookOutRight.floatValue > 0.2)
            if(eyeLookOutLeft.floatValue > 0.2 || eyeLookOutRight.floatValue > 0.2){
                return true;
            }
            
        }
        if let eyeSquintLeft = from.blendShapes[.eyeSquintLeft], let eyeSquintRight = from.blendShapes[.eyeSquintRight] {
            debugPrint("eyeSquintLeft.floatValue > 0.2 || eyeSquintRight.floatValue > 0.2",eyeSquintLeft.floatValue > 0.2 , eyeSquintRight.floatValue > 0.2)
            if(eyeSquintLeft.floatValue > 0.2 || eyeSquintRight.floatValue > 0.2){
                return true;
            }
            
        }
        if let eyeWideLeft = from.blendShapes[.eyeWideLeft], let eyeWideRight = from.blendShapes[.eyeWideRight] {
            debugPrint("eyeWideLeft.floatValue > 0.2 || eyeWideRight.floatValue > 0.2",eyeWideLeft.floatValue > 0.2 , eyeWideRight.floatValue > 0.2)
            if(eyeWideLeft.floatValue > 0.2 || eyeWideRight.floatValue > 0.2){
                return true;
            }
            
        }
        if let eyeLookUpLeft = from.blendShapes[.eyeLookUpLeft], let eyeLookUpRight = from.blendShapes[.eyeLookUpRight] {
            debugPrint("eyeLookUpLeft.floatValue > 0.2 || eyeLookUpRight.floatValue > 0.2",eyeLookUpLeft.floatValue > 0.2 , eyeLookUpRight.floatValue > 0.2)
            if(eyeLookUpLeft.floatValue > 0.2 || eyeLookUpRight.floatValue > 0.2){
                return true;
            }
           
        }
        if let jawForward = from.blendShapes[.jawForward] {
            debugPrint("jawForward.floatValue > 0.2",jawForward.floatValue > 0.2)
            if(jawForward.floatValue > 0.2){
                return true;
            }
           
        }
        if let jawRight = from.blendShapes[.jawRight], let jawLeft = from.blendShapes[.jawLeft] {
            debugPrint("jawRight.floatValue > 0.2 || jawLeft.floatValue > 0.2",jawRight.floatValue > 0.2 , jawLeft.floatValue > 0.2)
            if(jawRight.floatValue > 0.2 || jawLeft.floatValue > 0.2){
                return true;
            }
            
        }
        if let jawOpen = from.blendShapes[.jawOpen] {
            debugPrint("jawOpen.floatValue > 0.2",jawOpen.floatValue > 0.2)
            if(jawOpen.floatValue > 0.2){
                return true;
            }
            
        }
        if let tongueOut = from.blendShapes[.tongueOut] {
            debugPrint("tongueOut.floatValue > 0.2",tongueOut.floatValue > 0.2)
            if(tongueOut.floatValue > 0.2){
                return true;
            }
           
        }
        if let mouthFunnel = from.blendShapes[.mouthFunnel] {
            debugPrint("mouthFunnel.floatValue > 0.2",mouthFunnel.floatValue > 0.2)
            if(mouthFunnel.floatValue > 0.2){
                return true;
            }
           
        }
        if let mouthClose = from.blendShapes[.mouthClose] {
            debugPrint("mouthClose.floatValue > 0.2",mouthClose.floatValue > 0.2)
            if( mouthClose.floatValue > 0.2){
                return true;
            }
           
        }
        if let mouthDimpleLeft = from.blendShapes[.mouthDimpleLeft], let mouthDimpleRight = from.blendShapes[.mouthDimpleRight] {
            debugPrint("mouthDimpleLeft.floatValue > 0.2 || mouthDimpleRight.floatValue > 0.2",mouthDimpleLeft.floatValue > 0.2 || mouthDimpleRight.floatValue > 0.2)
            if(mouthDimpleLeft.floatValue > 0.2 || mouthDimpleRight.floatValue > 0.2){
                return true;
            }
            
        }
        if let mouthFrownLeft = from.blendShapes[.mouthFrownLeft], let mouthFrownRight = from.blendShapes[.mouthFrownRight] {
            debugPrint("mouthFrownLeft.floatValue > 0.2 || mouthFrownRight.floatValue > 0.2",mouthFrownLeft.floatValue > 0.2 || mouthFrownRight.floatValue > 0.2)
            if(mouthFrownLeft.floatValue > 0.2 || mouthFrownRight.floatValue > 0.2){
                return true;
            }
            
        }
        if let mouthFrownLeft = from.blendShapes[.mouthFrownLeft], let mouthFrownRight = from.blendShapes[.mouthFrownRight] {
            debugPrint("mouthFrownLeft.floatValue > 0.2 || mouthFrownRight.floatValue > 0.2",mouthFrownLeft.floatValue > 0.2 , mouthFrownRight.floatValue > 0.2)
            if( mouthFrownLeft.floatValue > 0.2 || mouthFrownRight.floatValue > 0.2){
                return true;
            }
        }
        if let mouthLeft = from.blendShapes[.mouthLeft], let mouthRight = from.blendShapes[.mouthRight] {
            debugPrint("mouthLeft.floatValue > 0.2 || mouthRight.floatValue > 0.2",mouthLeft.floatValue > 0.2 , mouthRight.floatValue > 0.2)
            if(mouthLeft.floatValue > 0.2 || mouthRight.floatValue > 0.2){
                return true;
            }
        }
        if let mouthLowerDownLeft = from.blendShapes[.mouthLowerDownLeft], let mouthLowerDownRight = from.blendShapes[.mouthLowerDownRight] {
            debugPrint("mouthLowerDownLeft.floatValue > 0.2 || mouthLowerDownRight.floatValue > 0.2",mouthLowerDownLeft.floatValue > 0.2 , mouthLowerDownRight.floatValue > 0.2)
            if(mouthLowerDownLeft.floatValue > 0.2 || mouthLowerDownRight.floatValue > 0.2){
                return true;
            }
        }
        if let mouthPressLeft = from.blendShapes[.mouthPressLeft], let mouthPressRight = from.blendShapes[.mouthPressRight] {
            debugPrint("mouthPressLeft.floatValue > 0.2 || mouthPressRight.floatValue > 0.2",mouthPressLeft.floatValue > 0.2 , mouthPressRight.floatValue > 0.2)
            if(mouthPressLeft.floatValue > 0.2 || mouthPressRight.floatValue > 0.2){
                return true;
            }
        }
        if let mouthPucker = from.blendShapes[.mouthPucker] {
            debugPrint("mouthPucker.floatValue > 0.2",mouthPucker.floatValue > 0.2)
            if(mouthPucker.floatValue > 0.2){
                return true;
            }
        }
        if let mouthShrugLower = from.blendShapes[.mouthShrugLower], let mouthShrugUpper = from.blendShapes[.mouthShrugUpper] {
            debugPrint("mouthShrugLower.floatValue > 0.2 || mouthShrugUpper.floatValue > 0.2",mouthShrugLower.floatValue > 0.2 , mouthShrugUpper.floatValue > 0.2)
            if(mouthShrugLower.floatValue > 0.2 || mouthShrugUpper.floatValue > 0.2){
                return true;
            }
        }
        if let mouthSmileLeft = from.blendShapes[.mouthSmileLeft], let mouthSmileRight = from.blendShapes[.mouthSmileRight] {
            debugPrint("mouthSmileLeft.floatValue > 0.2 || mouthSmileRight.floatValue > 0.2",mouthSmileLeft.floatValue > 0.2 || mouthSmileRight.floatValue > 0.2)
            if(mouthSmileLeft.floatValue > 0.2 || mouthSmileRight.floatValue > 0.2){
                return true;
            }
        }
        if let mouthStretchLeft = from.blendShapes[.mouthStretchLeft], let mouthStretchRight = from.blendShapes[.mouthStretchRight] {
            debugPrint("mouthStretchLeft.floatValue > 0.2 || mouthStretchRight.floatValue > 0.2",mouthStretchLeft.floatValue > 0.2 || mouthStretchRight.floatValue > 0.2)
            if(mouthStretchLeft.floatValue > 0.2 || mouthStretchRight.floatValue > 0.2){
                return true;
            }
        }
        if let mouthUpperUpLeft = from.blendShapes[.mouthUpperUpLeft], let mouthUpperUpRight = from.blendShapes[.mouthUpperUpRight] {
            debugPrint("mouthUpperUpLeft.floatValue > 0.2 || mouthUpperUpRight.floatValue > 0.2",mouthUpperUpLeft.floatValue > 0.2 || mouthUpperUpRight.floatValue > 0.2)
            if(mouthUpperUpLeft.floatValue > 0.2 || mouthUpperUpRight.floatValue > 0.2){
                return true;
            }
        }
        if let noseSneerLeft = from.blendShapes[.noseSneerLeft], let noseSneerRight = from.blendShapes[.noseSneerRight] {
            debugPrint("noseSneerLeft.floatValue > 0.2 || noseSneerRight.floatValue > 0.2",noseSneerLeft.floatValue > 0.2 || noseSneerRight.floatValue > 0.2)
            if(noseSneerLeft.floatValue > 0.2 || noseSneerRight.floatValue > 0.2){
                return true;
            }
        }
       return false;
    }
    
    // Start Config Value Functions.
   
    @IBAction func setHeadRoll(_ sender: UISlider) {
        
        self.headRollSlider.setValue((round(sender.value / 1) * 1), animated: false)
        self.headRollLbl.text = "Head Roll: \(sender.value)"
        self.savePlistKeyValues(key: "HEAD_ROLL", value: String(sender.value), plistname: "Config-MWAA");
        CBPConstants.sharedInstance.HEAD_ROLL_HIGH = Double(sender.value)
        
    }
    
    @IBAction func setHeadYaw(_ sender: UISlider) {
        
        self.headYawSlider.setValue((round(sender.value / 1) * 1), animated: false)
        self.headYawLbl.text = "Head Yaw: \(sender.value)";
        self.savePlistKeyValues(key: "HEAD_YAW", value: String(sender.value), plistname: "Config-MWAA")
        CBPConstants.sharedInstance.HEAD_YAW_HIGH = Double(sender.value)
        
    }
    
    @IBAction func setHeadPitch(_ sender: UISlider) {
        
        self.headPitchSlider.setValue((round(sender.value / 1) * 1), animated: false)
        self.headPitchLabl.text = "Head Pitch: \(sender.value)"
        self.savePlistKeyValues(key: "HEAD_PITCH", value: String(sender.value), plistname: "Config-MWAA")
        CBPConstants.sharedInstance.HEAD_PITCH_HIGH = Double(sender.value)
    }
    
    @IBAction func setCaptureDistance(_ sender: UISlider) {
        
        self.captureDistanceSlider.setValue((round(sender.value / 0.1) * 0.1), animated: false)
        self.captureDistanceLbl.text = "Capture Distance: \(sender.value)"
        self.savePlistKeyValues(key: "HEAD_CAPTURE_DISTANCE", value: String(sender.value), plistname: "Config-MWAA")
        CBPConstants.sharedInstance.CAPTURE_DISTANCE_HIGH = sender.value
    }
    
    @IBAction func setDistanceBetweenEyes(_ sender: UISlider) {
        
        self.distanceBetweenEyesSlider.setValue((round(sender.value / 5) * 5), animated: false)
        self.distanceBetweenEyesLbl.text = "Distance Between Eyes: \(sender.value)"
        self.savePlistKeyValues(key: "DISTANCE_BETWEEN_EYES", value: String(sender.value), plistname: "Config-MWAA")
        CBPConstants.sharedInstance.DISTANCE_BETWEEN_EYES_HIGH = sender.value
        
    }
    
    @IBAction func setEyeRoll(_ sender: UISlider) {
        
        self.eyeRollSlider.setValue((round(sender.value / 0.05) * 0.05), animated: false)
        self.eyeRollLabl.text = "Eye Roll: \(sender.value)"
        self.savePlistKeyValues(key: "EYE_ROLL", value: String(sender.value), plistname: "Config-MWAA")
        CBPConstants.sharedInstance.EYE_ROLL_HIGH = sender.value
    }
    
    @IBAction func showConfig(_ sender: UISwitch) {
        
        if(sender.isOn){
            self.mainStackView.isHidden = false;
            self.deleteAppLogs();
        }else{
            self.mainStackView.isHidden = true;
            
        }
        self.lastRefData.removeAll();
    }
    
    @IBAction func controlScanner(_ sender: UISwitch) {
        if(sender.isOn){
            
            self.sceneView.isHidden = true;
            self.optoutImgView.isHidden = false;
            self.sceneView.session.pause();
        }else{
            self.sceneView.isHidden = false;
            self.optoutImgView.isHidden = true;
            startSession();
        }
    }
  
    @objc func resetLocalAlbum(){
         debugPrint("RESET LOCAL ALBUM")
        // self.lastRefData.removeAll();
    }
    func addPlistKeyValues(key:String, value:String, plistname:String) -> Void{
        
        debugPrint("addPlistKeyValues called.....")
        SwiftyPlistManager.shared.addNew(value, key: key, toPlistWithName: plistname) { (err) in
            if err == nil {
                debugPrint("------------------->  Value '\(value)' successfully saved at Key '\(key)' into '\(plistname).plist'")
            }
        }
    }
    func savePlistKeyValues(key:String, value:String, plistname:String) -> Void{
        
        debugPrint("addPlistKeyValues called.....")
        SwiftyPlistManager.shared.save(value, forKey: key, toPlistWithName: plistname) { (err) in
            if err == nil {
                debugPrint("------------------->  Value '\(value)' successfully saved at Key '\(key)' into '\(plistname).plist'")
            }
        }
    }
    func addPlistKeyValuesBool(key:String, value:Bool, plistname:String) -> Void{
        SwiftyPlistManager.shared.addNew(value, key: key, toPlistWithName: plistname) { (err) in
            if err == nil {
                debugPrint("------------------->  Value '\(value)' successfully saved at Key '\(key)' into '\(plistname).plist'")
            }
        }
    }
    func getRemoteConfigValues(){
        
        debugPrint("Configuring Remote Config Values......")
        let configdata:NSMutableDictionary = NSMutableDictionary();
        configdata.setValue("30",  forKey: "HEAD_ROLL");
        configdata.setValue("30",  forKey: "HEAD_YAW");
        configdata.setValue("30",  forKey: "HEAD_PITCH");
        configdata.setValue("2.0",   forKey: "HEAD_CAPTURE_DISTANCE");
        configdata.setValue("250",  forKey: "DISTANCE_BETWEEN_EYES");
        configdata.setValue("0.5", forKey: "EYE_ROLL");
        
        SwiftyPlistManager.shared.removeAllKeyValuePairs(fromPlistWithName: GlobalConstants.CONFIG_PLIST) { (err) in
            if err != nil {
                debugPrint("Plist Removal eror:",err ?? " ERROR OCCURRED");
            }
        }
        if let headRoll = (configdata["HEAD_ROLL"] as? String) {
            self.addPlistKeyValues(key: "HEAD_ROLL", value: headRoll, plistname: "Config-MWAA")
        }
        if let headYaw = (configdata["HEAD_YAW"] as? String) {
            self.addPlistKeyValues(key: "HEAD_YAW", value: headYaw, plistname: "Config-MWAA")
        }
        if let headPitch = (configdata["HEAD_PITCH"] as? String) {
            self.addPlistKeyValues(key: "HEAD_PITCH", value: headPitch, plistname: "Config-MWAA")
        }
        if let captureDist = (configdata["HEAD_CAPTURE_DISTANCE"] as? String) {
            self.addPlistKeyValues(key: "HEAD_CAPTURE_DISTANCE", value: captureDist, plistname: "Config-MWAA")
        }
        if let distBetwnEyes = (configdata["DISTANCE_BETWEEN_EYES"] as? String) {
            self.addPlistKeyValues(key: "DISTANCE_BETWEEN_EYES", value: distBetwnEyes, plistname: "Config-MWAA")
        }
        if let eyeRoll = (configdata["EYE_ROLL"] as? String) {
            self.addPlistKeyValues(key: "EYE_ROLL", value: eyeRoll, plistname: "Config-MWAA")
        }
        self.populateConfigValues();
       
    }
    func populateConfigValues(){
        
        SwiftyPlistManager.shared.getValue(for: "HEAD_ROLL", fromPlistWithName: "Config-MWAA") { (headRoll, err) in
            if err == nil {
                self.headRollSlider.setValue((headRoll as! NSString).floatValue, animated: false);
                self.headRollLbl.text = "Head Roll: \(self.headRollSlider.value)"
                CBPConstants.sharedInstance.HEAD_ROLL_LOW = -Double(self.headRollSlider.value)
                CBPConstants.sharedInstance.HEAD_ROLL_HIGH = Double(self.headRollSlider.value)
            }
        }
        SwiftyPlistManager.shared.getValue(for: "HEAD_YAW", fromPlistWithName: "Config-MWAA") { (headYaw, err) in
            if err == nil {
                self.headYawSlider.setValue((headYaw as! NSString).floatValue, animated: false)
                self.headYawLbl.text = "Head Yaw: \(self.headYawSlider.value)"
                CBPConstants.sharedInstance.HEAD_YAW_LOW = -Double(self.headYawSlider.value)
                CBPConstants.sharedInstance.HEAD_YAW_HIGH = Double(self.headYawSlider.value)
            }
        }
        SwiftyPlistManager.shared.getValue(for: "HEAD_PITCH", fromPlistWithName: "Config-MWAA") { (headPitch, err) in
            if err == nil {
                self.headPitchSlider.setValue((headPitch as! NSString).floatValue, animated: false);
                self.headPitchLabl.text = "Head Pitch: \(self.headPitchSlider.value)"
                CBPConstants.sharedInstance.HEAD_PITCH_LOW = -Double(self.headPitchSlider.value)
                CBPConstants.sharedInstance.HEAD_PITCH_HIGH = Double(self.headPitchSlider.value)
            }
        }
        SwiftyPlistManager.shared.getValue(for: "HEAD_CAPTURE_DISTANCE", fromPlistWithName: "Config-MWAA") { (captureDist, err) in
            if err == nil {
                self.captureDistanceSlider.setValue((captureDist as! NSString).floatValue, animated: false)
                self.captureDistanceLbl.text = "Capture Distance: \(self.captureDistanceSlider.value)"
                CBPConstants.sharedInstance.CAPTURE_DISTANCE_LOW =  self.captureDistanceSlider.minimumValue
                CBPConstants.sharedInstance.CAPTURE_DISTANCE_HIGH = self.captureDistanceSlider.value
            }
        }
        SwiftyPlistManager.shared.getValue(for: "DISTANCE_BETWEEN_EYES", fromPlistWithName: "Config-MWAA") { (distBetwnEyes, err) in
            if err == nil {
                self.distanceBetweenEyesSlider.setValue((distBetwnEyes as! NSString).floatValue, animated: false);
                self.distanceBetweenEyesLbl.text = "Distance Between Eyes: \(self.distanceBetweenEyesSlider.value)"
                CBPConstants.sharedInstance.DISTANCE_BETWEEN_EYES_LOW =  self.distanceBetweenEyesSlider.minimumValue
                CBPConstants.sharedInstance.DISTANCE_BETWEEN_EYES_HIGH = self.distanceBetweenEyesSlider.value
            }
        }
        SwiftyPlistManager.shared.getValue(for: "EYE_ROLL", fromPlistWithName: "Config-MWAA") { (eyeRoll, err) in
            if err == nil {
                self.eyeRollSlider.setValue((eyeRoll as! NSString).floatValue, animated: false);
                self.eyeRollLabl.text = "Eye Roll: \(self.eyeRollSlider.value)"
                CBPConstants.sharedInstance.EYE_ROLL_LOW =  self.eyeRollSlider.minimumValue
                CBPConstants.sharedInstance.EYE_ROLL_HIGH = self.eyeRollSlider.value
            }
        }
        
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let statuscontroller = segue.destination as! StatusVC
        statuscontroller.statusType = self.scanResult;
        statuscontroller.globalMessage = self.globalMessage;
        
    }
    
}
extension UIImage {
    
    /**
     Creates a new UIImage from a CVPixelBuffer.
     NOTE: This only works for RGB pixel buffers, not for grayscale.
     */
    public convenience init?(pixelBuffer: CVPixelBuffer) {
        var cgImage: CGImage?
        VTCreateCGImageFromCVPixelBuffer(pixelBuffer, options: nil, imageOut: &cgImage)
        
        if let cgImage = cgImage {
            self.init(cgImage: cgImage)
        } else {
            return nil
        }
    }
    
    struct RotationOptions: OptionSet {
        let rawValue: Int
        
        static let flipOnVerticalAxis = RotationOptions(rawValue: 1)
        static let flipOnHorizontalAxis = RotationOptions(rawValue: 2)
    }
    
    func rotated(by rotationAngle: Measurement<UnitAngle>, options: RotationOptions = []) -> UIImage? {
        guard let cgImage = self.cgImage else { return nil }
        
        let rotationInRadians = CGFloat(rotationAngle.converted(to: .radians).value)
        let transform = CGAffineTransform(rotationAngle: rotationInRadians)
        var rect = CGRect(origin: .zero, size: self.size).applying(transform)
        rect.origin = .zero
        
        let renderer = UIGraphicsImageRenderer(size: rect.size)
        return renderer.image { renderContext in
            renderContext.cgContext.translateBy(x: rect.midX, y: rect.midY)
            renderContext.cgContext.rotate(by: rotationInRadians)
            
            let x = options.contains(.flipOnVerticalAxis) ? -1.0 : 1.0
            let y = options.contains(.flipOnHorizontalAxis) ? 1.0 : -1.0
            renderContext.cgContext.scaleBy(x: CGFloat(x), y: CGFloat(y))
            
            let drawRect = CGRect(origin: CGPoint(x: -self.size.width/2, y: -self.size.height/2), size: self.size)
            renderContext.cgContext.draw(cgImage, in: drawRect)
        }
    }
    
    
}
