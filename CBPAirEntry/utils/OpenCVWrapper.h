//
//  OpenCVWrapper.h
//  CBPAirEntry
//
//  Created by Kannan Seenivasagam on 6/14/19.
//  Copyright © 2019 Kannan Seenivasagam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OpenCVWrapper : NSObject

+ (NSString *)openCVVersionString;
+ (UIImage *)convertToGrayscale:(UIImage *)image;
+ (UIImage *)detectEdgesInRGBImage:(UIImage *)image;
+ (UIImage *)blur:(UIImage *)image radius:(double)radius;
+ (UIImage *)getChannel:(UIImage *)image channel:(NSString *)channel;
+ (NSString *)checkImageBlurry:(UIImage *)image;
+ (UIImage *)detect:(UIImage *)source;
+ (NSString *)checkEyeGlasses:(UIImage *)image;

@end

NS_ASSUME_NONNULL_END

