//
//  VeriscanSSLPinner.swift
//  CBPAirEntry
//
//  Created by Kannan Seenivasagam on 9/4/19.
//  Copyright © 2019 Kannan Seenivasagam. All rights reserved.
//

import UIKit
import Foundation
import Security
class VeriscanSSLPinner: NSObject , URLSessionDelegate {
     func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        debugPrint("VeriscanSSLPinner called.....")
        if challenge.protectionSpace.host == "192.168.0.10" {
            completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
        } else {
            completionHandler(.performDefaultHandling, nil)
        }
        
    }
}
