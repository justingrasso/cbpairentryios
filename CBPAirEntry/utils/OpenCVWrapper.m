//
//  OpenCVWrapper.m
//  CBPAirEntry
//
//  Created by Kannan Seenivasagam on 6/14/19.
//  Copyright © 2019 Kannan Seenivasagam. All rights reserved.
//

#import <opencv2/opencv.hpp>
#import "OpenCVWrapper.h"
#import <opencv2/imgcodecs/ios.h>
#import <UIKit/UIKit.h>

@implementation OpenCVWrapper

cv::CascadeClassifier face_cascade;
cv::CascadeClassifier eyes_cascade;
bool cascade_loaded = false;

+ (NSString *)openCVVersionString {
    return [NSString stringWithFormat:@"OpenCV Version %s",  CV_VERSION];
}
+ (NSString *)showKannan {
    return [NSString stringWithFormat:@"OpenCV Version %s",  CV_VERSION];
}

+ (UIImage *)convertToGrayscale:(UIImage *)image {
    cv::Mat mat;
    UIImageToMat(image, mat);
    cv::Mat gray;
    cv::cvtColor(mat, gray, cv::COLOR_BGR2GRAY);
    UIImage *grayscale = MatToUIImage(gray);
    return grayscale;
}

+ (UIImage *)detectEdgesInRGBImage:(UIImage *)image {
    cv::Mat mat;
    UIImageToMat(image, mat);
    cv::Mat gray;
    cv::cvtColor(mat, gray, cv::COLOR_BGR2GRAY);
    //    cv::Laplacian(gray, gray, gray.depth());
    cv::Sobel(gray, gray, gray.depth(), 1, 0);
    UIImage *grayscale = MatToUIImage(gray);
    return grayscale;
}

+ (UIImage *)blur:(UIImage *)image radius:(double)radius {
    cv::Mat mat;
    UIImageToMat(image, mat);
    cv::GaussianBlur(mat, mat, cv::Size(NULL, NULL), radius);
    UIImage *blurredImage = MatToUIImage(mat);
    return blurredImage;
}

+ (UIImage *)getChannel:(UIImage *)image channel:(NSString *)channel {
    cv::Mat mat;
    UIImageToMat(image, mat);
    cv::Mat baseImage = cv::Mat::zeros(mat.size(), CV_8UC3);
    // UIImage is RGB, so our default here is blue
    int ch = 2;
    if ([channel isEqual: @"r"] || [channel isEqual: @"R"]) {
        ch = 0;
    } else if ([channel isEqual: @"g"] || [channel isEqual: @"G"]) {
        ch = 1;
    }
    int from_to[] = { ch,ch };
    cv::mixChannels(&mat, 1, &baseImage, 1, from_to, 1);
    UIImage *retImage = MatToUIImage(baseImage);
    return retImage;
}
+(NSString *) checkImageBlurryZ:(UIImage *)image {
    cv::Mat matImage;
    UIImageToMat(image, matImage);
    cv::Mat matImageGrey;
    cv::cvtColor(matImage, matImageGrey, cv::COLOR_BGR2GRAY);
    cv::Mat dst2;
    UIImageToMat(image, dst2);
    cv::Mat laplacianImage;
    dst2.convertTo(laplacianImage, CV_8UC1);
    cv::Laplacian(matImageGrey, laplacianImage, CV_8U);
    cv::Mat laplacianImage8bit;
    laplacianImage.convertTo(laplacianImage8bit, CV_8UC1);
    unsigned char *pixels = laplacianImage8bit.data;
    // 16777216 = 256*256*256
    int maxLap = -16777216;
    for (int i = 0; i < ( laplacianImage8bit.elemSize()*laplacianImage8bit.total()); i++) {
        if (pixels[i] > maxLap) {
            maxLap = pixels[i];
        }
    }
    // one of the main parameters here: threshold sets the sensitivity for the blur check
    // smaller number = less sensitive; default = 180
    NSLog(@"%d",maxLap);
    int threshold = 100;
    return (maxLap <= threshold) ? [NSString stringWithFormat:@"YES"] : [NSString stringWithFormat:@"NO"];
    
}
+(NSString *) checkImageBlurryY:(UIImage *)image {
    // converting UIImage to OpenCV format - Mat
    cv::Mat matImage = [self convertUIImageToCVMat:image];
    cv::Mat matImageGrey;
    // converting image's color space (RGB) to grayscale
    cv::cvtColor(matImage, matImageGrey, cv::COLOR_BGR2GRAY);
    
    cv::Mat dst2 = [self convertUIImageToCVMat:image];
    cv::Mat laplacianImage;
    dst2.convertTo(laplacianImage, CV_8UC1);
    
    // applying Laplacian operator to the image
    cv::Laplacian(matImageGrey, laplacianImage, CV_8U);
    cv::Mat laplacianImage8bit;
    laplacianImage.convertTo(laplacianImage8bit, CV_8UC1);
    
    unsigned char *pixels = laplacianImage8bit.data;
    
    // 16777216 = 256*256*256
    int maxLap = -16777216;
    for (int i = 0; i < ( laplacianImage8bit.elemSize()*laplacianImage8bit.total()); i++) {
        if (pixels[i] > maxLap) {
            maxLap = pixels[i];
        }
    }
    // one of the main parameters here: threshold sets the sensitivity for the blur check
    // smaller number = less sensitive; default = 180
    int threshold = 180;
    NSLog(@"%d",maxLap);
    return (maxLap <= threshold) ? [NSString stringWithFormat:@"YES"] : [NSString stringWithFormat:@"NO"];
}

+ (cv::Mat)convertUIImageToCVMat:(UIImage *)image {
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = image.size.width;
    CGFloat rows = image.size.height;
    
    cv::Mat cvMat(rows, cols, CV_8UC4); // 8 bits per component, 4 channels (color channels + alpha)
    
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to  data
                                                    cols,                       // Width of bitmap
                                                    rows,                       // Height of bitmap
                                                    8,                          // Bits per component
                                                    cvMat.step[0],              // Bytes per row
                                                    colorSpace,                 // Colorspace
                                                    kCGImageAlphaNoneSkipLast |
                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
    
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);
    
    return cvMat;
}

/*
 Laplacian(gray, laplacianImage, CV_64F);
 Scalar mean, stddev; // 0:1st channel, 1:2nd channel and 2:3rd channel
 meanStdDev(laplacianImage, mean, stddev, Mat());
 double variance = stddev.val[0] * stddev.val[0];
 
 double threshold = 2900;
 
 if (variance <= threshold) {
 // Blurry
 } else {
 // Not blurry
 }
 */
+(NSString *) checkImageBlurry:(UIImage *)image {
    
    
    cv::Mat matImage;
    UIImageToMat(image, matImage);
    cv::Mat matImageGrey;
    cv::cvtColor(matImage, matImageGrey, cv::COLOR_BGR2GRAY);
    cv::Mat dst2;
    UIImageToMat(image, dst2);
    cv::Mat laplacianImage;
    dst2.convertTo(laplacianImage, CV_8UC1);
    cv::Laplacian(matImageGrey, laplacianImage, CV_64F);
    
    cv::Scalar mean, stddev; // 0:1st channel, 1:2nd channel and 2:3rd channel
    meanStdDev(laplacianImage, mean, stddev, cv::Mat());
    double variance = stddev.val[0] * stddev.val[0];
    double threshold = 80;
    NSLog(@"KANNAN:::>%f",variance);
    if (variance <= threshold) {
        return[NSString stringWithFormat:@"YES"] ;
    } else {
        return[NSString stringWithFormat:@"NO"] ;
    }
    
    
}
+(NSString *) checkImageBlurryD:(UIImage *)image {
    
    
    cv::Mat matImage;
    UIImageToMat(image, matImage);
    cv::Mat finalImage;
    cv::Mat matImageGrey;
    cv::cvtColor(matImage, matImageGrey, cv::COLOR_BGRA2GRAY);
    matImage.release();
    
    cv::Mat newEX;
    const int MEDIAN_BLUR_FILTER_SIZE = 15; // odd number
    cv::medianBlur(matImageGrey, newEX, MEDIAN_BLUR_FILTER_SIZE);
    matImageGrey.release();
    
    cv::Mat laplacianImage;
    cv::Laplacian(newEX, laplacianImage, CV_8U); // CV_8U
    newEX.release();
    
    cv::Mat laplacianImage8bit;
    laplacianImage.convertTo(laplacianImage8bit, CV_8UC1);
    laplacianImage.release();
    cv::cvtColor(laplacianImage8bit,finalImage,cv::COLOR_GRAY2BGRA);
    laplacianImage8bit.release();
    
    int rows = finalImage.rows;
    int cols= finalImage.cols;
    char *pixels = reinterpret_cast<char *>( finalImage.data);
    int maxLap = -16777216;
    for (int i = 0; i < (rows*cols); i++) {
        if (pixels[i] > maxLap)
            maxLap = pixels[i];
    }
    
    //int soglia = -6118750;
    
    pixels=NULL;
    finalImage.release();
    
    int threshold = 100;
    NSLog(@"KANNAN:::>%d",maxLap);
    return (maxLap <= threshold) ? [NSString stringWithFormat:@"YES"] : [NSString stringWithFormat:@"NO"];
    
}
+(NSString *) checkImageBlurryX:(UIImage *)image {
    
    //cv::Mat matImage;
    //UIImageToMat(image, matImage);
    cv::Mat matImage = [self convertUIImageToCVMat:image];
    cv::Mat matImageGrey;
    cv::cvtColor(matImage, matImageGrey, cv::COLOR_BGRA2GRAY);
    cv::Mat dst2;
    UIImageToMat(image, dst2);
    cv::Mat laplacianImage;
    dst2.convertTo(laplacianImage, CV_8UC1);
    cv::Laplacian(matImageGrey, laplacianImage, CV_64F);
    cv::Mat laplacianImage8bit;
    laplacianImage.convertTo(laplacianImage8bit, CV_8UC1);
    unsigned char *pixels = laplacianImage8bit.data;
    int maxLap = -16777216;
    
    for (int i = 0; i < ( laplacianImage8bit.elemSize()*laplacianImage8bit.total()); i++) {
        if (pixels[i] > maxLap)
            maxLap = pixels[i];
    }
    
    int soglia = -6118750;
    
    printf("\n maxLap : %i",maxLap);
    
    
    if (maxLap < soglia || maxLap == soglia) {
        NSLog(@"KANNAN:::>%d",maxLap);
        //printf("\n\n***** blur image *****");
        return [NSString stringWithFormat:@"YES"];
    }else {
        NSLog(@"KANNAN:::>%d",maxLap);
        //printf("\nNOT a blur image");
        return[NSString stringWithFormat:@"NO"];
        
    }
    
}
+(NSString *) checkEyeGlasses:(UIImage *)image {
    
    
    
    //img = cv2.GaussianBlur(img, (11,11), 0)
    
    cv::Mat mat;
    UIImageToMat(image, mat);
    cv::GaussianBlur(mat, mat, cv::Size(11,11), 0);
    
    cv::Mat sobel_y;
    //cv::Mat imgFilterResultVertical;
   // cv::Sobel(mat, imgFilterResultVertical, 0, 1, -1);
    
    cv::Mat imgFilterResultVertical;
    cv::Mat imgFilterResultHorizontal;
    cv::Mat imgFilterResultMagnitude;
    
    cv::Sobel(mat, sobel_y, CV_64F, 0, 1, -1);
  /*  cv::convertScaleAbs(sobel_y)
    
    cv::Sobel(mat, imgFilterResultHorizontal, CV_64F, 0, 1, -1);
    cv::magnitude(imgFilterResultVertical, imgFilterResultHorizontal, imgFilterResultMagnitude);
    
    void getGradients(IplImage* original, cv::Mat* gradArray) { cv::Mat original_Mat(original, true);
        // Convert it to gray
        cv::cvtColor( original_Mat, original_Mat, CV_RGB2GRAY );
        //cv::blur(original_Mat, original_Mat, cv::Size(7,7)); /// Generate grad_x and grad_y
        cv::Mat grad_x = cv::Mat::zeros(original->height, original->width, CV_16S);
        cv::Mat grad_y = cv::Mat::zeros(original->height, original->width, CV_16S);
           cv::Mat abs_grad_x = cv::Mat::zeros(original->height, original->width, CV_8U);
         cv::Mat abs_grad_y = cv::Mat::zeros(original->height, original->width, CV_8U);; /// Gradient X
            cv::Sobel(original_Mat, grad_x, CV_16S, 1, 0, 3);
            cv::convertScaleAbs( grad_x, abs_grad_x ); /// Gradient Y cv::Sobel(original_Mat, grad_y, CV_16S, 0, 1, 3); cv::convertScaleAbs( grad_y, abs_grad_y ); uchar* pixelX = abs_grad_x.data; uchar* pixelY = abs_grad_y.data; uchar* grad1 = gradArray[0].data; uchar* grad2 = gradArray[1].data; uchar* grad3 = gradArray[2].data; uchar* grad4 = gradArray[3].data; uchar* grad5 = gradArray[4].data; uchar* grad6 = gradArray[5].data; uchar* grad7 = gradArray[6].data; uchar* grad8 = gradArray[7].data; int count = 0; int min = 999999; int max = 0; for(int i = 0; i < grad_x.rows * grad_x.cols; i++) { int directionRAD = atan2(pixelY[i], pixelX[i]); int directionDEG = directionRAD / PI * 180; if(directionDEG < min){min = directionDEG;} if(directionDEG > max){max = directionDEG;} if(directionDEG >= 0 && directionDEG <= 45) { grad1[i] = 255; count++;} if(directionDEG >= 45 && directionDEG <= 90) { grad2[i] = 255; count++;} if(directionDEG >= 90 && directionDEG <= 135) { grad3[i] = 255; count++;} if(directionDEG >= 135 && directionDEG <= 190) { grad4[i] = 255; count++;} if(directionDEG >= 190 && directionDEG <= 225) { grad5[i] = 255; count++;} if(directionDEG >= 225 && directionDEG <= 270) { grad6[i] = 255; count++;} if(directionDEG >= 270 && directionDEG <= 315) { grad7[i] = 255; count++;} if(directionDEG >= 315 && directionDEG <= 360) { grad8[i] = 255; count++;} if(directionDEG < 0 || directionDEG > 360) { cout<<"Weird gradient direction given in method: getGradients."; } } }

        
    double directionRAD = atan2(pixelY[i], pixelX[i]);
    int directionDEG = (int)(180.0 + directionRAD / M_PI * 180.0);

    
    //cv::imshow('sobel_y', cv::Sobel(mat, gray, 0, 1, -1));
    //sobel_y = cv2.Sobel(img, cv2.CV_64F, 0 ,1 , ksize=-1) #y方向sobel边缘检测
    //sobel_y = cv2.convertScaleAbs(sobel_y) #转换回uint8类型
    //cv2.imshow('sobel_y',sobel_y)
    
    //edgeness = sobel_y #边缘强度矩阵
    
    
    
    cv::Mat gray;
    cv::cvtColor(mat, gray, cv::COLOR_BGR2GRAY);
    
    NSString *face_cascade_name = [[NSBundle mainBundle] pathForResource:@"haarcascade_eye_tree_eyeglasses" ofType:@"xml"];
    
    if(!cascade_loaded){
        std::cout<<"loading ..";
        if( !face_cascade.load( std::string([face_cascade_name UTF8String]) ) ){ printf("--(!)Error loading\n"); return [NSString stringWithFormat:@"NO"] ;};
        cascade_loaded = true;
    }
    
    cv::Size sOcchi(15,15);
    face_cascade.detectMultiScale(mat, faces, 1.1, 2,0,sOcchi);
    printf("\n faces.size() : %lu:::\n",faces.size());*/
    return [NSString stringWithFormat:@"NO"];
}

+ (UIImage *)detect:(UIImage *)source {
    
    std::vector<cv::Rect> faces;
    cv::Mat mat;
    UIImageToMat(source, mat);
    cv::Mat gray;
    cv::cvtColor(mat, gray, cv::COLOR_BGR2GRAY);
    
    NSString *eyes_cascade_name = [[NSBundle mainBundle] pathForResource:@"haarcascade_eye" ofType:@"xml"];
    NSString *face_cascade_name = [[NSBundle mainBundle] pathForResource:@"haarcascade_frontalface_default" ofType:@"xml"];
    
    if(!cascade_loaded){
        std::cout<<"loading ..";
        if( !eyes_cascade.load( std::string([eyes_cascade_name UTF8String]) ) ){ printf("--(!)Error loading\n"); return source;};
        if( !face_cascade.load( std::string([face_cascade_name UTF8String]) ) ){ printf("--(!)Error loading\n"); return source;};
        cascade_loaded = true;
    }
    face_cascade.detectMultiScale(gray, faces, 1.3, 5);
    for( size_t i = 0; i < faces.size(); i++ )
    {
        cv::Point center( faces[i].x + faces[i].width*0.5, faces[i].y + faces[i].height*0.5 );
        ellipse( gray, center, cv::Size( faces[i].width*0.5, faces[i].height*0.5), 0, 0, 360, cv::Scalar( 0, 100, 255 ), 4, 8, 0 );
        
        cv::Mat faceROI = gray( faces[i] );
        std::vector<cv::Rect> eyes;
        
        //-- In each face, detect eyes
        /*  eyes_cascade.detectMultiScale( faceROI, eyes, 1.1, 2, 0 |cv::CASCADE_SCALE_IMAGE, cv::Size(30, 30) );
         
         for( size_t j = 0; j < eyes.size(); j++ )
         {
         cv::Point center( faces[i].x + eyes[j].x + eyes[j].width*0.5, faces[i].y + eyes[j].y + eyes[j].height*0.5 );
         int radius = cvRound( (eyes[j].width + eyes[j].height)*0.25 );
         
         }
         */
    }
    
    return nil;
}

@end

