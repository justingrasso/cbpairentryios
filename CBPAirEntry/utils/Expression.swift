//
//  Expression.swift
//  CBPAirEntry
//
//  Created by Kannan Seenivasagam on 6/14/19.
//  Copyright © 2019 Kannan Seenivasagam. All rights reserved.
//

import UIKit
import ARKit

class Expression: NSObject {
    
    func name() -> String {
        return ""
    }
    func isExpressing(from: ARFaceAnchor) -> Bool {
        
        return false
    }
    func isDoingWrongExpression(from: ARFaceAnchor) -> Bool {
        
        return false
    }
    
}

class SmileExpression: Expression {
    override func name() -> String {
        return "Smile"
    }
    override func isExpressing(from: ARFaceAnchor) -> Bool {
        guard let smileLeft = from.blendShapes[.mouthSmileLeft], let smileRight = from.blendShapes[.mouthSmileRight] else {
            return false
        }
        
        // from testing: 0.5 is a lightish smile, and 0.9 is an exagerrated smile
        return smileLeft.floatValue > 0.5 && smileRight.floatValue > 0.5
    }
}
class JawOpenExpression: Expression {
    override func name() -> String {
        return "Open Jaw"
    }
    override func isExpressing(from: ARFaceAnchor) -> Bool {
        guard let jawOpen = from.blendShapes[.jawOpen] else {
            return false
        }
        
        // from testing: 0.4 is casual open (aka casual breathing)
        return jawOpen.floatValue > 0.7
    }
}

class EyebrowsRaisedExpression: Expression {
    override func name() -> String {
        return "Raise Eyebrows"
    }
    override func isExpressing(from: ARFaceAnchor) -> Bool {
        guard let browInnerUp = from.blendShapes[.browInnerUp] else {
            return false
        }
        
        // from testing: at least 0.9 is raised eyebrows
        return browInnerUp.doubleValue > 0.7
    }
}

class EyeBlinkExpression: Expression {
    override func name() -> String {
        return "Blink"
    }
    override func isExpressing(from: ARFaceAnchor) -> Bool {
        guard let leftBlink = from.blendShapes[.eyeBlinkLeft], let righBlink = from.blendShapes[.eyeBlinkRight] else {
            return false
        }
        return (leftBlink.doubleValue > 0.4) || (righBlink.doubleValue > 0.4)
    }
}

