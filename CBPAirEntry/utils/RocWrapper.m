//
//  RocWrapper.m
//  CBPAirEntry
//
//  Created by Kannan Seenivasagam on 6/21/19.
//  Copyright © 2019 Kannan Seenivasagam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RocWrapper.h"
#import <UIKit/UIKit.h>

@implementation RocWrapper

+ (NSString *)loadRocLicense {
    
    roc_string host_id;
    roc_ensure(roc_get_host_id(&host_id));
    puts(host_id);
    NSLog(@"%s",host_id);
    roc_ensure(roc_free_string(&host_id));
    roc_ensure(roc_initialize([[NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ROC" ofType:@"lic"] encoding:NSUTF8StringEncoding error:nil] UTF8String], NULL));
     return [NSString stringWithFormat:@"NO"];
}

+ (NSDictionary *) faceMetaData:(const uint8_t *)original origimagelen:(size_t) origimagelen{
    
    roc_image image;
    roc_template template;
    size_t adaptive_minimum_size;
    roc_string sunglasses, quality, iod, roll, pitch, yaw, eyeglasses;
    
    roc_string asian, white, Black, Hispanic;
    
    roc_string SpoofAF;
    
    roc_ensure(roc_decode_image(origimagelen, original, ROC_GRAY8, &image));
    roc_ensure(roc_adaptive_minimum_size(image, 0.08f, 36, &adaptive_minimum_size));
    roc_ensure(roc_represent(image, ROC_FRONTAL | ROC_FR | ROC_GLASSES | ROC_DEMOGRAPHICS | ROC_PITCHYAW | ROC_SPOOF_AF , adaptive_minimum_size, 1, 0.02f, &template));
    
    roc_ensure(roc_get_metadata(template, "Sun", &sunglasses));
    roc_ensure(roc_get_metadata(template, "Eye", &eyeglasses));
    roc_ensure(roc_get_metadata(template, "Quality", &quality));
    roc_ensure(roc_get_metadata(template, "IOD", &iod));
    roc_ensure(roc_get_metadata(template, "Roll;", &roll));
    roc_ensure(roc_get_metadata(template, "Yaw", &yaw));
    roc_ensure(roc_get_metadata(template, "Pitch", &pitch));
    
    roc_ensure(roc_get_metadata(template, "Black", &Black));
    roc_ensure(roc_get_metadata(template, "White", &white));
    roc_ensure(roc_get_metadata(template, "Asian", &asian));
    roc_ensure(roc_get_metadata(template, "Hispanic", &Hispanic));
    roc_ensure(roc_get_metadata(template, "SpoofAF", &SpoofAF));
    
    printf("Sun Glasses On: %s\n", sunglasses);
    printf("Eye Glasses On: %s\n", eyeglasses);
    NSMutableDictionary *dict =  [NSMutableDictionary dictionaryWithCapacity:6];
    [dict setValue:[NSString stringWithFormat:@"%s", sunglasses] forKey:@"sunglasses"];
     [dict setValue:[NSString stringWithFormat:@"%s", eyeglasses] forKey:@"eyeglasses"];
    [dict setValue:[NSString stringWithFormat:@"%s", quality] forKey:@"quality"];
    [dict setValue:[NSString stringWithFormat:@"%s", iod] forKey:@"iod"];
    [dict setValue:[NSString stringWithFormat:@"%s", roll] forKey:@"roll"];
    [dict setValue:[NSString stringWithFormat:@"%s", yaw] forKey:@"yaw"];
    [dict setValue:[NSString stringWithFormat:@"%s", pitch] forKey:@"pitch"];
    [dict setValue:[NSString stringWithFormat:@"%s", SpoofAF] forKey:@"SpoofAF"];
    
    printf("asian Glasses On: %s\n", asian);
    printf("white Glasses On: %s\n", white);
    printf("Black Glasses On: %s\n", Black);
    printf("Hispanic Glasses On: %s\n", Hispanic);
    printf("SpoofAF: %s\n", SpoofAF);
    NSLog(@"%@",dict);
    
    roc_ensure(roc_free_string(&sunglasses));
    roc_ensure(roc_free_string(&eyeglasses));
    roc_ensure(roc_free_string(&quality));
    roc_ensure(roc_free_string(&iod));
    roc_ensure(roc_free_string(&roll));
    roc_ensure(roc_free_string(&yaw));
    roc_ensure(roc_free_string(&pitch));
    
    roc_ensure(roc_free_string(&asian));
    roc_ensure(roc_free_string(&white));
    roc_ensure(roc_free_string(&Black));
    roc_ensure(roc_free_string(&Hispanic));
    roc_ensure(roc_free_string(&SpoofAF));
    
   
    
    
    if (template.algorithm_id & ROC_INVALID) {
        return dict;
    }
    return dict;
}

+ (NSDictionary *) faceCompare:(const uint8_t * )tobecompared original:(const uint8_t *)original tobeimagelen:(size_t) tobeimagelen origimagelen:(size_t) origimagelen{
    
    roc_image images[2];
    roc_template templates[2];
    roc_similarity similarity;
    size_t adaptive_minimum_size;
    roc_string sunglasses, quality, iod, roll, pitch, yaw, eyeglasses;
    int i;
    
    // Decode Input Images
    roc_ensure(roc_decode_image(origimagelen, original, ROC_GRAY8, &images[0]));
    roc_ensure(roc_decode_image(tobeimagelen, tobecompared, ROC_GRAY8, &images[1]));
    NSMutableDictionary *dict =  [NSMutableDictionary dictionaryWithCapacity:8];
    for (i=0; i<2; i++) {
        roc_ensure(roc_adaptive_minimum_size(images[i], 0.08f, 36, &adaptive_minimum_size));
        roc_ensure(roc_represent(images[i], ROC_FRONTAL | ROC_FR | ROC_GLASSES | ROC_DEMOGRAPHICS | ROC_PITCHYAW, adaptive_minimum_size, 1, 0.02f, &templates[i]));
        if(i == 0){
            
            roc_ensure(roc_get_metadata(templates[i], "Sun", &sunglasses));
            roc_ensure(roc_get_metadata(templates[i], "Eye", &eyeglasses));
            roc_ensure(roc_get_metadata(templates[i], "Quality", &quality));
            roc_ensure(roc_get_metadata(templates[i], "IOD", &iod));
            roc_ensure(roc_get_metadata(templates[i], "Roll;", &roll));
            roc_ensure(roc_get_metadata(templates[i], "Yaw", &yaw));
            roc_ensure(roc_get_metadata(templates[i], "Pitch", &pitch));
            printf("Sun Glasses On: %s\n", sunglasses);
            printf("Eye Glasses On: %s\n", eyeglasses);
            
            [dict setValue:[NSString stringWithFormat:@"%s", sunglasses] forKey:@"sunglasses"];
            [dict setValue:[NSString stringWithFormat:@"%s", eyeglasses] forKey:@"eyeglasses"];
            [dict setValue:[NSString stringWithFormat:@"%s", quality] forKey:@"quality"];
            [dict setValue:[NSString stringWithFormat:@"%s", iod] forKey:@"iod"];
            [dict setValue:[NSString stringWithFormat:@"%s", roll] forKey:@"roll"];
            [dict setValue:[NSString stringWithFormat:@"%s", yaw] forKey:@"yaw"];
            [dict setValue:[NSString stringWithFormat:@"%s", pitch] forKey:@"pitch"];
            
            roc_ensure(roc_free_string(&sunglasses));
            roc_ensure(roc_free_string(&eyeglasses));
            roc_ensure(roc_free_string(&quality));
            roc_ensure(roc_free_string(&iod));
            roc_ensure(roc_free_string(&roll));
            roc_ensure(roc_free_string(&yaw));
            roc_ensure(roc_free_string(&pitch));
        
        }
        if (templates[i].algorithm_id & ROC_INVALID) {
            return dict;
        }
    }
    roc_ensure(roc_compare_templates(templates[0], templates[1], &similarity));
    printf("Similarity: %g\n", similarity);
    float simval = 0;
    simval = similarity;
    // Cleanup
    for (i=0; i<2; i++) {
        roc_ensure(roc_free_template(&templates[i]));
        roc_ensure(roc_free_image(images[i]));
    }
    [dict setValue:[NSString stringWithFormat:@"%f", simval] forKey:@"similarity"];
    NSLog(@"%@",dict);
    return dict;
}
+ (float) verify:(const uint8_t * )tobecompared original:(const uint8_t *)original tobeimagelen:(size_t) tobeimagelen origimagelen:(size_t) origimagelen{
    
    const int maximum_faces = 1;
    const int maximum_candidates = 1;
    roc_gallery gallery = NULL;
    roc_template *gallery_templates;
    roc_template probe, candidate_template;
    roc_candidate candidate;
    roc_candidate *candidates;
    roc_string quality;
    size_t adaptive_minimum_size;
    int i;
    
    roc_image srcimage;
    roc_decode_image(origimagelen, original, ROC_GRAY8, &srcimage);
    
    roc_image tobeimage;
    roc_decode_image(tobeimagelen, tobecompared, ROC_GRAY8, &tobeimage);
    
    // Open both images
   
    roc_ensure(roc_open_gallery(NULL, &gallery, NULL));
    
   
    gallery_templates = (roc_template*) malloc(maximum_faces * sizeof(roc_template));
    roc_ensure(roc_adaptive_minimum_size(srcimage, 0.08f, 36, &adaptive_minimum_size));
    roc_ensure(roc_represent(srcimage, ROC_FRONTAL | ROC_FR, adaptive_minimum_size, maximum_faces, 0.02f, gallery_templates));
    
    for (i=0; i<maximum_faces; i++) {
        if (gallery_templates[i].algorithm_id & ROC_INVALID) {
            if (i == 0)
                roc_ensure("Failed to find a face in the gallery image!");
            break;
        }
        roc_ensure(roc_enroll(gallery, gallery_templates[i]));
    }
    
    // Find a single face in the probe image
    roc_ensure(roc_adaptive_minimum_size(tobeimage, 0.08f, 36, &adaptive_minimum_size));
    roc_ensure(roc_represent(tobeimage, ROC_FRONTAL | ROC_FR, adaptive_minimum_size, 1, 0.02f, &probe));
    if (probe.algorithm_id & ROC_INVALID)
        roc_ensure("Failed to find a face in the probe image!");
    roc_ensure(roc_get_metadata(probe, "Quality", &quality));
    printf("Probe Quality: %s\n", quality);
    roc_ensure(roc_free_string(&quality));
    
    candidates = (roc_candidate*) malloc(maximum_candidates * sizeof(roc_candidate));
    roc_ensure(roc_search(gallery, probe, maximum_candidates, 0.0f, candidates));
    puts("Similarity\tX\tY\tWidth\tHeight");
    float simval = 0;
    for (i=0; i<maximum_candidates; i++) {
        candidate = candidates[i];
        if (candidate.index == ROC_INVALID_TEMPLATE_INDEX)
            break;
        
        roc_ensure(roc_at(gallery, candidate.index, &candidate_template));
        simval = candidate.similarity;
        printf("%g\t%d\t%d\t%d\t%d\n", candidate.similarity, candidate_template.x, candidate_template.y, candidate_template.width, candidate_template.height);
        roc_ensure(roc_free_template(&candidate_template));
    }
    
    
    size_t gsize;
    roc_size(gallery, &gsize);
    printf("GALLERY SIZE::%zu\t\n", gsize);
    
    printf("%g\t\n", simval);
    
    // Cleanup
    roc_ensure(roc_free_image(srcimage));
    roc_ensure(roc_free_image(tobeimage));
    roc_ensure(roc_close_gallery(gallery));
    for (i=0; i<maximum_faces; i++){
        roc_ensure(roc_free_template(&gallery_templates[i]));
    }
    roc_ensure(roc_free_template(&probe));
    free(gallery_templates);
    free(candidates);
    //roc_ensure(roc_finalize());
    
    return simval;
}

@end
