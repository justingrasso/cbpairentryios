//
//  AppLog+CoreDataProperties.swift
//  
//
//  Created by Kannan Seenivasagam on 9/24/19.
//
//

import Foundation
import CoreData


extension AppLog {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AppLog> {
        return NSFetchRequest<AppLog>(entityName: "AppLog")
    }

    @NSManaged public var message: String?
    @NSManaged public var messagetype: String?
    @NSManaged public var transend: String?
    @NSManaged public var transstart: String?

}
