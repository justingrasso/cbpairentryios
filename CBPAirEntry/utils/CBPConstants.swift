//
//  CBPConstants.swift
//  CBPAirEntry
//
//  Created by Kannan Seenivasagam on 6/14/19.
//  Copyright © 2019 Kannan Seenivasagam. All rights reserved.
//

import UIKit

class CBPConstants: NSObject {
    
    static let sharedInstance = CBPConstants();
    
    private override init() {
        print("CBP CONSTANTS SINGLETON")
    }
    var HEAD_YAW_HIGH:Double                    =  30.0
    var HEAD_YAW_LOW:Double                     = -30.0
    var HEAD_ROLL_HIGH:Double                   =  30.0
    var HEAD_ROLL_LOW:Double                    = -30.0
    var HEAD_PITCH_HIGH:Double                  =  30.0
    var HEAD_PITCH_LOW:Double                   = -30.0
    var CAPTURE_DISTANCE_HIGH:Float             =  3.0
    var CAPTURE_DISTANCE_LOW:Float              =  0.1
    var DISTANCE_BETWEEN_EYES_HIGH:Float        =  400.0
    var DISTANCE_BETWEEN_EYES_LOW:Float         =  80.0
    var EYE_ROLL_HIGH:Float                     =  1.0
    var EYE_ROLL_LOW:Float                      =  0.1
    
    var TRANSACTION_TYPE:String                 =  "Network camera submission"
    var ENCOUNTER_PORT_CODE:String              =  "IAD"
    var ENCOUNTER_PORT_TYPE:String              =  "AIR"
    var SITE_ID:String                          =  "A541"
    var TERMINAL_ID:String                      =  "%Z37"
    var WORKSTATION_ID:String                   =  "WSTG01A04F"
    var CAMERA_VENDOR:String                    =  "APPLE"
    var CAMERA_MAKE:String                      =  "IPad Pro"
    var CAMERA_MODEL:String                     =  "GEN3"
    var CAMERA_SERIAL_NUMBER:String             =  "X2097P1"
    var CAMERA_FIRMWARE:String                  =  "NONE"
    var IMG_ANALYSIS_SOFTWARE_VENDOR:String     =  "MWAA"
    var IMG_ANALYSIS_SOFTWARE_PRODUCT:String    =  "VERISCAN"
    var IMG_ANALYSIS_SOFTWARE_VERSION:String    =  "1.0.0"
    var IMG_COMPRESSION_TYPE:String             =  "JPEG"
    var SVC_TOKEN                               =  "231e987-d25g-h357"
    
}
