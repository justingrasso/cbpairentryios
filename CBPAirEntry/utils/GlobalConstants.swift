//
//  GlobalConstants.swift
//  CBPAirEntry
//
//  Created by Kannan Seenivasagam on 6/16/19.
//  Copyright © 2019 Kannan Seenivasagam. All rights reserved.
//

import Foundation

struct GlobalConstants {
   
    static let CONFIG_PLIST:String                = "Config-MWAA"
    //static let AIR_ENTRY_VERIFY_URL:String        = "https://veritime.veri-scan.com/veritime"
    static let AIR_ENTRY_VERIFY_URL:String        = "https://52.22.252.58/veritime"
    //static let AIR_ENTRY_VERIFY_URL:String        = "http://192.168.0.10/UnisysBroker/api/bio"
    //static let AIR_ENTRY_VERIFY_URL:String        = "http://192.168.0.10/AirEntryImageCaptureBrokerService/api/bio"
    static let APP_PASS_RESET_INTRVL:Double       = 120
    static let REQ_TIMEOUT:Double                 = 10
    
}

