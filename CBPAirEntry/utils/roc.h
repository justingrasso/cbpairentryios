// Copyright © 2015-2019 Rank One Computing Corporation. All rights reserved.

#ifndef ROC_H
#define ROC_H

#include <float.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#if defined ROC_LIBRARY || defined BR_LIBRARY
#  if defined _WIN32 || defined __CYGWIN__
#    define ROC_EXPORT __declspec(dllexport)
#  else
#    define ROC_EXPORT __attribute__((visibility("default")))
#  endif
#else
#  if defined _WIN32 || defined __CYGWIN__
#    define ROC_EXPORT __declspec(dllimport)
#  else
#    define ROC_EXPORT
#  endif
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if defined(TARGET_OS_IPHONE) && TARGET_OS_IPHONE
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdocumentation"
#endif // defined(TARGET_OS_IPHONE) && TARGET_OS_IPHONE

/*!
 * \mainpage
 * \section introduction Introduction
 * Welcome to the ROC SDK documentation!
 * Please start by reading \ref installation_notes.
 * Questions should be addressed to support@rankone.io.
 * There are four primary ways to use our software:
 *
 * \ref api <br>
 * \copybrief api
 *
 * \ref api_wrappers <br>
 * \copybrief api_wrappers
 *
 * \ref cli <br>
 * \copybrief cli
 *
 * \ref explore <br>
 * \copybrief explore
 *
 * \subsection template_compatibility Template Compatibility
 * Please note that templates <b>are not compatible</b> between different
 * \ref ROC_VERSION_MINOR, and must be re-built from their images to upgrade.
 * Templates <b>are compatible</b> between our different supported platforms for
 * the same SDK version, and between different \ref ROC_VERSION_PATCH which is
 * used to indicate bug fixes.
 *
 * \subsection important_vocabulary Important Vocabulary
 * | Word / Phrase                                          | Definition                           |
 * |--------------------------------------------------------|--------------------------------------|
 * | \ref roc_represent "represent"                         | \copybrief roc_represent             |
 * | \ref roc_template "template"                           | \copybrief roc_template              |
 * | \ref roc_compare_templates "compare"                   | \copybrief roc_compare_templates     |
 * | \ref roc_similarity "similarity"                       | \copybrief roc_similarity            |
 * | \ref roc_gallery "gallery"                             | \copybrief roc_gallery               |
 * | \ref roc_gallery_file "gallery file"                   | \copybrief roc_gallery_file          |
 * | \ref face_quality "face quality"                       | \copybrief face_quality              |
 * | \ref face_liveness "liveness"                          | \copybrief face_liveness             |
 * | \ref roc_adaptive_minimum_size "adaptive minimum size" | \copybrief roc_adaptive_minimum_size |
 * | \ref ROC_THUMBNAIL "thumbnail"                         | \copybrief ROC_THUMBNAIL             |
 * | \ref threads "threads"                                 | The maximum concurrent threads.      |
 * | \ref roc_template_limit "template limit"               | \copybrief roc_template_limit        |
 *
 * \subsection license_tiers License Tiers
 * |                           | Demographics | Enrollment | Verification | Search |
 * |---------------------------|--------------|------------|--------------|--------|
 * | \ref versioning           | x            | x          | x            | x      |
 * | \ref error_handling       | x            | x          | x            | x      |
 * | \ref initialization       | x            | x          | x            | x      |
 * | \ref media_decoding       | x            | x          | x            | x      |
 * | \ref video_streaming      | x            | x          | x            | x      |
 * | \ref template_generation  | 1            | x          | x            | x      |
 * | \ref template_io          |              | x          | x            | x      |
 * | \ref gallery_construction |              | 2          | 2            | x      |
 * | \ref comparison           |              |            | 3            | x      |
 * | \ref search               |              |            |              | x      |
 * | \ref tracking             |              | 4          | 4            | x      |
 * | \ref identity_discovery   |              |            |              | x      |
 * | \ref remote_galleries     |              | 5          | 5            | x      |
 *
 * -# Enabled except for \ref ROC_FR, \ref ROC_FR_FAST, \ref ROC_SPOOF_AF and
 *    \ref ROC_SPOOF_FF.
 * -# Disabled, use \ref template_io for serializing templates in the
 *    \ref roc_gallery_file format.
 * -# Enabled except for similarity matrix construction with
 *    \ref roc_compare_galleries.
 * -# Enabled except for offline-tracking with \ref roc_track.
 * -# Can access a remote gallery with \ref roc_open_gallery but not host one
 *    with \ref roc_serve.
 *
 * \par Static vs. Floating Licenses
 * Most licenses are \em statically tied individual machines using
 * \ref roc_get_host_id.
 * If you are looking to set up a \em floating license server that allows the
 * transfer of licenses between machines see \ref roc-license-server.
 *
 * \par Software Updates
 * Software updates are available to customers with an active
 * maintainance plan.
 *
 * \subsection copyright Copyright
 * Copyright © 2015-2019 Rank One Computing Corporation.
 * All rights reserved.
 *
 * Unless otherwise indicated, all materials on these pages are copyrighted by
 * Rank One Computing Corporation.
 * All rights reserved.
 * No part of these pages, either text or image may be used for any purpose
 * other than as permitted in a valid and current license agreement entered into
 * with Rank One Computing Corporation.
 * Therefore, reproduction, modification, storage in a retrieval system or
 * retransmission, in any form or by any means, electronic, mechanical or
 * otherwise is strictly prohibited without prior written permission.
 *
 * \page installation_notes Installation Notes
 * \section supported_platforms Supported Platforms
 * The ROC SDK is supported on a wide variety of operating systems and CPU
 * architectures.
 * The SDK archive names use the following convention:
 *
 * <tt>roc-</tt><i>OperatingSystem</i><tt>-</tt><i>CPUArchitecture</i><tt>-</tt><i>VectorExtension</i><tt>-</tt><i>SDKVersion</i>
 *
 * \note If you receive an <b>illegal instruction</b> error, this indicates
 *       that you are attempting to run an instance of our SDK whose CPU
 *       architecture or vector extension is not supported by your computer.
 *       In most cases this can be resolved by downloading the correct flavor of
 *       our SDK.
 *       The <tt>FMA</tt> vector extension is available on nearly all
 *       <tt>x64</tt> CPUs since 2014, and <tt>SSE</tt> is the legacy fallback
 *       alternative.
 *       In some cases we can't support CPUs pre-dating 2011.
 *
 * \section license_generation License Generation
 * \par Windows
 * Double-click <tt>bin\\roc-host-id.exe</tt> to run this command and email the
 * file <tt>roc-host-id.json</tt> that it creates to support@rankone.io.
 *
 * \par Linux & OS X
 * Email the output of this command to support@rankone.io:
 *
 *     $ ./bin/roc-host-id
 *
 * \par Android (<= 7)
 * Email the output of this command to support@rankone.io:
 *
 *     $ adb shell settings get secure android_id
 *
 * \par Android (>= 8)
 * To generate a valid license file from your application, email the output of
 * \ref roc_get_host_id to <tt>support@rankone.io</tt>.
 *
 * \par iOS
 * To generate a valid license file call the ROC SDK function
 * \ref roc_get_host_id from your application.
 * Email the output to <tt>support@rankone.io</tt>.
 *
 * \subsection license_generation_all_platforms All Platforms
 * We will respond to your email with a valid <tt>ROC.lic</tt> file to copy to
 * the ROC SDK root directory, overwriting the placeholder license file of the
 * same name.
 *
 * \note If you intend to use the \ref cli or \ref explore, export the
 * environment variable <tt>ROC_LIC</tt> to the full path to the
 * <tt>ROC.lic</tt> license file.
 *
 * \subsection threads Threads
 * License files include a \em threads value which is the maximum number of
 * concurrent calls that can be made to ROC SDK functions.
 * If the limit is exceeded, later threads will block until earlier threads have
 * returned.
 *
 * The thread count limit can be queried with \ref roc_get_thread_limit and set
 * with \ref roc_set_thread_limit.
 * Most SDK functions are single-threaded and safe to call concurrently, look
 * for \ref thread-safe, \ref reentrant or \ref thread-unsafe markings in the
 * documentation for each function.
 * A few trivially parallelizable functions are marked \ref parallel, indicating
 * that they will use more than one thread when beneficial.
 * Parallel functions are counted against the thread limit in accordance with
 * the number of threads they utilize.
 *
 * \section dongles Dongles
 * If your license requires a USB hardware dongle, there is a driver provided in
 * the <tt>share</tt> folder that must be installed to recognize it.
 *
 * \par Windows
 * Run <tt>HASPUserSetup.exe</tt>.
 *
 * \par Linux
 *
 *     $ sudo ./install_32bit_compatibility_package_for_x64.sh
 *     $ sudo dpkg -i --force-architecture aksusbd_7.60-1_i386.deb
 *
 * \par OS X
 * Run <tt>Sentinel_Runtime.dmg</tt>.
 *
 * \par Android & iOS
 * Not supported.
 *
 * \section video_decoding_dependencies Video Decoding Dependencies
 * An additional installation step is required \em only for users in need of
 * \ref video_decoding or \ref explore.
 *
 * \par Windows
 * No additional installation step.
 *
 * \par Linux
 *
 *     $ sudo apt-get update
 *     $ ./install_ffmpeg.sh
 *
 * \par OS X
 *
 *     $ brew install https://raw.githubusercontent.com/Homebrew/homebrew-core/1b62cb95ea3acc6386e1a59e4f46b9fe8ff41ff3/Formula/ffmpeg.rb
 *
 * \par Android & iOS
 * Not supported.
 *
 * \section evaluation Evaluation
 * The <tt>Makefile</tt> in the root of the SDK is a turn-key solution for
 * evaluating ROC algorithms on the
 * [LFW](http://vis-www.cs.umass.edu/lfw/) and
 * [MEDS](http://www.nist.gov/itl/iad/ig/sd32.cfm) datasets.
 * See comments in the Makefile for details.
 *
 *     $ make
 *
 * \section acknowledgements Acknowledgements
 * The ROC SDK uses the following software packages:
 *
 * | Name   | License                                                            | Source                          |
 * |--------|--------------------------------------------------------------------|---------------------------------|
 * | OpenBR | [Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0)           | http://www.openbiometrics.org   |
 * | Qt     | [LGPL 3.0](http://www.gnu.org/licenses/lgpl-3.0.html)              | http://www.qt.io                |
 * | OpenCV | [BSD 3-clause](http://opensource.org/licenses/BSD-3-Clause)        | http://www.opencv.org           |
 * | Eigen  | [MPL 2.0](https://www.mozilla.org/en-US/MPL/2.0/)                  | http://www.eigen.tuxfamily.org  |
 * | FFMPEG | [LGPL 2.1](http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html) | http://www.ffmpeg.org           |
 * | Caffe  | [BSD 2-Clause](https://github.com/BVLC/caffe/blob/master/LICENSE)  | http://caffe.berkeleyvision.org |
 *
 * \page api_wrappers API Wrappers
 * \brief \ref java, \ref csharp, \ref python and \ref go interfaces that wrap
 *        our C API, and information on setting up \ref visual_studio,
 *        \ref android and \ref ios projects.
 *
 * \section api_wrappers_overview Overview
 * The ROC SDK ships with API wrappers for the \c Java, \c C#, \c Python and
 * \c Go programming languages.
 * The wrappers live in the \c java, \c csharp, \c python and \c go
 * subdirectories of the SDK respectively.
 * In addition to the language-specific interface files, each folder also
 * contains translations of the C <tt>examples</tt> programs prefixed with
 * <tt>roc_example_</tt>.
 * Lastly, source code for the API translation is provided in the \c roc_swig.c
 * and \c roc_swig_video.c files as a reference for resolving specific technical
 * questions.
 *
 * \subsection api_wrappers_reproducibility Reproducibility
 * The wrappers are generated automatically using [SWIG](http://www.swig.org/),
 * thus it is trivial to reproduce them or generate new wrappers for other
 * programming languages.
 * The general syntax is:
 * \code{.sh}
 * $ cd include
 * $ swig -<language> -module roc roc.h
 * $ swig -<language> -module roc_video -DSWIG_ROC_VIDEO roc.h
 * \endcode
 *
 * To silence Warning 451, which is not a concern in this case, add
 * <tt>-w451</tt> to the <tt>swig</tt> command.
 *
 * \subsection api_wrappers_help Help
 * To the extent possible, symbol names are identical between the wrappers and
 * the C API, thus the \ref api documentation should continue to serve as the
 * primary reference.
 * A reading of [SWIG Basics](http://www.swig.org/Doc3.0/SWIG.html) and the
 * language specific documentation for
 * [Java](http://www.swig.org/Doc3.0/Java.html),
 * [C#](http://www.swig.org/Doc3.0/CSharp.html) and
 * [Python](http://www.swig.org/Doc3.0/Python.html) is strongly encouraged.
 *
 * The most difficult aspect of these wrappers is understanding how SWIG handles
 * pointers and arrays.
 * Cross-referencing the following documents will offer clarity:
 * - The SWIG documentation for
 *   <a href="http://www.swig.org/Doc3.0/Library.html#Library_nn4">
 *   <tt>\%pointer_functions</tt></a> and
 *   <a href="http://www.swig.org/Doc3.0/Library.html#Library_carrays">
 *   <tt>\%array_functions</tt></a>.
 * - The use of \c \%pointer_functions and \c \%array_functions in \ref roc.h.
 * - The provided examples that make use of these functions.
 *
 * If in doubt, please direct questions to \c support@rankone.io.
 *
 * \subsection exporting_path Exporting Path
 * Before executing the examples you will need to ensure that your system can
 * find the ROC software libraries.
 *
 * \par Windows
\code{.sh}
set PATH=..\bin;%PATH%
\endcode
 *
 * \par Linux
\code{.sh}
export LD_LIBRARY_PATH=../lib
\endcode
 *
 * \par OS X
\code{.sh}
export DYLD_LIBRARY_PATH=../lib
\endcode
 *
 * \section java Java
 * Incorporating the ROC SDK into your development environment takes two steps:
 * -# Copy ROC SDK's <tt>java/io</tt> folder into your project source tree.
 * -# Set <tt>-Djava.library.path="/path/to/ROC_SDK/java"</tt> in your IDE's VM
 *    settings.
 *
 * Make sure the line <tt>static { System.loadLibrary("_roc"); }</tt> occurs at
 * the top of your class, before any static variables referencing ROC SDK
 * values.
 *
 * \subsection java_examples Examples
 * \code{.sh}
cd java
javac *.java
java -Djava.library.path=. roc_example_convert_image ../data/josh_1.jpg
java -Djava.library.path=. roc_example_verify ../data/josh_1.jpg ../data/josh_2.jpg
java -Djava.library.path=. roc_example_search ../data/roc.jpg ../data/josh_2.jpg
java -Djava.library.path=. roc_example_flatten ../data/josh_1.jpg
java -Djava.library.path=. roc_example_track ../data/josh.mp4
java -Djava.library.path=. roc_example_host_id
\endcode
 *
 * \section csharp C#
 * \subsection visual_studio Visual Studio
 * The C# wrapper is compatible with Visual Studio 2015 and later.
 * From the Visual Studio Solution Explorer, add the <tt>.cs</tt> files provided
 * in the <tt>csharp</tt> directory into your project tree, excluding those that
 * begin with <tt>roc_example_</tt>.
 * Developers who have not integrated a native third-party library into their
 * application before should take careful note of the following common pitfalls:
 *  - Decide if you are running a 32-bit or 64-bit application and make sure
 *    to download a copy of the ROC SDK for the correct architecture (x86 or
 *    x64 respectively).
 *  - Double check your project settings to confirm you are in fact running a
 *    32-bit or 64-bit application.
 *  - Ensure that <tt>bin/roc.dll</tt> and the other DLLs in this folder can
 *    be discovered at runtime by either appending this directory to your
 *    \c PATH environment variable or by copying all the DLLs in this folder to
 *    your application's working directory.
 *
 * \subsection mono Mono
 * - When calling \c mono on OS X include the <tt>\-\-arch=64</tt> flag.
 * - When calling \c mono on Windows x64 there is no support for
 *   <tt>-pkg:dotnet</tt> so remove this flag and delete the file
 *   <tt>roc_example_convert_image.cs</tt> which can't be compiled for this
 *   platform.
 *   This example does work when compiled with \ref visual_studio.
 *
 * \subsection csharp_examples Examples
 * Using the [Mono](http://www.mono-project.com/) compiler.
\code{.sh}
cd csharp
mcs -pkg:dotnet -platform:x64 *.cs -m:roc_example_convert_image -out:roc_example_convert_image.exe
mcs -pkg:dotnet -platform:x64 *.cs -m:roc_example_verify -out:roc_example_verify.exe
mcs -pkg:dotnet -platform:x64 *.cs -m:roc_example_search -out:roc_example_search.exe
mcs -pkg:dotnet -platform:x64 *.cs -m:roc_example_flatten -out:roc_example_flatten.exe
mcs -pkg:dotnet -platform:x64 *.cs -m:roc_example_track -out:roc_example_track.exe
mcs -pkg:dotnet -platform:x64 *.cs -m:roc_example_host_id -out:roc_example_host_id.exe
mono roc_example_convert_image.exe ../data/josh_1.jpg
mono roc_example_verify.exe ../data/josh_1.jpg ../data/josh_2.jpg
mono roc_example_search.exe ../data/roc.jpg ../data/josh_2.jpg
mono roc_example_flatten.exe ../data/josh_1.jpg
mono roc_example_track.exe ../data/josh.mp4
mono roc_example_host_id.exe
\endcode
 *
 * \section python Python
 * - Supports Python 2.7 only.
 * - On Windows be sure to use a 64-bit installation of Python.
 * - On OS X be sure to use the Python executable supplied by Apple:
 *   `/usr/bin/python`.
 * - The `roc_example_convert_image.py` example requires an installation of
 *   [Pillow](https://python-pillow.org/).
 *
 * \subsection homebrew_python Homebrew Python
 * By default the ROC SDK Python wrapper links against Apple's Python
 * installation.
 * To use Homebrew's Python installation:
\code{.sh}
cd roc-osx-x64-sse2/python
install_name_tool -change /System/Library/Frameworks/Python.framework/Versions/2.7/Python /usr/local/Cellar/python/2.7.13/Frameworks/Python.framework/Versions/2.7/lib/libpython2.7.dylib _roc.so
install_name_tool -change /System/Library/Frameworks/Python.framework/Versions/2.7/Python /usr/local/Cellar/python/2.7.13/Frameworks/Python.framework/Versions/2.7/lib/libpython2.7.dylib _roc_video.so
\endcode
 * Changing `2.7.13` to the version installed on your machine.
 *
 * \subsection python_examples Examples
\code{.sh}
cd python
python roc_example_convert_image.py ../data/josh_1.jpg
python roc_example_verify.py ../data/josh_1.jpg ../data/josh_2.jpg
python roc_example_search.py ../data/roc.jpg ../data/josh_2.jpg
python roc_example_flatten.py ../data/josh_1.jpg
python roc_example_track.py ../data/josh.mp4
python roc_example_host_id.py
\endcode
 *
 * \section go Go
 * - Unlike the other language wrappers, the Go wrapper does not use SWIG, but
 *   instead makes use of the excellent built-in
 *   [cgo](https://golang.org/cmd/cgo/) bridge.
 * - Windows users must install a copy of GCC to use cgo and add it to their
 *   \c PATH environment variable. We recommend
 *   [this MinGW-w64 build](https://downloads.sourceforge.net/project/mingw-w64/Toolchains%20targetting%20Win64/Personal%20Builds/rubenvb/gcc-4.8-release/x86_64-w64-mingw32-gcc-4.8.0-win64_rubenvb.7z).
 *
 * \subsection go_examples Examples
\code{.sh}
cd go
export CGO_CFLAGS=-I../include
export CGO_LDFLAGS=-L../lib
go run roc_example_convert_image.go ../data/josh_1.jpg
go run roc_example_verify.go ../data/josh_1.jpg ../data/josh_2.jpg
go run roc_example_search.go ../data/roc.jpg ../data/josh_2.jpg
go run roc_example_flatten.go ../data/josh_1.jpg
go run roc_example_track.go ../data/josh.mp4
go run roc_example_host_id.go
\endcode
 *
 * \section android Android
 * - The Android SDK is provided as a native binary for \c arm32v7a and
 *   \c arm64v8a CPUs, therefore it will \em not work in the simulator.
 * - An APK for testing the \ref face_liveness algorithm will be made available
 *   upon request. Source code for this application can be found inside the
 *   `java/android_liveness` folder of the SDK.
 * - The ROC SDK function \ref roc_get_host_id uses Android's
 *   [<b>ANDROID_ID</b>](https://developer.android.com/reference/android/provider/Settings.Secure.html#ANDROID_ID)
 *   setting as the <i>Host ID</i>.
 *   Please read the linked Google documentation for this value to understand
 *   the limitations of this approach.
 *   You will need a new license file if the value of this setting changes.
 *
 * The following steps cover how to add the ROC SDK to an Android Studio
 * project:
 * -# Copy the <tt>roc-android/java/io/rankone/rocsdk</tt> folder into your
 *    project directory <tt>app/src/main/java/io/rankone/rocsdk</tt>.
 * -# Copy the <tt>.so</tt> files in <tt>roc-android/lib</tt> and
 *    <tt>roc-android/java</tt> into your project directory
 *    <tt>app/src/main/jniLibs/armeabi-v7a</tt> if using the 32-bit SDK, or
 *    <tt>app/src/main/jniLibs/arm64-v8a</tt> if using the 64-bit SDK.
 * -# Copy the <tt>roc-android/java/QtAndroid-bundled.jar</tt> file into your
 *    project directory <tt>app/libs</tt>.
 * -# In the file <tt>app/build.gradle</tt>, inside the object
 *    <tt>android.defaultConfig</tt>, add the line:
\code{.gradle}
ndk { abiFilters 'armeabi-v7a' } // if using the 32-bit SDK
ndk { abiFilters 'arm64-v8a' } // if using the 64-bit SDK
\endcode
 * Also confirm the line:
\code{.gradle}
compile fileTree(include: ['*.jar'], dir: 'libs')
\endcode
 * exists inside the <tt>dependencies</tt> object, adding it if necessary.
 * -# After obtaining a valid <tt>ROC.lic</tt> license file, copy it into your
 *    project directory <tt>app/src/main/assets</tt>.
 * -# Note the following important lines from the provided Java example
 *    applications to import the ROC module.
\code{.java}
import io.rankone.rocsdk.*;
public class roc_example {
    static {
        System.loadLibrary("_roc");
    }
\endcode
 * -# Pass the contents of <tt>ROC.lic</tt> to \ref roc_initialize.
\code{.java}
String readAssetFile(String fileName)
{
    try {
        InputStream is = getAssets().open(fileName);
        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();
        return new String(buffer);
    } catch (IOException ioe) {
        return new String();
    }
}

void init()
{
    roc.roc_ensure(roc.roc_preinitialize_android(this)); // Where "this" is an Android Activity class
    roc.roc_ensure(roc.roc_initialize(readAssetFile("ROC.lic"), null));
}
\endcode
 *
 * \subsection android_camera_preview Camera Preview
 * Note that the \ref roc_convert_nv21 function is offered as an efficient way
 * to use images from the camera preview in the ROC SDK.
 *
 * \section ios iOS
 * - The iOS SDK is provided as a native binary for \c arm64 CPUs, therefore it
 *   will \em not work in the simulator.
 * - The ROC SDK for iOS supports only \c arm64 devices running iOS 7 or later.
 *   This covers any device released since September 2013.
 * - The ROC SDK function \ref roc_get_host_id calls Apple's
 *   [<b>identifierForVendor</b>](https://developer.apple.com/reference/uikit/uidevice/1620059-identifierforvendor)
 *   function to obtain the <i>Host ID</i>.
 *   Please read the linked Apple documentation for this function to understand
 *   the limitations of this approach.
 *   You will need a new license file if the value returned by this function
 *   changes.
 *
 * The following steps cover how to add the ROC SDK to an XCode Objective-C
 * project:
 * -# Resign <tt>lib/libroc.\<version\>.dylib</tt> with your developer
 *    certificate:
 *    \code
 *    $ codesign -s "${Your_iPhone_Developer_Certificate}" -f lib/libroc.<version>.dylib
 *    \endcode
 *    Your developer certificate is stored in the <tt>Keychain Access</tt>
 *    application and typically begins with <tt>iPhone Developer:</tt>.
 * -# In your project's <b>Build Phases</b> page drag and drop
 *    <tt>lib/libroc.\<version\>.dylib</tt> into <b>Link Binary With
 *    Libraries</b> and <b>Copy Bundle Resources</b>.
 * -# In your project's <b>Build Settings</b> page find the <b>Library Search
 *    Paths</b> setting under the <b>Search Paths</b> section and add the full
 *    path to the folder containing <tt>libroc.\<version\>.dylib</tt>.
 * -# In your project's <b>Build Settings</b> page find the <b>Enable
 *    Bitcode</b> setting under the <b>Build Options</b> section and set it to
 *    <b>No</b>.
 * -# In your project's <b>Build Settings</b> page find the <b>Runpath Search
 *    Path</b> setting under the <b>Linking</b> section and add an entry
 *    <b>\@executable_path</b>.
 * -# In the XCode <b>Project navigator</b> window, drag and drop
 *    <tt>include/roc.h</tt> into the folder containing source code that will
 *    call the ROC SDK.
 * -# At runtime call \ref roc_get_host_id and email <tt>support@rankone.io</tt>
 *    the value of <tt>host_id</tt>.
 *    We will respond with a <tt>ROC.lic</tt> license file.
 * -# In your project's <b>Build Phases</b> page drag and drop
 *    <tt>ROC.lic</tt> into <b>Copy Bundle Resources</b>.
 *    Call \ref roc_initialize be passing the contents of the bundled license file:
 *    \code
 *    roc_ensure(roc_initialize([[NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ROC" ofType:@"lic"] encoding:NSUTF8StringEncoding error:nil] UTF8String], NULL));
 *    \endcode
 *
 * Since Objective-C is interoperable with C, consult the source code in the
 * \c examples folder for reference implementations of common use cases.
 */

#ifndef SWIG_ROC_VIDEO

#ifdef SWIG
%module roc
%include cpointer.i
%include carrays.i
#if defined(SWIGPYTHON) || defined(SWIGJAVA)
%include cdata.i
#endif // SWIGPYTHON || SWIGJAVA
%include stdint.i
%{
#include "roc.h"
%}
#ifdef SWIGJAVA
%include enumtypeunsafe.swg
%native (roc_preinitialize_android) roc_string roc_preinitialize_android(jobject);
%{
JNIEXPORT jstring JNICALL
Java_io_rankone_rocsdk_rocJNI_roc_1preinitialize_1android(JNIEnv *,
                                                          jclass,
                                                          jobject);
%}
#endif // SWIGJAVA
#ifdef SWIGCSHARP
%apply void *VOID_INT_PTR { void * }
#endif // SWIGSCHARP
%pointer_functions(float,  float)
%pointer_functions(size_t, size_t)
%pointer_functions(roc_gallery,    roc_gallery)
%pointer_functions(roc_similarity, roc_similarity)
%pointer_functions(roc_string,     roc_string)
%pointer_functions(roc_buffer,     roc_buffer)
%pointer_functions(roc_time,  roc_time)
%pointer_functions(roc_video, roc_video)
%pointer_functions(roc_stream, roc_stream)
%pointer_functions(roc_tracker, roc_tracker)
%array_functions(uint8_t, uint8_t_array)
%array_functions(roc_template,   roc_template_array)
%array_functions(roc_similarity, roc_similarity_array)
%array_functions(roc_candidate,  roc_candidate_array)
%array_functions(roc_person_id,  roc_person_id_array)
#endif // SWIG

/*!
 * \page cli Command Line Interface
 * \brief A robust Command Line Interface (CLI) wrapping key API functions.
 *        The CLI is ideal for headless server environments or scripts that
 *        perform face recognition tasks.
 *        It is designed to mesh well with standard Unix commands.
 *
 * \note Be sure to export the \c ROC_LIC environment variable with the path to
 *       <tt>ROC.lic</tt> before using the CLI applications.
 *       See \ref roc_initialize for details.
 *
 * \section cli_workflow Workflow
 * Below is a diagram illustrating the common workflow through our command line
 * applications.
 * Click on an application to pull up its documentation and example usage.
 *
 * \htmlinclude cli.svg
 *
 * \section cli_applications Applications
 * Here's a list of the applications along with a brief description of each.
 * | Application                  | Description                    |
 * |------------------------------|--------------------------------|
 * | \subpage roc-at              | \copybrief roc-at              |
 * | \subpage roc-cluster         | \copybrief roc-cluster         |
 * | \subpage roc-compare         | \copybrief roc-compare         |
 * | \subpage roc-consolidate     | \copybrief roc-consolidate     |
 * | \subpage roc-evaluate        | \copybrief roc-evaluate        |
 * | \subpage roc-extract-frames  | \copybrief roc-extract-frames  |
 * | \subpage roc-host-id         | \copybrief roc-host-id         |
 * | \subpage roc-license-server  | \copybrief roc-license-server  |
 * | \subpage roc-log             | \copybrief roc-log             |
 * | \subpage roc-metadata        | \copybrief roc-metadata        |
 * | \subpage roc-remove          | \copybrief roc-remove          |
 * | \subpage roc-represent       | \copybrief roc-represent       |
 * | \subpage roc-represent-video | \copybrief roc-represent-video |
 * | \subpage roc-search          | \copybrief roc-search          |
 * | \subpage roc-serve           | \copybrief roc-serve           |
 * | \subpage roc-track           | \copybrief roc-track           |
 * | \subpage roc-validate        | \copybrief roc-validate        |
 *
 * \section cli_gallery_files Gallery Files
 * See \ref roc_gallery_file for a description of the ROC gallery file format
 * that is an integral aspect of the CLI.
 *
 * \section cli_usage_logging Usage Logging
 * If your license requires usage logging, first export the \c ROC_LOG
 * environment variable before using the CLI.
 * See \ref api_usage_logging for details.
 *
 * \page api Application Programming Interface
 * \brief All ROC SDK capabilities are accessible via a C-language Application
 *        Programming Interface (API).
 *        This approach is well suited for software engineers looking to build
 *        custom applications using ROC SDK face recognition functions.
 *
 * \section getting_started Getting Started
 * Add <tt>/path/to/ROC_SDK/include</tt> to your include directories and
 * <tt>/path/to/ROC_SDK/lib</tt> to your lib directories.
 * <tt>\#include "roc.h"</tt> and link against <tt>roc</tt>.
 * Additionally link against <tt>roc_video</tt> for \ref video_decoding.
 *
 * \section examples Examples
 * - \subpage roc_example_verify  "1-to-1 Verification"
 * - \subpage roc_example_search  "1-to-N Search"
 * - \subpage roc_example_flatten "Flatten Template Into Buffer"
 * - \subpage roc_example_track   "Track Faces in a Video"
 * - \subpage roc_example_host_id "Print Host ID"
 *
 * Source code for these examples is available in the \c examples folder.
 *
 * \section api_overview Overview
 * \ref versioning <br>
 * \copydetails versioning
 *
 * \ref error_handling <br>
 * \copydetails error_handling
 *
 * \ref initialization <br>
 * \copydetails initialization
 *
 * \ref media_decoding <br>
 * \copydetails image_decoding
 * \copydetails video_decoding
 * \copydetails media_decoding
 *
 * \ref video_streaming <br>
 * \copydetails video_streaming
 *
 * \ref template_generation <br>
 * \copydetails template_generation
 *
 * \ref template_io <br>
 * \copydetails template_io
 *
 * \ref gallery_construction <br>
 * \copydetails gallery_construction
 *
 * \ref comparison <br>
 * \copydetails comparison
 *
 * \ref search <br>
 * \copydetails search
 *
 * \ref tracking <br>
 * \copydetails tracking
 *
 * \ref identity_discovery <br>
 * \copydetails identity_discovery
 *
 * \ref remote_galleries <br>
 * \copydetails remote_galleries
 *
 * \section thread_safety Thread Safety
 * All functions are marked one of:
 * - \anchor thread-safe \b thread-safe
 *   Can be called simultaneously from multiple threads, even when the
 *   invocations use the same data.
 * - \anchor reentrant \b reentrant
 *   Can be called simultaneously from multiple threads, but only if each
 *   invocation uses different data.
 * - \anchor thread-unsafe \b thread-unsafe
 *   Can not be called simultaneously from multiple threads.
 *
 * A function may also be marked:
 * - \anchor parallel \b parallel
 *   The workload for this function is automatically parallelized to make use of
 *   all CPU cores belonging to the system.
 *
 * \section api_workflow Workflow
 * \htmlinclude api.svg
 *
 * \page roc_example_host_id roc_example_host_id.c
 * \brief Source code for <tt>examples/roc_example_host_id.c</tt>.
 *
 * \snippet roc_example_host_id.c ROC Example Host ID Application
 *
 * \page roc_example_verify roc_example_verify.c
 * \brief Source code for <tt>examples/roc_example_verify.c</tt>.
 *
 * \snippet roc_example_verify.c ROC Example Verify Application
 *
 * \page roc_example_search roc_example_search.c
 * \brief Source code for <tt>examples/roc_example_search.c</tt>.
 *
 * \snippet roc_example_search.c ROC Example Search Application
 *
 * \page roc_example_flatten roc_example_flatten.c
 * \brief Source code for <tt>examples/roc_example_flatten.c</tt>.
 *
 * \snippet roc_example_flatten.c ROC Example Flatten Application
 *
 * \page roc_example_track roc_example_track.c
 * \brief Source code for <tt>examples/roc_example_track.c</tt>.
 *
 * \snippet roc_example_track.c ROC Example Video Tracking Application
 *
 * \page face_quality Face Quality
 * \brief A metric indicating how accurately a template will perform in
 *        recognition.
 *
 * A higher face quality (or "face matchability") value indicates a lower false
 * non-match rate.
 * The use of a face quality metric most often arises when one has multiple
 * templates of a person and needs to decide which to add to a gallery.
 * Absent of adding all the templates (or some subset thereof using
 * \ref roc_consolidate), the face with the highest quality should be used.
 * When using the \ref ROC_FR or \ref ROC_FR_FAST algorithm, a \c Quality key is
 * added to the \ref template_metadata.
 * This value is normalized with a mean of \c 0 and standard deviation of \c 1
 * relative to a training set.
 * Faces with a quality below \c -2 will generally not result in successful
 * matching.
 *
 * \page face_liveness Face Liveness
 * \brief A test to determine if the detected face is a living person or an
 *        impostor trying to defeat the system with a stolen image.
 *
 * A <i>dynamic</i> algorithm generally involves asking the user to complete a
 * task.
 * For example, asking the user to turn their head to the left or the right
 * or to open their mouth.
 * These movements can be measured using \ref ROC_PITCHYAW and
 * \ref ROC_LIPS respectively.
 * These algorithms generally have low error rates, at the cost of requiring a
 * few seconds of interaction from the user.
 * In order to leverage these methods, permission from Amazon and Google may
 * need to be obtained as they own patents in this field.
 *
 * A <i>static</i> algorithm attempts to detect the impostor from a single
 * image.
 * For example, detecting if the face image is on a printed piece of paper or
 * computer screen.
 * These algorithms generally have higher error rates, with the benefit that
 * they do not require interaction from the user.
 * Rank One's approach uses a patent pending method based on micro-texture
 * analysis of the user's image.
 * This method requires an image in its original form from the camera, without
 * changes introduced by compression, resizing or other image processing
 * transformations.
 * Use \ref ROC_SPOOF_FF or \ref ROC_SPOOF_AF for a static liveness metric.
 */

/*!
 * \defgroup versioning Versioning
 * \brief SDK version information.
 *
 * The SDK version is comprised of three values: \ref ROC_VERSION_MAJOR,
 * \ref ROC_VERSION_MINOR and \ref ROC_VERSION_PATCH.
 *
 * @{
 */

/*!
 * \brief Used to indicate substantial new algorithms that require a license
 *        purchase.
 */
#define ROC_VERSION_MAJOR 1

/*!
 * \brief Used to indicate incremental algorithm improvements and API changes.
 *
 * Unless otherwise noted by \ref ROC_FR_COMPATIBILITY_VERSION, templates are
 * not compatible between minor versions.
 */
#define ROC_VERSION_MINOR 18

/*!
 * \brief Used to indicate bug fixes.
 */
#define ROC_VERSION_PATCH 2

/*!
 * \brief String version of \ref ROC_VERSION_MAJOR . \ref ROC_VERSION_MINOR .
 *        \ref ROC_VERSION_PATCH.
 */
#define ROC_VERSION_STRING "1.18.2"

/*!
 * \brief Function equivalent of \ref ROC_VERSION_MAJOR for runtime version
 *        checking.
 */
ROC_EXPORT int roc_version_major();

/*!
 * \brief Function equivalent of \ref ROC_VERSION_MINOR for runtime version
 *        checking.
 */
ROC_EXPORT int roc_version_minor();

/*!
 * \brief Function equivalent of \ref ROC_VERSION_PATCH for runtime version
 *        checking.
 */
ROC_EXPORT int roc_version_patch();

/*!
 * \brief Function equivalent of \ref ROC_VERSION_STRING for runtime version
 *        checking.
 */
ROC_EXPORT const char *roc_version_string();

/*!
 * \brief Rank One Computing copyright text.
 */
#define ROC_COPYRIGHT "Copyright © 2015-2019 Rank One Computing Corporation. All rights reserved."

/*!
 * \brief Function equivalent of \ref ROC_COPYRIGHT.
 */
ROC_EXPORT const char *roc_copyright();

/** @} */ // end of versioning

/*!
 * \defgroup error_handling Error Handling
 * \brief Handling errors.
 *
 * All ROC functions return a printable \ref roc_error message by design.
 * The \ref ROC_ATTEMPT macro and \ref roc_ensure function are provided as basic
 * error handling methods.
 *
 * @{
 */

#endif // !SWIG_ROC_VIDEO

/*!
 * \brief A printable error string.
 *
 * All functions in the ROC API return a \ref roc_error.
 * A value of \c NULL indicates \em success, and all other values indicate
 * \em failure.
 * \note Failure values are statically-allocated strings that should \em not be
 * freed.
 *
 * \par Example
 * \code{C}
 * roc_error error = ...;
 * if (error) {
 *   puts(error);
 *   abort();
 * }
 * \endcode
 *
 * \see \ref ROC_ATTEMPT \ref roc_ensure
 */
typedef const char *roc_error;

#ifndef SWIG_ROC_VIDEO

/*!
 * \brief A simple return-error-message error handling macro.
 * \par Example
 * \code{C}
 * ROC_ATTEMPT(roc_initialize(NULL, NULL))
 * \endcode
 * \see \ref roc_ensure
 */
#define ROC_ATTEMPT(EXPRESSION)           \
{                                         \
    const roc_error error = (EXPRESSION); \
    if (error)                            \
        return error;                     \
}

/*!
 * \brief A simple print-and-abort error handling function.
 *
 * \code
 * roc_error roc_ensure(roc_error error)
 * {
 *     if (error) {
 *         fprintf(stderr, "ROC Error: %s\n", error);
 *         abort();
 *     }
 * }
 * \endcode
 *
 * \param[in] error Error return value from a ROC API function call.
 * \remark This function is \ref thread-safe.
 * \see \ref ROC_ATTEMPT
 */
ROC_EXPORT void roc_ensure(roc_error error);

/** @} */ // end of error_handling

/*!
 * \defgroup initialization Initialization
 * \brief Initializing the SDK.
 *
 * A ROC application begins with a call to \ref roc_initialize, and ends with
 * a call to \ref roc_finalize.
 * For license management see \ref roc_get_host_id.
 *
 * @{
 */

#ifndef SWIGJAVA

/*!
 * \brief Inform the ROC SDK about the Java calling environment.
 *
 * \note This function need \em only be called when using the Android SDK.
 * In this case call it before calling \ref roc_initialize.
 * Calling this function on other operating systems is a no-op.
 *
 * \note The Java wrapper for this function takes only one argument, the
 * \p jobject Android activity.
 * The \p JNIEnv is passed automatically.
 *
 * \param JNIEnv JNI environment.
 * \param jobject Android activity.
 * \remark This function is \ref thread-unsafe.
 */
ROC_EXPORT roc_error roc_preinitialize_android(void *JNIEnv, void *jobject);

#endif // !SWIGJAVA

/*!
 * \brief Call once at the start of the application before making any other
 *        calls to the API.
 *
 * When using the Android SDK call \ref roc_preinitialize_android first.
 *
 * \section license_file License File
 * The \p license_file is the path to <tt>ROC.lic</tt> or the contents
 * thereof.
 * \note If \p license_file ends with <tt>.lic</tt> then it is treated as a file
 * \em path, otherwise it is treated as the file \em contents.
 *
 * If \p license_file is \c NULL (as is the case for the \ref cli and
 * \ref explore), this function will use the value from the \c ROC_LIC
 * environment variable if it exists.
 * Otherwise it will check for <tt>../ROC.lic</tt> first relative to the current
 * working directory, and second relative to the application binary directory
 * (not always possible to determine).
 * It is permissible for <tt>ROC.lic</tt> to be \em read-only.
 * See \ref roc_get_host_id for details on obtaining <tt>ROC.lic</tt>.
 *
 * \section api_usage_logging Usage Logging
 * For certain applications the ROC SDK is licensed on a per-function-call
 * basis.
 * In these cases the \c ROC.lic license file will contain a logging
 * requirement and \p log_file should be provided, otherwise \p log_file may be
 * \c NULL.
 *
 * \p log_file is a file path to which the ROC SDK will write encrypted usage
 * statistics.
 * By convention, \p log_file is named \c ROC.log.
 * If \p log_file does not exist then it will be created, otherwise it will be
 * appended to.
 * To satisfy licensing requirements \p log_file should be periodically sent to
 * Rank One Computing, and may then be deleted locally (when the application is
 * \em not running).
 * \p log_file contains no sensitive information, just simple usage statistics.
 *
 * If \p log_file is \c NULL, but the license requires logging, this function
 * will use the value from the \c ROC_LOG environment variable if it exists,
 * otherwise this function will return an error.
 *
 * The \ref cli uses the \c ROC_LOG idiom.
 * The contents of \p log_file can be printed using \ref roc-log.
 *
 * \note If multiple concurrent processes are using the ROC SDK, each must
 * specify its own log file.
 *
 * \section process_lock Process Locking
 * Most licenses impose a maximum limit on the number of processes that can use
 * the ROC SDK simultaneously.
 * If you recieve the error <code>QSystemSemaphore::handle:
 * permission denied</tt> please ensure the <code>TMPDIR</code> environment
 * variable is set to a folder for which you have read+write permission.
 *
 * \par Example
 * \code{C}
 * const char *license_file = ...;
 * roc_initialize(license_file, NULL);
 * \endcode
 *
 * \param[in] license_file File path to <tt>ROC.lic</tt>.
 * \param[in] log_file File path to write usage statistics to, or \c NULL if
 *                     \ref api_usage_logging is not required.
 * \remark This function is \ref thread-unsafe and must only be called once.
 * \see \ref roc_finalize
 */
ROC_EXPORT roc_error roc_initialize(const char *license_file,
                                    const char *log_file);

/*!
 * \brief Call once at the end of the application after making all other calls
 *        to the API.
 *
 * This will free memory allocated during initialization.
 * Calling \ref roc_initialize after this function to re-initialize the SDK is
 * \em not supported.
 * \remark This function is \ref thread-unsafe and must only be called once.
 *
 * \par Example
 * \code{C}
 * roc_finalize();
 * \endcode
 * \see \ref roc_initialize
 */
ROC_EXPORT roc_error roc_finalize();

/*!
 * \brief Represents a string output value that should be freed with
 *        \ref roc_free_string after use.
 */
typedef const char *roc_string;

/*!
 * \brief Free a \ref roc_string previously returned by another function.
 *
 * If \p str is \c NULL, this function does nothing.
 *
 * \par Example
 * \code{C}
 * roc_string str = ...;
 * roc_free_string(&str);
 * \endcode
 *
 * \param[in,out] str \ref roc_string to free.
 * \remark This function is \ref reentrant.
 */
ROC_EXPORT roc_error roc_free_string(roc_string *str);

/*!
 * \brief Represents a buffer output value that should be freed with
 *        \ref roc_free_buffer after use.
 */
typedef const uint8_t *roc_buffer;

/*!
 * \brief Free a \ref roc_buffer previously returned by another function.
 *
 * If \p buffer is \c NULL, this function does nothing.
 *
 * \par Example
 * \code{C}
 * roc_buffer buffer = ...;
 * roc_free_buffer(&buffer);
 * \endcode
 *
 * \param[in,out] buffer \ref roc_buffer to free.
 * \remark This function is \ref reentrant.
 */
ROC_EXPORT roc_error roc_free_buffer(roc_buffer *buffer);

/*!
 * \brief Retrieve the Host ID of the current machine.
 *
 * \section host_id Host ID
 * The <i>Host ID</i> is a null-terminated JSON string uniquely identifying the
 * current machine.
 *
 * The output of this function (or the \ref roc-host-id application) should be
 * emailed to \c support@rankone.io to obtain a valid license file prior to
 * calling \ref roc_initialize.
 * Rank One uses the Host ID to generate the <tt>ROC.lic</tt> license
 * file that should be copied to the ROC SDK root directory, overwriting the
 * placeholder license file of the same name.
 *
 * Free \p host_id after use with \ref roc_free_string.
 *
 * This function can be called before \ref roc_initialize.
 *
 * \note Android developers must call \ref roc_preinitialize_android before
 *       calling this function.
 *
 * \par Example
 * \code{C}
 * roc_string host_id;
 * roc_get_host_id(&host_id);
 * \endcode
 *
 * \param[out] host_id The Host ID for the current machine.
 * \remark This function is \ref thread-unsafe.
 */
ROC_EXPORT roc_error roc_get_host_id(roc_string *host_id);

/*!
 * \brief Optional license terms.
 */
typedef enum roc_license_term
{
    /*!
     * \brief True if \ref roc_initialize has been called successfully.
     */
    ROC_INITIALIZED,

    /*!
     * \brief A license that disables feature vector construction.
     */
    ROC_DEMOGRAPHICS_ONLY,

    /*!
     * \brief A license that enables the Explore GUI.
     */
    ROC_EXPLORE
} roc_license_term;

/*!
 * \brief Check if a license term is enabled.
 *
 * \par Example
 * \code{C}
 * bool demographics_only;
 * roc_has_license_term(ROC_DEMOGRAPHICS_ONLY, &demographics_only);
 * \endcode
 *
 * \param[in] license_term The term to query.
 * \param[out] enabled The value for \p license_term.
 * \remark This function is \ref thread-safe.
 */
ROC_EXPORT roc_error roc_get_license_term(roc_license_term license_term,
                                          bool *enabled);

/*!
 * \brief Get the thread count limit.
 *
 * The thread count limit is the maximum number of concurrent calls that can
 * be made to ROC SDK functions as specified by the \c threads value in the
 * \c ROC.lic license file.
 *
 * \par Example
 * \code{C}
 * int limit;
 * roc_get_thread_limit(&limit);
 * \endcode
 *
 * \param[out] limit The thread count limit.
 * \remark This function is \ref thread-safe.
 * \see \ref threads
 */
ROC_EXPORT roc_error roc_get_thread_limit(int *limit);

/*!
 * \brief Set the thread count limit.
 *
 * If \p limit is greater than the thread limit specified in the license file
 * then it will be ignored.
 *
 * \note This function must be called before \ref roc_initialize.
 *
 * \param[in] limit Maximum number threads to use.
 * \remark This function is \ref thread-safe.
 * \see \ref threads
 */
ROC_EXPORT roc_error roc_set_thread_limit(int limit);

/*!
 * \brief Query the floating license server for limits.
 *
 * The value for \p clients_remaining is the \c instances specified in the
 * client license file \em less the number of currently active clients
 * (including the client making the request).
 * The value for \p comparisons_remaining is the \ref roc_comparison_limit
 * reported by the server.
 *
 * If the license file specified for \ref roc_initialize is not floating then
 * \p clients_remaining and \p comparisons_remaining will be set to zero and
 * this function will return without error.
 * If the license does not require \ref api_usage_logging then the value
 * returned for \p comparisons_remaining should be ignored.
 *
 * \par Example
 * \code{C}
 * uint32_t clients_remaining;
 * uint64_t comparisons_remaining;
 * roc_get_floating_license_limit(&clients_remaining, &comparisons_remaining);
 * \endcode
 *
 * \param[out] clients_remaining Number of client licenses available.
 * \param[out] comparisons_remaining Number of comparisons remaining for
 *                                   licenses that require
 *                                   \ref api_usage_logging.
 * \remark This function is \ref thread-safe.
 * \see \ref roc-license-server
 */
ROC_EXPORT roc_error roc_get_floating_license_limit(uint32_t *clients_remaining,
                                                    uint64_t *
                                                         comparisons_remaining);

/** @} */ // end of initialization

/*!
 * \defgroup media_decoding Media Decoding
 * \brief Reading images and videos.
 *
 * Free an image with \ref roc_free_image.
 *
 * @{
 */

#endif // !SWIG_ROC_VIDEO

/*!
 * \brief Supported image formats.
 */
typedef enum roc_color_space
{
    /*!
     * \brief 1-channel grayscale, 8-bit depth.
     */
    ROC_GRAY8,

    /*!
     * \brief 3-channel color (BGR order), 8-bit depth.
     */
    ROC_BGR24
} roc_color_space;

/*!
 * \brief Common representation for still images and video frames.
 *
 * Images can be read with \ref roc_read_image or \ref roc_read_frame, and freed
 * with \ref roc_free_image.
 *
 * Pixels are stored in _row-major_ order.
 * In other words, pixel layout with respect to decreasing memory spatial
 * locality is \a channel, \a column, \a row.
 * Thus pixel intensity can be retrieved as follows:
 *
 * \code{C}
 * uint8_t get_intensity(roc_image image,
 *                       size_t channel,
 *                       size_t column,
 *                       size_t row)
 * {
 *     const size_t channels = (image.color_space == ROC_BGR24 ? 3 : 1);
 *     const size_t index = row*image.step + column*channels + channel;
 *     return image.data[index];
 * }
 * \endcode
 *
 * Coordinate (0, 0) corresponds to the top-left corner of the image.
 * Coordinate (width-1, height-1) corresponds to the bottom-right corner of the
 * image.
 *
 */
typedef struct roc_image
{
    uint8_t *data; /*!< \brief Buffer of pixel intensities. */
    size_t width;  /*!< \brief Column count in pixels. */
    size_t height; /*!< \brief Row count in pixels. */
    size_t step;   /*!< \brief Bytes per row, including padding. */
    roc_color_space color_space; /*!< \brief Interpretation of
                                             roc_image::data. */
} roc_image;

#ifndef SWIG_ROC_VIDEO

/*!
 * \brief Deep copy an image.
 *
 * \par Example
 * \code{C}
 * roc_image src = ...;
 * roc_image dst;
 * roc_copy_image(src, &dst);
 * \endcode
 *
 * \param[in] src Image to copy to \p dst.
 * \param[out] dst Uninitialized output to hold a copy of \p src.
 * \remark This function is \ref reentrant.
 */
ROC_EXPORT roc_error roc_copy_image(roc_image src, roc_image *dst);

/*!
 * \brief Rotate an image clockwise in intervals of 90 degrees.
 *
 * \p image is rotated in-place.
 * \p degrees must be evenly divisible by 90.
 * A negative value indicates a counter-clockwise rotation.
 *
 * \par Example
 * \code{C}
 * roc_image image = ...;
 * roc_rotate(&image, 90);
 * \endcode
 *
 * \param[in,out] image Image to rotate.
 * \param[in] degrees Degrees to rotate \p image clockwise.
 * \remark This function is \ref reentrant.
 */
ROC_EXPORT roc_error roc_rotate(roc_image *image, int degrees);

/*!
 * \brief Swap red and blue channels to convert between RGB and BGR color
 *        spaces.
 *
 * \param[in,out] image Image to swap channels in-place.
 * \remark This function is \ref thread-safe.
 */
ROC_EXPORT roc_error roc_swap_channels(roc_image image);

/*!
 * \brief Frees the memory previously allocated for a \ref roc_image.
 * \param[in] image Image to free.
 * \remark This function is \ref reentrant.
 */
ROC_EXPORT roc_error roc_free_image(roc_image image);

/*!
 * \defgroup image_decoding Image Decoding
 * \brief Reading images.
 *
 * Decode a \ref roc_image from a file with \ref roc_read_image or from a
 * buffer with \ref roc_decode_image.
 *
 * @{
 */

/*!
 * \brief Read an image from a file.
 *
 * Free \p image after use with \ref roc_free_image.
 *
 * \section supported_image_formats Supported Image Formats
 * - Windows bitmaps - *.bmp, *.dib
 * - JPEG files - *.jpeg, *.jpg, *.jpe
 * - JPEG 2000 files - *.jp2
 * - Portable Network Graphics - *.png
 * - Portable image format - *.pbm, *.pgm, *.ppm
 * - Sun rasters - *.sr, *.ras
 * - TIFF files - *.tiff, *.tif
 *
 * \section color_space_considerations Color Space Considerations
 * \ref roc_represent operates natively on \ref ROC_GRAY8 images, and will
 * automatically convert provided \ref ROC_BGR24 images.
 * When you don't otherwise need color images, as a speed/memory optimization
 * you may read an image file directly into grayscale to eliminate the color
 * space conversion in \ref roc_represent.
 * \note A template constructed from an image opened with \ref ROC_GRAY8 may not
 * be binary-identical to a template constructed from the same image opened with
 * \ref ROC_BGR24; this difference will not impact system accuracy.
 *
 * \section orientation Orientation
 * A common unfortunate case is when an image file is originally saved with
 * incorrect orientation information.
 * For such an image rotated 90 degrees in either direction from vertical, or
 * upside down, \ref roc_represent will fail to find any faces in it.
 * If you suspect this is the case with your image, use \ref roc_rotate before
 * \ref roc_represent to try the same image at multiple possible orientations.
 *
 * \par Example
 * \code{C}
 * roc_image image;
 * roc_read_image("lenna.jpeg", ROC_GRAY8, &image);
 * \endcode
 *
 * \param[in] file_name Path to the image file.
 * \param[in] color_space Desired color space, see
 *                        \ref color_space_considerations.
 * \param[out] image Address to store the decoded image.
 * \remark This function is \ref reentrant.
 * \see roc_write_image roc_decode_image
 */
ROC_EXPORT roc_error roc_read_image(const char *file_name,
                                    roc_color_space color_space,
                                    roc_image *image);

/*!
 * \brief Write an image to a file.
 *
 * The extension of \p file_name is used to determine the encoding format, see
 * \ref supported_image_formats for details.
 *
 * \par Example
 * \code{C}
 * roc_image image = ...;
 * const char *file_name = ...;
 * roc_write_image(image, file_name);
 * \endcode
 *
 * \param image Image to write.
 * \param file_name Path to the image to.
 * \remark This function is \ref reentrant.
 * \see \ref roc_read_image
 */
ROC_EXPORT roc_error roc_write_image(roc_image image,
                                     const char *file_name);

/*!
 * \brief Decode an image from a buffer.
 *
 * Free \p image after use with \ref roc_free_image.
 *
 * See \ref roc_read_image for \ref supported_image_formats and
 * \ref color_space_considerations.
 *
 * \par Example
 * \code{C}
 * size_t len = ...;
 * const char *data = ...;
 * roc_image image;
 * roc_decode_image(len, data, ROC_GRAY8, &image);
 * \endcode
 *
 * \param[in] len Length of \p data.
 * \param[in] data Encoded image buffer.
 * \param[in] color_space Desired color space, see
 *                        \ref color_space_considerations.
 * \param[out] image Address to store the decoded image.
 * \remark This function is \ref reentrant.
 * \see \ref roc_read_image \ref roc_encode_image
 */
ROC_EXPORT roc_error roc_decode_image(size_t len,
                                      const uint8_t *data,
                                      roc_color_space color_space,
                                      roc_image *image);

/*!
 * \brief Encode an image to a buffer.
 *
 * Free \p buffer after use with \ref roc_free_buffer.
 *
 * \section encoding_quality Encoding Quality
 * For \c .jpg the valid range for \p quality is 0 to 100, where higher values
 * are better quality.
 * For \c .png the valid range for \p quality is 0 to 9, where higher values are
 * more compressed.
 * The \p quality parameter is ignored for all other formats.
 *
 * \par Example
 * \code{C}
 * roc_image image = ...;
 * roc_buffer buffer;
 * size_t buffer_length;
 * roc_encode_image(image, ".jpg", 95, &buffer, &buffer_length);
 * \endcode
 *
 * \param[in] image Image to encode.
 * \param[in] format File extension specifying the encoding format, see
 *                   \ref supported_image_formats for details.
 * \param[in] quality Image encoding quality, see \ref encoding_quality.
 * \param[out] buffer Encoded image buffer.
 * \param[out] buffer_length Length of \p buffer.
 * \remark This function is \ref reentrant.
 * \see \ref roc_write_image \ref roc_decode_image
 */
ROC_EXPORT roc_error roc_encode_image(roc_image image,
                                      const char *format,
                                      int quality,
                                      roc_buffer *buffer,
                                      size_t *buffer_length);

/*!
 * \brief Convert a color image to grayscale.
 *
 * Converts an image from \ref ROC_BGR24 to \ref ROC_GRAY8.
 * Free \p dst after use with \ref roc_free_image.
 *
 * \par Example
 * \code{C}
 * roc_image src = ...;
 * roc_image dst;
 * roc_bgr2gray(src, &dst);
 * \endcode
 *
 * \param[in] src Input color image.
 * \param[out] dst Output grayscale image.
 * \remark This function is \ref reentrant.
 */
ROC_EXPORT roc_error roc_bgr2gray(roc_image src, roc_image *dst);

/*!
 * \brief Convert an NV21-formatted buffer to a \ref roc_image.
 *
 * [\c NV21 is the default format for the Android camera preview.]
 * (https://developer.android.com/reference/android/graphics/ImageFormat.html#NV21)
 *
 * \param[in] data NV21 data buffer.
 * \param[in] width Image width.
 * \param[in] height Image height.
 * \param[out] image Output \ref ROC_BGR24 image.
 * \remark This function is \ref thread-safe.
 */
ROC_EXPORT roc_error roc_convert_nv21(const uint8_t *data,
                                      size_t width,
                                      size_t height,
                                      roc_image *image);

/** @} */ // end of image_decoding

#endif // !SWIG_ROC_VIDEO

/*!
 * \defgroup video_decoding Video Decoding
 * \brief Reading videos.
 *
 * Open a \ref roc_video with \ref roc_open_video or \ref roc_open_spinnaker.
 * Read frames sequentially with \ref roc_read_frame, seek to a frame or
 * keyframe with \ref roc_seek_frame or \ref roc_seek_keyframe and close a video
 * with \ref roc_close_video.
 * @{
 */

/*!
 * \brief Time in milliseconds
 */
typedef uint32_t roc_time;

/*!
 * \brief Handle to a private video decoding type.
 */
typedef struct roc_video_type *roc_video;

/*!
 * \brief Video metadata
 */
typedef struct roc_video_metadata
{
    size_t width;      /*!< \brief Width of the video's picture. */
    size_t height;     /*!< \brief Height of the video's picture. */
    roc_time duration; /*!< \brief Length of the opened video in milliseconds.
                                   Defaults to 0 for videos of unknown duration
                                   such as webcam and IP camera video
                                   streams. */
    int bit_rate;      /*!< \brief Number of bits per second. Equal to
                                   \code{C} (video_bytes*8) / duration
                                   \endcode */
    double frame_rate; /*!< \brief Average frame rate of the video in
                                   seconds. */
    roc_time keyframe_interval; /*!< \brief Approximate milliseconds between
                                            keyframes. Defaults to 0 for webcam
                                            and IP camera video streams. */
} roc_video_metadata;

#if !defined(SWIG) || defined(SWIG_ROC_VIDEO)

#ifdef SWIG
%module roc_video
#ifdef SWIGJAVA
%include enumtypeunsafe.swg
#endif // SWIGJAVA
%{
#include "roc.h"
%}
#endif // SWIG

/*!
 * \brief Open a video for reading.
 *
 * In addition to files on disk, this function supports capture devices such
 * as a webcam by specifying the device index as the file name, as well as
 * streaming videos over network protocols including http/https.
 *
 * If specifying a device index, parameters for framerate and/or video_size can
 * be set.
 *
 * Close a video after use with \ref roc_close_video.
 * Remember to close the video even if \ref roc_open_video fails.
 *
 * \note This function is \em only available for x64-based versions of the SDK
 *       as well as x86 on Windows.
 * \note Be sure to read and follow \ref video_decoding_dependencies.
 *
 * \section supported_video_formats Supported Video Formats
 * ROC SDK makes use of the system's FFmpeg / libAV library so supported file
 * formats may vary slightly by installation.
 * The standard list of supported formats is available
 * [here](http://www.ffmpeg.org/general.html#Video-Codecs).
 *
 * \par Example
 * \code{C}
 * roc_video video;
 * roc_video_metadata video_metadata;
 * roc_open_video("gangnam-style.mp4", ROC_BGR24, &video, &video_metadata);
 *
 * roc_video webcam;
 * roc_open_video("0", ROC_BGR24, &webcam, NULL);
 *
 * roc_video webcam;
 * roc_open_video("0:framerate=30,video_size=1280x720", ROC_BGR24, &webcam,
 *                NULL);
 *
 * roc_video ip_camera;
 * roc_open_video("http://204.248.124.202/mjpg/video.mjpg", ROC_BGR24,
 *                &ip_camera, NULL);
 *
 * roc_video http_video
 * roc_open_video("https://s3.amazonaws.com/rankone/exampleData/video.mp4",
 *                ROC_BGR24
 *                &http_video, NULL);
 * \endcode
 *
 * \param[in] file_name Path to video file.
 * \param[in] color_space Desired color space, see
 *                        \ref color_space_considerations.
 * \param[out] video Address to store the opened video.
 * \param[out] metadata Address to metadata associated with the opened video,
 *                      or \c NULL.
 * \remark This function is \ref reentrant.
 * \see \ref roc_close_video
 */
ROC_EXPORT roc_error roc_open_video(const char *file_name,
                                    roc_color_space color_space,
                                    roc_video *video,
                                    roc_video_metadata *metadata);

#endif // !defined(SWIG) || defined(SWIG_ROC_VIDEO)
#ifndef SWIG_ROC_VIDEO

/*!
 * \brief Connect to a Spinnaker USB3 Vision camera.
 *
 * About the
 * [Spinnaker Software Development Kit](https://www.ptgrey.com/spinnaker-sdk).
 *
 * \note This function is currently only supported on Windows x64.
 * \note The use of this function requires an installation of the Spinnaker SDK,
 *       available for download
 *       [here](https://s3.amazonaws.com/rankone/dependencies/SpinnakerSDK_FULL_1.7.0.9_x64.exe).
 *
 * \par Example
 * \code{C}
 * roc_video video;
 * roc_open_spinnaker(0, ROC_BGR24, &video);
 * \endcode
 *
 * \param[in] index Camera to open (default is \c 0).
 * \param[in] color_space Desired color space, see
                          \ref color_space_considerations.
 * \param[out] camera Address to store the opened camera.
 * \remark This function is \ref reentrant.
 */
ROC_EXPORT roc_error roc_open_spinnaker(int index,
                                        roc_color_space color_space,
                                        roc_video *camera);

/*!
 * \brief Determine if a video is live or recorded.
 *
 * \par Example
 * \code{C}
 * roc_video video = ...;
 * bool is_live;
 * roc_is_live(video, &is_live);
 * \endcode
 *
 * \param[in] video Video to query.
 * \param[out] is_live Live status of \p video.
 * \remark This function is \ref thread-safe.
 */
ROC_EXPORT roc_error roc_is_live(roc_video video, bool *is_live);

/*!
 * \brief Seek to the nearest \em keyframe for the given \ref roc_time
 *        timestamp.
 *
 * If \p timestamp does not correspond to a keyframe then \p backwards will
 * determine whether the video is advanced forwards to the next keyframe or
 * backwards to the previous keyframe.
 *
 * If \p timestamp is set to \ref ROC_NO_TIMESTAMP, the function will seek to
 * the next keyframe or backwards to the previous keyframe from the current
 * position in the video.
 *
 * If \p timestamp is beyond the duration of the video, the function will seek
 * to the end of the video and return without error.
 *
 * \section iterating_over_keyframes Iterating Over Keyframes
 * \par Iterate forward over keyframes.
 * \code{C}
 * roc_video video = ...;
 * while (true) {
 *     roc_image frame;
 *     roc_time timestamp;
 *     roc_read_frame(video, &frame, &timestamp);
 *     if (!frame.data)
 *         break;
 *     // Process the frame
 *     roc_free_image(frame);
 *     roc_seek_keyframe(video, ROC_NO_TIMESTAMP, false);
 * }
 * \endcode
 *
 * \par Seek to the nearest keyframe for the given timestamp.
 * \code{C}
 * roc_video video = ...;
 * roc_time timestamp = ...;
 * roc_seek_keyframe(video, timestamp, false);
 * \endcode
 *
 * \param[in] video Video to perform keyframe seek on.
 * \param[in] timestamp Timestamp to seek to.
 * \param[in] backwards Seek to the nearest keyframe before \p timestamp.
 * \remark This function is \ref reentrant.
 */
ROC_EXPORT roc_error roc_seek_keyframe(roc_video video,
                                       roc_time timestamp,
                                       bool backwards);

/*!
 * \brief Seek to the nearest \em frame for the given \ref roc_time timestamp.
 *
 * If \p timestamp is beyond the duration of the video, the function will seek
 * to the end of the video and return without error.
 *
 * \section iterating_over_frames Iterating Over Frames
 * \par Process a frame every second.
 * \code{C}
 * roc_video video = ...;
 * roc_time timestamp_of_interest = 0;
 * while (true) {
 *     roc_image frame;
 *     roc_time timestamp;
 *     roc_read_frame(video, &frame, &timestamp);
 *     if (!frame.data)
 *         break;
 *     // Process the frame
 *     roc_free_image(frame);
 *     timestamp_of_interest += 1000;
 *     roc_seek_frame(video, timestamp_of_interest, false);
 * }
 * \endcode
 *
 * \par Seek to the nearest frame for the given timestamp.
 * \code{C}
 * roc_video video = ...;
 * roc_time timestamp = ...;
 * roc_seek_frame(video, timestamp, false);
 * \endcode
 *
 * \param[in] video Video to perform frame seek on.
 * \param[in] timestamp Timestamp to seek to.
 * \remark This function is \ref reentrant.
 */
ROC_EXPORT roc_error roc_seek_frame(roc_video video,
                                    roc_time timestamp);

/*!
 * \brief Read the current frame and advance the video to the next frame.
 *
 * If no frames are left in \p video, \p frame.data will be set to \c NULL
 * and the function will return without error.
 *
 * Free \p frame after use with \ref roc_free_image.
 *
 * \par Example
 * \code{C}
 * roc_video video = ...;
 * roc_image frame;
 * roc_time timestamp;
 * roc_read_frame(video, &frame, &timestamp);
 * \endcode
 *
 * \param[in] video Video to decode.
 * \param[out] frame Address to store the decoded frame.
 * \param[out] timestamp Address to store the current position of the video
 *                       file in milliseconds or video capture timestamp,
 *                       or \c NULL if this value is not needed.
 * \remark This function is \ref reentrant.
 */
ROC_EXPORT roc_error roc_read_frame(roc_video video,
                                    roc_image *frame,
                                    roc_time *timestamp);

/*!
 * \brief Close an open video.
 *
 * This will free memory associated with \p video.
 * If \p video is \c NULL this operation is a no-op.
 *
 * \par Example
 * \code{C}
 * roc_video video = ...;
 * roc_close_video(video);
 * \endcode
 *
 * \param[in] video The video to close.
 * \remark This function is \ref reentrant.
 * \see \ref roc_open_video
 */
ROC_EXPORT roc_error roc_close_video(roc_video video);

/** @} */ // end of video_decoding
/** @} */ // end of media_decoding

/*!
 * \defgroup video_streaming Video Streaming
 * \brief A high-level interface for processing videos at a specified frame
 *        rate.
 *
 * Start a stream with \ref roc_stream_start and stop it with
 * \ref roc_stream_stop.
 * Control the stream with \ref roc_stream_set_frame_rate and
 * \ref roc_stream_set_paused.
 *
 * @{
 */

/*!
 * \brief Handle to a private streaming type.
 */
typedef struct roc_stream_type *roc_stream;

/*!
 * \brief A user-defined unique identifier corresponding a specific camera.
 */
typedef uint32_t roc_camera_id;

#ifndef SWIG
/*!
 * \brief Default value for a \ref roc_camera_id of unknown origin.
 *
 * Equal to 2^32 - 1.
 */
#define ROC_UNKNOWN_CAMERA 0xFFFFFFFF
#else // SWIG
%constant uint32_t ROC_UNKNOWN_CAMERA = 0xFFFFFFFF;
#endif // !SWIG

/*!
 * \brief Pointer to a callback function used to report video frames.
 *
 * The function should return \c void and take four parameters: a
 * <tt>void*</tt> callback context, a \ref roc_error error, a
 * \ref roc_image frame, and a \ref roc_camera_id camera.
 *
 * In the event of an \em error (non-\c NULL \ref roc_error) or an
 * \em end-of-stream (\c NULL \ref roc_image::data) this function will not be
 * called again by the stream.
 *
 * See \ref roc_example_track for an example.
 *
 * \note The \ref roc_image is owned by the stream and will be freed
 *       automatically when this function returns.
 * \note This function must not call \ref roc_stream_stop.
 */
typedef void (*roc_stream_callback)(void*,
                                    roc_error,
                                    roc_image,
                                    roc_camera_id);

/*!
 * \brief Start a video stream and report frames to a generic callback function.
 *
 * This function spawns two new threads and returns immediately.
 * A \em producer thread reads frames from \p video, and a \em consumer thread
 * calls \p callback when a new frame is available.
 *
 * The threads make a best-effort attempt to respect \p frames_per_second.
 * In the case that \p callback can't handle frames fast enough:
 * - For recorded videos, the producer will wait until the consumer has caught
 *   up.
 * - For live videos, the consumer will selectively drop frames so that
 *   \p callback is always seeing the most recent data.
 *
 * If \p callback is \c NULL then \ref roc_tracker_add_image_callback will be
 * used, in which case \p callback_context must be a \ref roc_tracker.
 *
 * Free a stream after use with \ref roc_stream_stop.
 *
 * \note \p stream takes ownership of \p video and will free it automatically.
 *
 * \param[out] stream Video stream to initialize.
 * \param[in] video Video to stream.
 * \param[in] camera_id Unique identifier for \p video.
 * \param[in] frames_per_second Desired frame rate.
 * \param[in] callback Function to call with frames from \p video.
 * \param[in] callback_context Pointer to arbitrary user data to provide to
 *                             \p callback.
 * \remark This function is \ref reentrant.
 * \see \ref roc_stream_stop
 */
ROC_EXPORT roc_error roc_stream_start(roc_stream *stream,
                                      roc_video video,
                                      roc_camera_id camera_id,
                                      float frames_per_second,
                                      roc_stream_callback callback,
                                      void *callback_context);

/*!
 * \brief Stop a video stream.
 *
 * Deallocates \p stream and sets it to \c NULL.
 *
 * \param[in,out] stream Stream to stop.
 * \remark This function is \ref reentrant.
 * \see \ref roc_stream_start
 */
ROC_EXPORT roc_error roc_stream_stop(roc_stream *stream);

/*!
 * \brief Change the processing frame rate.
 *
 * \param[in] stream Stream to change the frame rate of.
 * \param[in] frames_per_second Desired frame rate.
 * \remark This function is \ref thread-safe.
 */
ROC_EXPORT roc_error roc_stream_set_frame_rate(roc_stream stream,
                                               float frames_per_second);

/*!
 * \brief Check if a stream is paused.
 *
 * For pre-recorded videos that haven't been paused with
 * \ref roc_stream_set_paused this can also be used to test for the end of the
 * video.
 *
 * \param[in] stream Stream to check.
 * \return \c true if \p stream is paused, \c false otherwise.
 * \remark This function is \ref thread-safe.
 */
ROC_EXPORT bool roc_stream_is_paused(roc_stream stream);

/*!
 * \brief Pause or unpause a stream.
 *
 * \param[in] stream Stream to pause or unpause.
 * \param[in] paused Desired execution status.
 * \remark Thus function is \ref reentrant.
 */
ROC_EXPORT roc_error roc_stream_set_paused(roc_stream stream,
                                           bool paused);

/** @} */ // end of video_streaming

/*!
 * \defgroup template_generation Template Generation
 * \brief Process faces in an image.
 *
 * Process faces in an image with \ref roc_represent.
 * Free a face \ref roc_template after use with \ref roc_free_template.
 *
 * @{
 */

/*!
 * \brief Get the parameters used by \ref ROC_THUMBNAIL in \ref roc_represent.
 *
 * \par Example
 * \code{C}
 * int width, height;
 * float scale, quality;
 * roc_get_thumbnail_parameters(&width, &height, &scale, &quality);
 * \endcode
 *
 * \param[out] width Thumbnail image width in pixels.
 * \param[out] height Thumbnail image height in pixels.
 * \param[out] scale Thumbnail face scale, where larger values are a tighter
 *                   cropping.
 * \param[out] quality Thumbnail quality, where larger values are higher quality
 *                     at the expense of more storage.
 * \see \ref roc_set_thumbnail_parameters
 * \remark This function is \ref thread-safe.
 */
ROC_EXPORT roc_error roc_get_thumbnail_parameters(int *width,
                                                  int *height,
                                                  float *scale,
                                                  float *quality);

/*!
 * \brief Set the parameters used by \ref ROC_THUMBNAIL in \ref roc_represent.
 *
 * \par Example
 * \code{C}
 * roc_set_thumbnail_parameters(192, 256, 0.6, 32);
 * \endcode
 *
 * \param[in] width Thumbnail image width in pixels.
 *                  The default is \b 192, and the range is 32 to 1024.
 * \param[in] height Thumbnail image height in pixels.
 *                   The default is \b 256, and the range is 32 to 1024.
 * \param[in] scale Thumbnail face scale, where larger values are a tighter
 *                  cropping.
 *                  The default is \b 0.6, and the range is 0.1 to 1.0.
 * \param[in] quality Thumbnail quality, where larger values are higher quality
 *                    at the expense of more storage.
 *                    The default is \b 32, and the range is 0 to 100.
 * \see \ref roc_get_thumbnail_parameters
 * \remark This function is \ref thread-unsafe.
 */
ROC_EXPORT roc_error roc_set_thumbnail_parameters(int width,
                                                  int height,
                                                  float scale,
                                                  float quality);

/*!
 * \brief Supported algorithm configurations for use in \ref roc_represent.
 * \see \ref specifying_an_algorithm_id
 */
typedef enum roc_algorithm_options
{
    /*!
     * \brief Detect faces from -30 to +30 degrees yaw.
     */
    ROC_FRONTAL = 0x0000,

    /*!
     * \brief Detect faces from -100 to +100 degrees yaw.
     */
    ROC_FULL = 0x0001,

    /*!
     * \brief Detect faces in which the upper or lower half of the face is
     *        occluded.
     */
    ROC_PARTIAL = 0x0002,

    /*!
     * \brief Bypass automatic face detection by using caller-provided bounding
     *        boxes.
     *
     * \par Required Fields
     * Templates passed to \ref roc_represent are assumed to provide valid
     * values for:
     * - \ref roc_template::x
     * - \ref roc_template::y
     * - \ref roc_template::width
     * - \ref roc_template::height
     *
     * All other fields may be left uninitialized.
     *
     * \par When To Use
     * The suggested use case for \ref ROC_MANUAL is when the caller wishes to
     * isolate face representation (e.g. \ref ROC_FR) from face detection (e.g.
     * \ref ROC_FRONTAL).
     * The required fields must be obtained from a prior call to
     * \ref roc_represent or from \ref roc_landmarks_to_face.
     * Face bounding boxes obtained from other means are not supported as they
     * are not guaranteed to be the correct position and size.
     */
    ROC_MANUAL = 0x0004,

    /*!
     * \brief Detect faces with significant in-plane rotation.
     *
     * Rotate the image to detect faces with significant in-plane rotation
     * (roll).
     * By default faces are detected up to +/- 15 degrees roll.
     * With this flag faces are detected up to +/- 45 degrees roll.
     *
     * For images that are upside down or sideways see \ref roc_rotate.
     */
    ROC_ROLL = 0x0008,

    /*!
     * \brief Represent faces for comparison.
     *
     * See \ref ROC_FR_FAST for a faster alternative.
     */
    ROC_FR = 0x0010,

    /*!
     * \brief Faster but less accurate alternative to \ref ROC_FR.
     *
     * Approximately 7x faster at the cost of 4x the false positive rate.
     * Incompatible with \ref ROC_FULL.
     */
    ROC_FR_FAST = 0x0020,

    /*!
     * \brief Add \c Yaw and \c Pitch (degrees) estimation to the template
     *        metadata.
     *
     * Yaw is defined to be negative when the subject's face is oriented
     * towards the subject's left shoulder.
     * Pitch is defined to be negative when the subject's face is below the
     * camera.
     *
     * \c Yaw and \c Pitch estimation are expected to be accurate on faces in
     * the -30 to +30 degree range.
     */
    ROC_PITCHYAW = 0x0100,

    /*!
     * \brief Estimate age, emotion, ethnicity, and gender.
     *
     * The unit \c Age is years old, and ages under 18 are not supported.
     * For emotion estimation seven probability values are reported for
     * \c Anger, \c Disgust, \c Fear, \c Joy, \c Neutral, \c Sadness and \c
     * Surprise.
     * For ethnicity estimation five probability values are reported for
     * \c White, \c Black, \c Hispanic, \c Asian and \c Other.
     * For gender estimation two probability values are reported for \c Male and
     * \c Female.
     */
    ROC_DEMOGRAPHICS = 0x0200,

    /*!
     * \brief Three probability values are reported for \c None, \c Eye, and
     *        \c Sun.
     */
    ROC_GLASSES = 0x0400,

    /*!
     * \brief Add \c RightEyeX, \c RightEyeY, \c LeftEyeX, \c LeftEyeY,
     *        \c ChinX, \c ChinY, \c NoseRootX and \c NoseRootY pixel locations,
     *        and \c IOD (inter-occular pixel distance) to the template
     *        metadata.
     *
     * Certain \ref roc_algorithm_options imply this flag automatically.
     */
    ROC_LANDMARKS = 0x0800,

    /*!
     * \brief Add \c Lips position estimation to the template metadata, value is
     *        either \c Together or \c Apart.
     */
    ROC_LIPS = 0x1000,

    /*!
     * \brief Pad the image to detect faces that occupy a significant portion of
     *        the image.
     */
    ROC_PAD = 0x2000,

    /*!
     * \brief If face detection fails on low contrast images, normalize and
     *        retry.
     */
    ROC_ENHANCE_CONTRAST = 0x4000,

    /*!
     * \brief An aligned and cropped face image suitable for displaying.
     *
     * Align, scale, crop, \c JPEG encode, and store the face image in
     * \ref roc_template::tn and set \ref roc_template::tn_size accordingly.
     *
     * Control thumbnail quality with \ref roc_set_thumbnail_parameters.
     *
     * Decode a thumbnail with \ref roc_decode_image or write it to a file and
     * open it with \ref roc_read_image.
     *
     * \par Example
     * \code{C}
     * roc_image image = ...;
     * roc_template t;
     * roc_represent(image, ROC_FRONTAL | ROC_THUMBNAIL, 36, 1, 0.02f, &t);
     * if (t.algorithm_id & ROC_INVALID == 0) {
     *     FILE *thumbnail_file = fopen("thumbnail.jpg", "wb");
     *     fwrite(t.tn, t.tn_size, 1, thumbnail_file);
     *     fclose(thumbnail_file);
     * }
     * roc_free_template(&t);
     * \endcode
     */
    ROC_THUMBNAIL = 0x8000,

    /*!
     * \brief The \ref roc_template was not initialized by
     *        \ref roc_represent.
     */
    ROC_INVALID = 0x010000,

    /*!
     * \brief Add \c SpoofFF metric for static \ref face_liveness detection with
     *        a fixed-focus camera sensor.
     *
     * A real-valued number between 0.0 and 1.0 where a higher value indicates a
     * greater chance of a spoof face and a lower value indicates a greater
     * chance of a live face.
     * Due to the diverse nature of camera sensors, a single threshold may not
     * be optimal across multiple sensors.
     * Please thoroughly test the performance on sensors of interest to select
     * an appropriate threshold for your needs.
     *
     * \note This metric is specifically designed with front-facing mobile
     *       cameras in mind.
     *       Therefore, the following engineering constraints should be
     *       observed:
     *        - Capture images with approximately one-megapixel resolution.
     *        - Interocular distance (\c IOD as reported in the
     *          \ref template_metadata) should be between 140 and 300 pixels
     *          (typical of a "selfie" image).
     *        - Images should not be compressed, resized, or otherwise modified
     *          (i.e. raw image).
     *          We recommend capturing images in the YUV format and initializing
     *          a \ref roc_image using the  values from the Y channel (grayscale
     *          image) directly.
     * \see \ref ROC_SPOOF_AF
     */
    ROC_SPOOF_FF = 0x020000,

    /*!
     * \brief Add \c SpoofAF metric for static \ref face_liveness detection with
     *        an auto-focus camera sensor.
     *
     * See the description of \ref ROC_SPOOF_FF for details.
     * \see \ref ROC_SPOOF_FF
     */
    ROC_SPOOF_AF = 0x040000,

    /*!
     * \brief Don't parallelize face representation.
     *
     * By default each detected face is processed in parallel up to the thread
     * count limitation imposed by the license file.
     */
    ROC_SERIAL = 0x080000,

    /*!
     * \brief The format of \ref roc_template::fv changes between most releases,
     *        this field is used to check template compatibility.
     *
     * \code
     * roc_template t = ...;
     * if (t->algorithm_id & ROC_TEMPLATE_VERSION_MASK == ROC_TEMPLATE_VERSION)
     *     ...; // generated with current SDK version
     * \endcode
     * \see \ref ROC_FR_COMPATIBILITY_VERSION
     */
    ROC_TEMPLATE_VERSION = (ROC_VERSION_MAJOR << 28)
                         + (ROC_VERSION_MINOR << 25),

    /*!
     * \brief Minimum compatible version for templates generated with
     *        \ref ROC_FR or \ref ROC_FR_FAST.
     *
     * \code
     * roc_template t = ...;
     * if (t->algorithm_id & ROC_TEMPLATE_VERSION_MASK >= ROC_FR_COMPATIBILITY_VERSION)
     *     ...; // compatible with current SDK version
     * \endcode
     * \see \ref ROC_TEMPLATE_VERSION
     */
    ROC_FR_COMPATIBILITY_VERSION = (ROC_VERSION_MAJOR << 28)
                                 + (ROC_VERSION_MINOR << 25),

    /*!
     * \brief See \ref ROC_TEMPLATE_VERSION.
     */
    ROC_TEMPLATE_VERSION_MASK = 0x7F000000,
} roc_algorithm_options;

/*!
 * \brief A combination of \ref roc_algorithm_options.
 * \see \ref specifying_an_algorithm_id
 */
typedef uint32_t roc_algorithm_id;

/*!
 * \brief Pre-load the model files to be used by \ref roc_represent.
 *
 * Call this function once after \ref roc_initialize with the \p algorithm_id
 * that will be used in \ref roc_represent.
 * See \ref lazy_initialization for details.
 *
 * \par Example
 * \code{C}
 * roc_preload(ROC_FRONTAL | ROC_FR);
 * \endcode
 *
 * \param[in] algorithm_id Pre-load the model files to be used by
 *                         \ref roc_represent for this \ref roc_algorithm_id.
 * \remark This function is \ref thread-safe.
 */
ROC_EXPORT roc_error roc_preload(roc_algorithm_id algorithm_id);

/*!
 * \brief Return an error if the specified \ref roc_algorithm_id is incompatible
 *        with this version of the SDK.
 *
 * \param[in] algorithm_id Value to check for compatibility.
 * \remark This function is \ref thread-safe.
 */
ROC_EXPORT roc_error roc_check_template_version(roc_algorithm_id algorithm_id);

/*!
 * \brief Estimate the face bounding box from landmarks.
 *
 * Used for face representation with \ref ROC_MANUAL.
 * The eyes and chin may come from another source (e.g. human annotator or
 * third-party algorithm) provided that the eye locations correspond to the
 * center of the eye socket and the chin location corresponds to the
 * bottom-center of the lower jaw.
 *
 * \section frontal_versus_profile Frontal vs. Profile
 * A face is considered \em profile if the head yaw is severe enough to make
 * only one eye visible.
 * Otherwise a face is considered \em frontal.
 *
 * \section right_versus_left Right vs. Left
 * The \a right and \a left sides of the face are defined from the perspective
 * of the subject, not the image.
 * Thus the right eye is the one with the lesser \c x location, and the left
 * eye is the one with the greater \c x location.
 * Similarly, a \em right-profile face is one that looks in the direction of the
 * subject's right shoulder (toward \c x=0), and a \em left-profile face is one
 * that looks in the direction of the subject's left shoulder (away from
 * \c x=0).
 *
 * \par Example
 * \code{C}
 * float right_eye_x = ...;
 * float right_eye_y = ...;
 * float left_eye_x = ...;
 * float left_eye_y = ...;
 * float chin_x = ...;
 * float chin_y = ...;
 * float face_x, face_y, face_width, face_height;
 * roc_landmarks_to_face(right_eye_x, right_eye_y,
 *                       left_eye_x, left_eye_y,
 *                       chin_x, chin_y,
 *                       &face_x, &face_y, &face_width, &face_height);
 * \endcode
 * \param[in] right_eye_x Right eye horizontal location (pixels).
 * \param[in] right_eye_y Right eye vertical location (pixels).
 * \param[in] left_eye_x Left eye horizontal location (pixels).
 * \param[in] left_eye_y Left eye vertical location (pixels).
 * \param[in] chin_x Chin horizontal location (pixels).
 * \param[in] chin_y Chin vertical location (pixels).
 * \param[out] face_x Face horizontal offset (pixels).
 * \param[out] face_y Face vertical offset (pixels).
 * \param[out] face_width Face horizontal size (pixels).
 * \param[out] face_height Face vertical size (pixels).
 */
ROC_EXPORT roc_error roc_landmarks_to_face(float right_eye_x,
                                           float right_eye_y,
                                           float left_eye_x,
                                           float left_eye_y,
                                           float chin_x,
                                           float chin_y,
                                           float *face_x,
                                           float *face_y,
                                           float *face_width,
                                           float *face_height);

#ifndef SWIG
/*!
 * \brief Default value for \ref roc_template::timestamp, indicating the
 *        template was generated from a still image.
 *
 * Equal to 2^32 - 1.
 */
#define ROC_NO_TIMESTAMP 0xFFFFFFFF
#else // SWIG
%constant uint32_t ROC_NO_TIMESTAMP = 0xFFFFFFFF;
#endif // !SWIG

/*!
 * \brief A unique identifier used to associate multiple \ref roc_template.
 * \see \ref identity_discovery
 */
typedef uint32_t roc_person_id;

#ifndef SWIG
/*!
 * \brief Default value for \ref roc_template::person_id, indicating the
 *        template has no known associations.
 *
 * Equal to 2^32 - 1.
 */
#define ROC_UNKNOWN_PERSON 0xFFFFFFFF
#else // SWIG
%constant uint32_t ROC_UNKNOWN_PERSON = 0xFFFFFFFF;
#endif // !SWIG

#ifndef SWIG
/*!
 * \brief Length of a \ref ROC_FR feature vector in bytes.
 */
#define ROC_FR_FV_SIZE 165
#else // SWIG
%constant uint32_t ROC_FR_FV_SIZE = 165;
#endif // !SWIG

#ifndef SWIG
/*!
 * \brief Length of a \ref ROC_FR_FAST feature vector in bytes.
 */
#define ROC_FR_FAST_FV_SIZE 85
#else // SWIG
%constant uint32_t ROC_FR_FAST_FV_SIZE = 85;
#endif // !SWIG

/*!
 * \brief A digital encoding of a face.
 *
 * A pair of templates can be compared for similarity.
 *
 * Templates are designed with the following considerations in mind:
 * - Maximizing the retention of the unique identifying characteristics of a
 *   face.
 * - Minimizing the computation time needed to compare two templates.
 * - Minimizing the computation time needed to generate a template.
 * - Minimizing the storage space required to save a template.
 *
 * \section template_metadata Metadata
 * A template's <i>metadata</i> string is a <a href="http://json.org/"><i>Java
 * Script Object Notation</i> (JSON)</a> object containing at least the
 * following keys:
 * | Key          | Definition                                                                                                                   |
 * |--------------|------------------------------------------------------------------------------------------------------------------------------|
 * | \c RightEyeX | Right eye horizontal location (pixels), see \ref right_versus_left.                                                          |
 * | \c RightEyeY | Right eye vertical location (pixels).                                                                                        |
 * | \c LeftEyeX  | Left eye horizontal location (pixels), see \ref right_versus_left.                                                           |
 * | \c LeftEyeY  | Left eye vertical location (pixels).                                                                                         |
 * | \c ChinX     | Chin horizontal location (pixels).                                                                                           |
 * | \c ChinY     | Chin vertical location (pixels).                                                                                             |
 * | \c IOD       | Interocular distance between the right and left eyes (pixels).                                                               |
 * | \c Pose      | Pose estimation (\c Frontal, \c RightProfile or \c LeftProfile), see \ref frontal_versus_profile and \ref right_versus_left. |
 * | \c Quality   | See \ref face_quality.                                                                                                       |
 * | \c Roll      | Face roll (degrees clockwise from vertical).                                                                                 |
 *
 * Additional keys exist for the requested \ref metadata_options, including:
 * | Key          | Definition                 |
 * |--------------|----------------------------|
 * | \c Pitch     | See \ref ROC_PITCHYAW.     |
 * | \c Yaw       | See \ref ROC_PITCHYAW.     |
 * | \c Age       | See \ref ROC_DEMOGRAPHICS. |
 * | \c Male      | See \ref ROC_DEMOGRAPHICS. |
 * | \c Female    | See \ref ROC_DEMOGRAPHICS. |
 * | \c White     | See \ref ROC_DEMOGRAPHICS. |
 * | \c Black     | See \ref ROC_DEMOGRAPHICS. |
 * | \c Hispanic  | See \ref ROC_DEMOGRAPHICS. |
 * | \c Asian     | See \ref ROC_DEMOGRAPHICS. |
 * | \c Other     | See \ref ROC_DEMOGRAPHICS. |
 * | \c Glasses   | See \ref ROC_GLASSES.      |
 * | \c Lips      | See \ref ROC_LIPS.         |
 * | \c SpoofFF   | See \ref ROC_SPOOF_FF.     |
 * | \c SpoofAF   | See \ref ROC_SPOOF_AF.     |
 *
 * Metadata may be queried and edited using \ref roc_get_metadata and
 * \ref roc_set_metadata.
 *
 * \section feature_vector Feature Vector
 * A template's <i>feature vector</i> is the statistical model of the
 * distinguishing characteristics of a face and is the basis for measuring
 * \ref roc_similarity.
 * It is stored in the template as \ref roc_template::fv and has length
 * \ref roc_template::fv_size.
 */
typedef struct roc_template
{
    roc_algorithm_id algorithm_id; /*!< \brief The value of
                                               \ref roc_algorithm_id passed to
                                               \ref roc_represent to construct
                                               this template bitwise OR'd with
                                               \ref ROC_TEMPLATE_VERSION. */
    roc_time timestamp; /*!< \brief Face video timestamp or
                                    \ref ROC_NO_TIMESTAMP by default. */
    int32_t x; /*!< \brief Face horizontal offset (pixels). */
    int32_t y; /*!< \brief Face vertical offset (pixels). */
    uint32_t width;  /*!< \brief Face horizontal size (pixels). */
    uint32_t height; /*!< \brief Face vertical size (pixels). */
    float confidence; /*!< \brief Face detection confidence. */
    roc_person_id person_id; /*!< \brief Unique identifier for the person or
                                         \ref ROC_UNKNOWN_PERSON by default. */
    uint32_t md_size; /*!< \brief Length of metadata buffer \ref md.
                                  This value is greater than or equal to
                                  <tt>strlen+1</tt> of \ref md. */
    uint32_t fv_size; /*!< \brief Length of the feature vector \ref fv. */
    uint32_t tn_size; /*!< \brief Length of face thumbnail \ref tn. */
    const char *md; /*!< \brief Null-terminated JSON string. */
    const uint8_t *fv; /*!< \brief \ref feature_vector of length
                                   \ref fv_size. */
    const uint8_t *tn; /*!< \brief JPEG-encoded face thumbnail of length
                                   \ref tn_size, see \ref ROC_THUMBNAIL. */
} roc_template;

/*!
 * \brief Size of the fixed-length portion of a \ref roc_template.
 * \remark This function is \ref thread-safe.
 */
extern const size_t ROC_EXPORT roc_template_header_size;

/*!
 * \brief A method for determining the minimum face detection size as a fraction
 *        of the image size.
 *
 * In the interest of efficiency, it is recommended to set a lower bound on the
 * minimum face detection size as a fraction of the image size.
 * Given a \em relative minimum size of \c 4% of the image dimensions, and an
 * \em absolute minimum size of \c 36 pixels, the \em adaptive minimum size is:
 * <tt>max(max(image.width, image.height) * 0.04, 36)</tt>.
 *
 * \par Example
 * \code{C}
 * roc_image image = ...;
 * size_t adaptive_minimum_size;
 * roc_adaptive_minimum_size(image, 0.04, 36, &adaptive_minimum_size);
 * \endcode
 *
 * \param[in] image Image dimensions to use for the relative minimum size
 *                  calculation.
 * \param[in] relative_minimum_size Faces will be no smaller than this fraction
 *                                  of the image width and height.
 * \param[in] absolute_minimum_size Faces will be no smaller than this in
 *                                  pixels.
 * \param[out] adaptive_minimum_size Maximum of the relative and absolute
 *                                   minimum sizes.
 * \remark This function is \ref thread-safe.
 */
ROC_EXPORT roc_error roc_adaptive_minimum_size(roc_image image,
                                               float relative_minimum_size,
                                               size_t absolute_minimum_size,
                                               size_t *adaptive_minimum_size);

/*!
 * \brief Detect faces in an image and process them into templates for
 *        comparison.
 *
 * Each face is represented by a \ref roc_template.
 * In the case that the number of representable faces is less than \p k, the
 * trailing elements of \p templates will have \ref roc_template::algorithm_id
 * set to \ref ROC_INVALID.
 * In the case that the number of representable faces is greater than \p k, the
 * implementation will choose which faces to represent, see \ref maximum_faces
 * for details.
 * The populated \p templates will be returned sorted by decreasing
 * \ref roc_template::confidence.
 *
 * Each \ref roc_template in \p templates should be freed after use with
 * \ref roc_free_template.
 * Templates with the \ref ROC_INVALID flag set do not have any allocated memory
 * associated with them, freeing them is a no-op and therefore optional.
 *
 * This function is exposed in the \ref cli as \ref roc-represent and
 * \ref roc-represent-video.
 *
 * \section specifying_an_algorithm_id Specifying an Algorithm ID
 * The \p algorithm_id is a combination of \ref roc_algorithm_options indicating
 * how to process faces in an image.
 * There are three components to an algorithm id: detection, representation and
 * metadata.
 * Components are combined together using a \em bitwise-or operation, as in
 * \ref algorithm_id_examples.
 * You do not need to specify \ref ROC_TEMPLATE_VERSION, this will be added
 * automatically.
 *
 * \subsection detection_options Detection Options
 * <i>Exactly one</i> of the below options must be specified to indicate the
 * desired face detection algorithm.
 *
 * | Detection Options | Description            |
 * |-------------------|------------------------|
 * | \ref ROC_FRONTAL  | \copybrief ROC_FRONTAL |
 * | \ref ROC_FULL     | \copybrief ROC_FULL    |
 * | \ref ROC_PARTIAL  | \copybrief ROC_PARTIAL |
 * | \ref ROC_MANUAL   | \copybrief ROC_MANUAL  |
 *
 * <i>Zero or more</i> of the below options may be specified to indicate
 * additional behavior.
 *
 * | Detection Options | Description         |
 * |-------------------|---------------------|
 * | \ref ROC_ROLL     | \copybrief ROC_ROLL |
 * | \ref ROC_PAD      | \copybrief ROC_PAD  |
 *
 * \subsection representation_options Representation Options
 * <i>At most one</i> option should be specified to indicate the desired face
 * comparison mechanism.
 * If no option is specified, \ref roc_template::fv_size will be zero and the
 * resulting \ref roc_template <i>can not</i> be used in
 * \ref roc_compare_templates or \ref roc_search.
 *
 * | Representation Options | Description               |
 * |------------------------|---------------------------|
 * | \ref ROC_FR            | \copybrief ROC_FR         |
 * | \ref ROC_FR_FAST       | \copybrief ROC_FR_FAST    |
 * | \em None               | No face comparison needed |
 *
 * \subsection metadata_options Metadata Options
 * Each metadata option may be <i>optionally</i> included to add additional
 * fields to the \ref template_metadata.
 *
 * | Metadata Options      | Description                   |
 * |-----------------------|-------------------------------|
 * | \ref ROC_PITCHYAW     | \copybrief ROC_PITCHYAW       |
 * | \ref ROC_DEMOGRAPHICS | \copybrief ROC_DEMOGRAPHICS   |
 * | \ref ROC_GLASSES      | \copybrief ROC_GLASSES        |
 * | \ref ROC_LANDMARKS    | \copybrief ROC_LANDMARKS      |
 * | \ref ROC_LIPS         | \copybrief ROC_LIPS           |
 * | \ref ROC_THUMBNAIL    | \copybrief ROC_THUMBNAIL      |
 * | \ref ROC_SPOOF_AF     | \copybrief ROC_SPOOF_AF       |
 * | \ref ROC_SPOOF_FF     | \copybrief ROC_SPOOF_FF       |
 * | \em None              | No additional metadata needed |
 *
 * \subsection algorithm_id_examples Algorithm ID Examples
 * \code{C}
 * // Detect frontal faces and represent for comparison.
 * roc_algorithm_id algorithm_id = ROC_FRONTAL | ROC_FR;
 * \endcode
 * \code{C}
 * // Detect frontal faces, represent for comparison, and report demographics
 * // and pose estimation.
 * roc_algorithm_id algorithm_id = ROC_FRONTAL | ROC_FR |
 *                                 ROC_DEMOGRAPHICS | ROC_PITCHYAW;
 * \endcode
 * \code{C}
 * // Detect faces across all yaw ranges, report pose estimation, and construct
 * // a thumbnail.
 * roc_algorithm_id algorithm_id = ROC_FULL | ROC_PITCHYAW | ROC_THUMBNAIL;
 * \endcode
 *
 * \section minimum_size Minimum Size
 * The \p min_size parameter indicates the smallest face to detect in an image.
 * Face detection size is measured by the width of the face in pixels.
 * The suggested default value for \p min_size is \c 36, which corresponds
 * roughly to 18 pixels between the eyes.
 * A larger value for \p min_size will result in faster execution speed as less
 * time will be spent scanning the image for small faces.
 * While faces smaller than 36 pixels can be detected, they will be unreliable
 * for recognition.
 * See \ref roc_adaptive_minimum_size for computing the minimum size relative to
 * the image dimensions.
 *
 * \section maximum_faces Maximum Faces
 * The parameter \p k indicates the maximum number of faces to detect in an
 * image.
 * When \p k is less than the number of faces in the image, the faces with the
 * highest \ref roc_template::confidence will be returned.
 *
 * \subsection k_equals_negative_one k == -1
 * When <code>k == -1</code> the following special-case behaviors apply:
 *  - If a large face above the confidence threshold is found, the detector will
 *    return early without scanning the image for smaller faces.
 *  - If the highest confidence face is below the confidence threshold, it will
 *    still be returned.
 *  - If no face is found, the detector will return the entire area of the image
 *    as the detected face.
 *
 * This is useful for enrolling controlled capture datasets where exactly one
 * face is expected.
 *
 * \section false_detection_rate False Detection Rate
 * The \p false_detection_rate parameter specifies the allowable false positive
 * rate for face detection.
 * The suggested default value for \p false_detection_rate is \c 0.02 which
 * corresponds to one false detection in 50 images on the
 * <a href="http://vis-www.cs.umass.edu/fddb/">FDDB</a> benchmark.
 * A higher false detection rate will correctly detect more faces at the cost of
 * also incorrectly detecting more non-faces.
 * The accepted range of values for \p false_detection_rate is between \c 0 and
 * \c 1 inclusive.
 * Values outside this range will be truncated to the aforementioned bounds
 * automatically.
 *
 * \section lazy_initialization Lazy Initialization
 * The first call to \ref roc_represent incurs additional overhead as the model
 * files needed to execute the specified \p algorithm_id are loaded from disk.
 * Call \ref roc_preload beforehand to avoid incuring this overhead on the first
 * call.
 *
 * \section operating_system_compatibility Operating System Compatibililty
 * Identical templates are produced across all operating systems when provided
 * the same input parameters.
 * Templates are completely compatible when transfered to a different operating
 * system.
 *
 * \par Example
 * \code{C}
 * roc_image image = ...;
 * roc_template templates[3];
 * roc_represent(image, ROC_FRONTAL | ROC_FR, 36, 3, 0.02f,
 *               (roc_template*) &templates);
 * \endcode
 *
 * \param[in] image Input image to process for faces.
 * \param[in] algorithm_id Algorithm identifier, see
 *                         \ref specifying_an_algorithm_id.
 * \param[in] min_size Minimum size face to detect, see \ref minimum_size.
 * \param[in] k The desired number of face templates, see \ref maximum_faces.
 * \param[in] false_detection_rate The lower bound for face detection
 *                                 confidence, see \ref false_detection_rate.
 * \param[out] templates Pre-allocated array to hold the generated templates.
 * \remark This function is \ref reentrant and \ref parallel.
 */
ROC_EXPORT roc_error roc_represent(roc_image image,
                                   roc_algorithm_id algorithm_id,
                                   size_t min_size,
                                   int k,
                                   float false_detection_rate,
                                   roc_template *templates);

/*!
 * \brief Call this function on a template after it is no longer needed.
 *
 * Frees the memory previously allocated for \ref roc_template::fv,
 * \ref roc_template::md, and \ref roc_template::tn.
 * Sets \ref roc_template::algorithm_id to \ref ROC_INVALID.
 * If \p template_ is \c NULL, this function does nothing.
 * \param[in,out] template_ The template to deallocate.
 * \remark This function is \ref reentrant.
 */
ROC_EXPORT roc_error roc_free_template(roc_template *template_);

/*!
 * \brief Query the remaining representation limit.
 *
 * Some licenses of the ROC SDK include a limit to the number of representations
 * that can be made over the lifetime of the license.
 * The number of representations is the number of times \ref roc_represent has
 * been called.
 * This function returns the number of representations specified in the license
 * file \em less the number of representations used to date.
 *
 * \par Example
 * \code{C}
 * size_t representation_limit;
 * roc_representation_limit(&representation_limit);
 * \endcode
 *
 * \param[out] representation_limit The remaining number of representations
 *                                  available.
 * \remark This function is \ref thread-safe.
 * \see \ref roc_comparison_limit \ref roc_template_limit
 */
ROC_EXPORT roc_error roc_representation_limit(size_t *representation_limit);

/** @} */ // end of template_generation

/*!
 * \defgroup template_io Template I/O
 * \brief Read and write templates.
 *
 * Copy a template with \ref roc_copy_template.
 * Serialize and deserialze templates to memory buffers with \ref roc_flatten
 * and \ref roc_unflatten.
 * Read and write templates using file pointers with \ref roc_read_template and
 * \ref roc_write_template, or file descriptors with \ref roc_read_template_fd
 * and \ref roc_write_template_fd.
 * Read and write template metadata with \ref roc_get_metadata and
 * \ref roc_set_metadata or \ref roc_get_metadata_double and
 * \ref roc_set_metadata_double.
 *
 * @{
 */

/*!
  * \brief Deep-copy a template.
  *
  * In-place copying is supported with \p dst pointing to \p src.
  *
  * Free \p dst after use with \ref roc_free_template.
  *
  * \par Example
  * \code{C}
  * roc_template src = ...;
  * roc_template dst;
  * roc_copy_template(src, &dst);
  * \endcode
  *
  * \param[in] src Template to copy.
  * \param[out] dst Unallocated pointer to hold a copy of \p src.
  * \remark This function is \ref reentrant.
  */
ROC_EXPORT roc_error roc_copy_template(const roc_template src,
                                       roc_template *dst);

/*!
  * \brief Serialize a template to a memory buffer.
  *
  * Use \ref roc_flattened_bytes to determine the appropriate size for
  * \p buffer.
  *
  * \par Example
  * \code{C}
  * roc_template template_ = ...;
  * size_t bytes = ...;
  * uint8_t *buffer = (uint8_t*) malloc(bytes);
  * roc_flatten(template_, buffer);
  * \endcode
  *
  * \param[in] template_ Template to serialize.
  * \param[out] buffer Pre-allocated buffer to hold the serialized template.
  * \remark This function is \ref reentrant.
  * \see \ref roc_unflatten
  */
ROC_EXPORT roc_error roc_flatten(const roc_template template_,
                                 uint8_t *buffer);

/*!
 * \brief Deserialize a template from a memory buffer.
 *
 * Free \p template_ after use with \ref roc_free_template.
 *
 * \par Example
 * \code{C}
 * uint8_t *buffer = ...;
 * roc_template template_;
 * roc_unflatten(buffer, &template_);
 * \endcode
 *
 * \param[in] buffer Buffer holding the serialized template.
 * \param[out] template_ Uninitialized address to hold the deserialized
 *                       template.
 * \remark This function is \ref reentrant.
 * \see \ref roc_flatten
 */
ROC_EXPORT roc_error roc_unflatten(const uint8_t *buffer,
                                   roc_template *template_);

/*!
 * \brief Calculate the bytes required to flatten a template.
 *
 * Use this function to determine the appropriate buffer size for
 * \ref roc_flatten.
 *
 * \par Example
 * \code{C}
 * roc_template template_ = ...;
 * size_t bytes;
 * roc_flattened_bytes(template_, &bytes);
 * \endcode
 *
 * \param[in] template_ Template to calculate the serialized size.
 * \param[out] bytes Bytes required to serialize the template.
 * \remark This function is \ref thread-safe.
 */
ROC_EXPORT roc_error roc_flattened_bytes(const roc_template template_,
                                         size_t *bytes);

/*!
 * \brief Read a template from a file pointer.
 *
 * Use this function to read from a \ref roc_gallery_file.
 * On success, \p file will be advanced to the end of \p template_.
 * If the \em end-of-file (EOF) is reached before reading a complete template,
 * \ref roc_template::algorithm_id will be set to \ref ROC_INVALID, \p file will
 * be reset to its original position, and the function will return without
 * error.
 *
 * Free \p template_ after use with \ref roc_free_template.
 *
 * \warning On Windows it is not recommended to pass C runtime objects across
 *          library boundaries, therefore this function may not work.
 *          See \ref roc_gallery_file for alternatives.
 *
 * \par Example
 * \code{C}
 * roc_template template_;
 * FILE *file = fopen("templates.t", "rb");
 * roc_read_template(file, &template_);
 * \endcode
 *
 * \param[in] file File to read from.
 * \param[out] template_ Pointer to unallocated template to construct.
 * \remark This function is \ref reentrant.
 * \see \ref roc_write_template
 */
ROC_EXPORT roc_error roc_read_template(FILE *file,
                                       roc_template *template_);

/*!
 * \brief Read a template from a file descriptor.
 *
 * Use this function to read from a \ref roc_gallery_file.
 * On success, \p file will be advanced to the end of \p template_.
 * If the \em end-of-file (EOF) is reached before reading a complete template,
 * \ref roc_template::algorithm_id will be set to \ref ROC_INVALID, \p file will
 * be reset to its original position, and the function will return without
 * error.
 *
 * Free \p template_ after use with \ref roc_free_template.
 *
 * \par Example
 * \code{C}
 * roc_template template_;
 * int fd = open("templates.t", O_RDONLY);
 * roc_read_template_fd(fd, &template_);
 * \endcode
 *
 * \param[in] fd File descriptor to read from.
 * \param[out] template_ Pointer to unallocated template to construct.
 * \remark This function is \ref reentrant.
 * \see \ref roc_write_template_fd
 */
ROC_EXPORT roc_error roc_read_template_fd(int fd,
                                          roc_template *template_);

/*!
 * \brief Write a template to a file pointer.
 *
 * Use this function to write to a \ref roc_gallery_file.
 * On success, \p file will be advanced to the end of \p template_.
 *
 * This function calls \c fwrite but not \c fflush.
 *
 * \warning On Windows it is not recommended to pass C runtime objects across
 *          library boundaries, therefore this function may not work.
 *          See \ref roc_gallery_file for alternatives.
 *
 * \par Example
 * \code{C}
 * roc_template template_ = ...;
 * FILE *file = fopen("templates.t", "wb");
 * roc_write_template(file, template_);
 * \endcode
 *
 * \param[in] file File to write to.
 * \param[out] template_ Template to write.
 * \remark This function is \ref reentrant.
 * \see \ref roc_read_template
 */
ROC_EXPORT roc_error roc_write_template(FILE *file,
                                        const roc_template template_);

/*!
 * \brief Write a template to a file descriptor.
 *
 * Use this function to write to a \ref roc_gallery_file.
 * On success, \p file will be advanced to the end of \p template_.
 *
 * This function calls \c write but not \c fsync.
 *
 * \par Example
 * \code{C}
 * roc_template template_ = ...;
 * int fd = fopen("templates.t", O_WRONLY);
 * roc_write_template_fd(fd, template_);
 * \endcode
 *
 * \param[in] fd File descriptor to write to.
 * \param[out] template_ Template to write.
 * \remark This function is \ref reentrant.
 * \see \ref roc_read_template_fd
 */
ROC_EXPORT roc_error roc_write_template_fd(int fd,
                                           const roc_template template_);

/*!
 * \brief Retrieve the value for a metadata key.
 *
 * If \p key does not exist then \p value will be set to \c NULL.
 * For values of type \c double, see \ref roc_get_metadata_double.
 *
 * Template metadata is JSON formatted, see \ref template_metadata for details.
 *
 * Free \p value after use with \ref roc_free_string.
 *
 * \par Example
 * \code{C}
 * roc_template template_ = ...;
 * roc_string value;
 * roc_get_metadata(template_, "LeftEyeX", &value);
 * \endcode
 *
 * \param[in] template_ Template to retrieve the metadata value from.
 * \param[in] key Key to retrieve the value of.
 * \param[out] value Value corresponding to the key.
 * \see \ref roc_set_metadata
 */
ROC_EXPORT roc_error roc_get_metadata(const roc_template template_,
                                      const char *key,
                                      roc_string *value);

/*!
 * \brief Add, update, or remove a single key/value pair in the metadata of a
 *        template.
 *
 * If \p value is \c NULL then \p key will be removed from the metadata.
 * After this function returns, the \ref roc_template::md field of \p template_
 * will point to a new string.
 * This function does not modify other metadata entries.
 * For values of type \c double, see \ref roc_get_metadata_double.
 *
 * Template metadata is JSON formatted, see \ref template_metadata for details.
 *
 * \par Example
 * \code{C}
 * roc_template template_ = ...;
 * roc_set_metadata(template_, "LeftEyeX", "127");
 * \endcode
 *
 * \param[in,out] template_ Template to update the metadata of.
 * \param[in] key Metadata key to set.
 * \param[in] value Metadata value to set the key to.
 * \remark This function is \ref reentrant.
 * \see \ref roc_get_metadata \ref roc_reserve_metadata
 */
ROC_EXPORT roc_error roc_set_metadata(roc_template *template_,
                                      const char *key,
                                      const char *value);

/*!
 * \brief Allocate additional metadata space to allow later in-place editing.
 *
 * To expand the template metadata buffer, \p md_size should be larger than
 * \p template_ \ref roc_template::md_size.
 * After this operation \p template_ \ref roc_template::md_size will equal
 * \p md_size, and be larger than <tt>strlen+1</tt> of \ref roc_template::md.
 *
 * To clear the metadata buffer, \p md_size should be 0, in which case
 * \p template_ \ref roc_template::md will be set to \c NULL.
 *
 * If \p md_size is less than or equal to \p template_
 * \ref roc_template::md_size this function is a no-op.
 *
 * \param[in,out] template_ Template to update the metadata of.
 * \param[in] md_size New metadata buffer size.
 * \remark This function is \ref reentrant.
 * \see \ref roc_set_metadata
 */
ROC_EXPORT roc_error roc_reserve_metadata(roc_template *template_,
                                          uint32_t md_size);

/*!
 * \brief Retrieve the value for a metadata key of type \c double.
 *
 * A convenient alternative to \ref roc_get_metadata for numerical values.
 * Works for both integer and floating-point values.
 * If \p key does not exist then \p value will be set to \c NAN.
 *
 * \par Example
 * \code{C}
 * roc_template template_ = ...;
 * double value;
 * roc_get_metadata_double(template_, "LeftEyeX", &value);
 * \endcode
 *
 * \param[in] template_ Template to retrieve the metadata value from.
 * \param[in] key Key to retrieve the value of.
 * \param[out] value Value corresponding to the key.
 * \see \ref roc_set_metadata_double
 */
ROC_EXPORT roc_error roc_get_metadata_double(const roc_template template_,
                                             const char *key,
                                             double *value);

/*!
 * \brief Add, update, or remove a single key/value pair in the metadata of a
 *        template of type \c double.
 *
 * A convenient alternative to \ref roc_set_metadata for numerical values.
 * Works for both integer and floating-point values.
 * If \p value is \c NAN then \p key will be removed from the metadata.
 *
 * \par Example
 * \code{C}
 * roc_template template_ = ...;
 * roc_set_metadata_double(template_, "LeftEyeX", 127);
 * \endcode
 *
 * \param[in,out] template_ Template to update the metadata of.
 * \param[in] key Metadata key to set.
 * \param[in] value Metadata value to set the key to.
 * \remark This function is \ref reentrant.
 * \see \ref roc_get_metadata_double
 */
ROC_EXPORT roc_error roc_set_metadata_double(roc_template *template_,
                                             const char *key,
                                             double value);

/*!
 * \brief Cast and return a <tt>uint8_t*</tt> as a <tt>void*</tt>.
 *
 * Helper function for some of the API language wrappers that don't support this
 * type cast.
 *
 * \param[in] pointer Input pointer.
 * \remark This function is \ref thread-safe.
 */
ROC_EXPORT void *roc_cast(uint8_t *pointer);

/*!
 * \brief Convert a binary buffer to a Base64 string.
 *
 * Free \p str after use with \ref roc_free_string.
 *
 * \par Example
 * \code{C}
 * uint8_t *buffer = ...;
 * size_t buffer_length = ...;
 * roc_string str;
 * roc_to_base64(buffer, &str);
 * \endcode
 *
 * \param[in] buffer Binary buffer to convert.
 * \param[in] buffer_length Length of \p buffer.
 * \param[out] str Base64 string encoding of \p buffer.
 * \remark This function is \ref reentrant.
 * \see \ref roc_from_base64
 */
ROC_EXPORT roc_error roc_to_base64(const uint8_t *buffer,
                                   size_t buffer_length,
                                   roc_string *str);

/*!
 * \brief Convert a Base64 string to a binary buffer.
 *
 * Free \p buffer after use with \ref roc_free_buffer.
 *
 * \par Example
 * \code{C}
 * const char *str = ...;
 * roc_buffer;
 * roc_from_base64(str, &buffer);
 * \endcode
 *
 * \param[in] str Base64 string to convert.
 * \param[out] buffer Binary decoding of \p str.
 * \param[out] buffer_length Length of \p buffer.
 * \remark This function is \ref reentrant.
 * \see \ref roc_to_base64
 */
ROC_EXPORT roc_error roc_from_base64(const char *str,
                                     roc_buffer *buffer,
                                     size_t *buffer_length);

/** @} */ // end of template_io

/*!
 * \defgroup gallery_construction Gallery Construction
 * \brief Open, read, edit and close galleries.
 *
 * Open a \ref roc_gallery_file with \ref roc_open_gallery and close it with
 * \ref roc_close_gallery.
 * Treat a gallery as a list of templates with \ref roc_enroll, \ref roc_size
 * and \ref roc_at, and \ref roc_remove.
 * Query the template limit with \ref roc_template_limit.
 *
 * @{
 */

/*!
 * \brief A list of flattened templates written to disk.
 *
 * \section gallery_io Gallery I/O
 * There are several ways to <b>write</b> to a gallery that all result in the
 * same file:
 *  - <tt>fopen</tt> + \ref roc_write_template
 *  - <tt>open</tt> + \ref roc_write_template_fd
 *  - <tt>fopen</tt> + \ref roc_flatten + <tt>fwrite</tt>
 *  - \ref roc_open_gallery + \ref roc_enroll
 *
 * There are several ways to <b>read</b> from a gallery that all result in the
 * same templates:
 *  - <tt>fopen</tt> + \ref roc_read_template
 *  - <tt>open</tt> + \ref roc_read_template_fd
 *  - <tt>fopen</tt> + <tt>fread</tt> + \ref roc_unflatten
 *  - \ref roc_open_gallery + \ref roc_at
 *
 * \section gallery_file_interoperability Interoperability
 * Gallery files produced from the \ref cli and \ref explore are interoperable
 * with those produced from the \ref api.
 * By convention this file format is identified using a <tt>.t</tt> suffix.
 *
 * \section gallery_file_concatenation Concatenation
 * An important aspect of gallery files is that two or more of them can be
 * concatenated together to produce one larger gallery file.
 * For example:
 * \verbatim $ cat a.t b.t > ab.t \endverbatim
 *
 * \section gallery_file_format Format
 * The exact format of a flattened template is defined by \ref roc_flatten which
 * is implemented as:
 * \snippet roc_embedded.c ROC Flatten
 */
typedef const char *roc_gallery_file;

/*!
 * \brief An array of templates.
 *
 * Templates are held in memory for efficient search.
 *
 * Initialize with \ref roc_open_gallery, search with
 * \ref roc_search, and free with \ref roc_close_gallery.
 *
 * All functions involving a gallery are designed to be \ref thread-safe, and
 * are protected with a <i>read-write lock</i> internally to ensure correctness.
 *
 * An immutable gallery is a \ref roc_const_gallery.
 */
typedef struct roc_gallery_type *roc_gallery;

/*!
 * \brief A constant \ref roc_gallery.
 */
typedef const struct roc_gallery_type *roc_const_gallery;

/*!
 * \brief Pointer to a callback function used to report progress on a long
 *        running task.
 *
 * The function should return \c void and take one \c float parameter which will
 * range from \c 0.0 to \c 100.0 indicating the percent completed.
 *
 * Any function taking a \ref roc_progress parameter will accept a \c NULL
 * value indicating not to report progress.
 *
 * \par Example
 * \code{C}
 * void print_progress(float percent_completed)
 * {
 *     printf("%.1f percent complete!\n", percent_completed);
 * }
 *
 * ...
 *
 * roc_gallery_file gallery_file = ...;
 * roc_gallery gallery;
 * roc_open_gallery(gallery_file, &gallery, &print_progress);
 *
 * // `roc_open_gallery()` will periodically call `print_progress()` with the
 * // percent completed.
 * \endcode
 */
typedef void (*roc_progress)(float);

/*!
 * \brief Open a gallery from a file.
 *
 * If \p gallery_file does not exist, an empty file will be created
 * automatically.
 * Release the gallery after use with \ref roc_close_gallery.
 * \p gallery_file must have both read and write permission.
 * See \ref roc_gallery_file for a description of the gallery file format.
 *
 * \note 32-bit SDK builds only support gallery files up to 2^31-1 bytes.
 *
 * Your license file includes a limit on the maximum number of templates in a
 * gallery, see \ref roc_template_limit for details.
 *
 * \section temporary_galleries Temporary (Memory-Only) Galleries
 * If \p gallery_file is \c NULL, a \em memory-only gallery will be initialized.
 * This is offered as a convenience when working with galleries that are
 * \em short-lived in nature.
 * Memory-only galleries are not nearly as efficient as disk-backed galleries,
 * and therefore should not be used when memory utilization is a concern.
 * Furthermore, temporary galleries cannot be defragmented by
 * \ref roc_defragment.
 *
 * \par Example
 * \code{C}
 * roc_gallery gallery;
 * roc_open_gallery("faces.t", &gallery, NULL);
 * \endcode
 *
 * \param[in] gallery_file File name to read/append templates from/to.
 * \param[out] gallery Address to store the opened gallery.
 * \param[in] progress Progress callback or \c NULL.
 * \remark This function is \ref reentrant.
 */
ROC_EXPORT roc_error roc_open_gallery(roc_gallery_file gallery_file,
                                      roc_gallery *gallery,
                                      roc_progress progress);

/*!
 * \brief Add a template to a gallery.
 *
 * This function is similar to \ref roc_write_template, appending the template
 * to the end of the corresponding \ref roc_gallery_file.
 * The enrolled template is now a potential candidate returned by
 * \ref roc_search.
 *
 * On platforms that support it, this function calls \c fsync on the gallery
 * file after writing \p template_.
 * Even if the application closes without calling \ref roc_close_gallery, the
 * biometric data will be preserved.
 *
 * Templates are stored in \p gallery akin to a \c std::vector.
 * The \ref roc_template_index of \p template_ in \p gallery will be equal the
 * \ref roc_size of \p gallery just prior to calling this function.
 *
 * To remove an enrolled template see \ref roc_remove.
 *
 * \par Example
 * \code{C}
 * roc_gallery gallery = ...;
 * roc_template template_ = ...;
 * roc_template_index template_index;
 * roc_size(gallery, &template_index);
 * roc_enroll(gallery, template_);
 * \endcode
 *
 * \param[in] gallery Gallery to add \p template_ to.
 * \param[in] template_ Template to add to \p gallery.
 * \remark This function is \ref thread-safe.
 */
ROC_EXPORT roc_error roc_enroll(roc_gallery gallery,
                                const roc_template template_);

/*!
 * \brief Retrieve the number of templates in a gallery.
 *
 * \par Example
 * \code{C}
 * roc_const_gallery gallery = ...;
 * size_t size;
 * roc_size(gallery, &size);
 * \endcode
 *
 * \param[in] gallery The gallery to retrieve the number of templates.
 * \param[out] size The number of templates in \p gallery.
 * \remark This function is \ref thread-safe.
 * \see \ref roc_at
 */
ROC_EXPORT roc_error roc_size(roc_const_gallery gallery,
                              size_t *size);

/*!
 * \brief The index of a template in a gallery.
 *
 * Retrieve the corresponding template with \ref roc_at.
 * The value \ref ROC_INVALID_TEMPLATE_INDEX indicates an invalid template
 * index.
 */
typedef size_t roc_template_index;

/*!
 * \brief An invalid \ref roc_template_index.
 *
 * The value of this constant is <tt>std::numeric_limits<size_t>::max()</tt>.
 */
extern const roc_template_index ROC_EXPORT ROC_INVALID_TEMPLATE_INDEX;

/*!
 * \brief Retrieve a template from a gallery at the specified index.
 *
 * This function returns a \em copy of the gallery template.
 * Free the copy after use with \ref roc_free_template.
 *
 * A gallery preserves the order of templates in its corresponding
 * \ref roc_gallery_file.
 *
 * This function is exposed in the \ref cli as \ref roc-at.
 *
 * \par Example
 * \code{C}
 * roc_const_gallery gallery = ...;
 * roc_template_index index = ...;
 * roc_template template_;
 * roc_at(gallery, index, &template_);
 * \endcode
 *
 * \param[in] gallery The gallery to retrieve a template from.
 * \param[in] index The index of the template, between 0 and
 *                  (\ref roc_size - 1).
 * \param[out] template_ A copy of the template in \p gallery at \p index.
 * \remark This function is \ref thread-safe.
 * \see \ref roc_size \ref roc_replace
 */
ROC_EXPORT roc_error roc_at(roc_const_gallery gallery,
                            roc_template_index index,
                            roc_template *template_);

/*!
 * \brief Replace a template in a gallery.
 *
 * This function has two limitations due to the way that galleries are
 * organized for efficient search.
 * Use cases that can't meet these limitations should consider using
 * \ref roc_remove and \ref roc_enroll instead.
 *
 * The first limitation is that the length of the feature vectors for both the
 * current template and the one replacing it must be the equal.
 * For normal use this will always by the case, unless attempting to replace a
 * template without a feature vector with one that has one (or vice versa), an
 * operation that is not supported by this function.
 *
 * The second limitation is that this function will only succeed if
 * \ref roc_flattened_bytes of \p template_ is less than or equal to that of the
 * existing template it is to replace, in which case it will be expanded using
 * \ref roc_reserve_metadata to be exactly the size of the current template.
 * If you anticipate needing to replace templates with larger ones, most
 * commonly because you intend to grow the template metadata, you should call
 * \ref roc_reserve_metadata on a template prior to \ref roc_enroll so there is
 * available space for later expansion.
 * This limitation only exists for disk-backed galleries, for \ref
 * temporary_galleries this is not a limitation.
 *
 * This function assumes that \p template_index is less than \p roc_size of
 * \p gallery.
 *
 * \par Example
 * \code{C}
 * roc_gallery gallery = ...;
 * roc_template template_ = ...;
 * roc_template_index template_index = ...;
 * roc_replace(gallery, template_index, template_);
 * \endcode
 *
 * \param[in] gallery Gallery to add \p template_ to.
 * \param[in] template_index Index of the gallery template to replace.
 * \param[in] template_ The new template to replace the gallery template with.
 * \remark This function is \ref thread-safe.
 * \see \ref roc_at
 */
ROC_EXPORT roc_error roc_replace(roc_gallery gallery,
                                 roc_template_index template_index,
                                 const roc_template template_);

/*!
 * \brief Remove the template at the specified index.
 *
 * For efficiency purposes, this function will replace the removed template with
 * an \ref empty_template.
 * Therefore \ref roc_size will remain the same, as will the
 * \ref roc_template_index of the remaining templates.
 * To truly delete the templates, reducing the gallery size and increasing the
 * template limit, use \ref roc_defragment.
 *
 * This function is exposed in the \ref cli as \ref roc-remove.
 *
 * \par Example
 * \code{C}
 * roc_gallery gallery = ...;
 * roc_remove(gallery, 3);
 * \endcode
 *
 * \param[in] gallery Gallery to remove template from.
 * \param[in] index Index of the template to remove.
 * \remark This function is \ref thread-safe.
 */
ROC_EXPORT roc_error roc_remove(roc_gallery gallery,
                                roc_template_index index);

/*!
 * \brief Retrieve the file path associated with a gallery.
 *
 * For a local gallery \p file_path will be the absolute path to the gallery
 * file.
 * For a temporary gallery \p file_path will be \c NULL.
 * For a remote gallery (see \ref roc-serve) \p file_path will include the IP
 * address, port and absolute file path of the gallery file on the server.
 * Free \p file_path after use with \ref roc_free_string.
 *
 * \par Example
 * \code{C}
 * roc_const_gallery gallery = ...;
 * roc_string file_name;
 * roc_get_gallery_file_path(gallery, &file_name);
 * \endcode
 *
 * \param[in] gallery Gallery to retrieve the file name of.
 * \param[out] file_path File path associated with \p gallery.
 * \remark This function is \ref reentrant.
 */
ROC_EXPORT roc_error roc_get_gallery_file_path(roc_const_gallery gallery,
                                               roc_string *file_path);

/*!
 * \brief Close an open gallery.
 *
 * This will free memory associated with \p gallery.
 *
 * \par Example
 * \code{C}
 * roc_gallery gallery = ...;
 * roc_close_gallery(gallery);
 * \endcode
 *
 * \param[in] gallery The gallery to close.
 * \remark This function is \ref reentrant.
 * \see \ref roc_open_gallery
 */
ROC_EXPORT roc_error roc_close_gallery(roc_gallery gallery);

/*!
 * \brief The number of templates available in the license.
 *
 * A ROC SDK license includes a limit on the number of templates that can be
 * used at once.
 * The number of templates in use is the sum of \ref roc_size for all galleries
 * in use.
 * Templates are put in and out of use by \ref roc_open_gallery and
 * \ref roc_close_gallery respectively.
 * This function returns the number of templates specified in the license file
 * \em less the number of templates in use.
 *
 * \par Example
 * \code{C}
 * size_t template_limit;
 * roc_template_limit(&template_limit);
 * \endcode
 *
 * \param[out] template_limit The remaining number of templates available.
 * \remark This function is \ref thread-safe.
 * \see \ref roc_comparison_limit \ref roc_representation_limit
 */
ROC_EXPORT roc_error roc_template_limit(size_t *template_limit);

/*!
 * \brief Delete empty templates from a gallery.
 *
 * An \ref empty_template in a gallery is most often the result of calling
 * \ref roc_remove.
 *
 * For each template in \p input, write it to \p output if and only if it has a
 * feature vector.
 * This operation preserves the order of non-empty templates.
 * If \p output exists then it will be overwritten.
 * If <tt>input == output</tt> the operation will occur in-place.
 * Close any gallery dependent on \p output before calling this function.
 *
 * \param[in] input Gallery to read templates from.
 * \param[in] output Gallery to write non-empty templates to.
 * \remark This function is \ref reentrant.
 */
ROC_EXPORT roc_error roc_defragment(roc_gallery_file input,
                                    roc_gallery_file output);

/** @} */ // end of gallery_construction

/*!
 * \defgroup comparison Comparison
 * \brief Compare face templates.
 *
 * Measure the \ref roc_similarity between individual face templates with
 * \ref roc_compare_templates or galleries with \ref roc_compare_galleries.
 *
 * @{
 */

typedef float roc_similarity;

#ifndef SWIG
/*!
 * \brief Maximum \ref roc_similarity.
 */
#define ROC_MAX_SIMILARITY (1.f)
#else // SWIG
%constant float ROC_MAX_SIMILARITY = 1.f;
#endif // !SWIG

#ifndef SWIG
/*!
 * \brief Minimum \ref roc_similarity.
 */
#define ROC_MIN_SIMILARITY (0.f)
#else // SWIG
%constant float ROC_MIN_SIMILARITY = 0.f;
#endif // !SWIG

#ifndef SWIG
/*!
 * \brief An invalid \ref roc_similarity.
 */
#define ROC_INVALID_SIMILARITY (-1.f)
#else // SWIG
%constant float ROC_INVALID_SIMILARITY = -1.f;
#endif // !SWIG

/*!
 * \brief A measurement between two faces quantifying the pseudo-probability
 *        that they are the same person.
 *
 * A larger value indicates a greater similarity.
 * Similarity is measured between the \ref feature_vector component of two
 * templates without consideration of any other potential sources of
 * information.
 * Similarity scores range from 0.0 to 1.0 where 0.0 represents a strong
 * non-match and 1.0 represents identical templates.
 *
 * We recommend a <b>default similarity threshold of 0.50</b> and adjusting as
 * low as 0.40 or as high as 0.70 depending on the tolerance for false matches
 * versus false non-matches.
 *
 * \section estimating_error_rates Estimating Error Rates
 * The following tables list \ref ROC_FR and \ref ROC_FR_FAST empirically
 * measured error rates for various common scenarios.
 * The true error rates for a given similarity threshold vary \em significantly
 * across datasets.
 * Customers are \em strongly encouraged to pick a similarity threshold based on
 * an accuracy evaluation of their production data.
 *
 * \subsection frontal_constrained_capture Frontal Constrained-Capture
 * Examples: driver's licenses, passports, mugshots and visas.
 *
 * \par ROC_FR
 * | False Match Rate | Similarity Threshold | False Non-Match Rate |
 * |------------------|----------------------|----------------------|
 * | 1e-2             | 0.360                | < 0.001              |
 * | 1e-3             | 0.486                | 0.003                |
 * | 1e-4             | 0.595                | 0.006                |
 * | 1e-5             | 0.686                | 0.011                |
 *
 * \par ROC_FR_FAST
 * | False Match Rate | Similarity Threshold | False Non-Match Rate |
 * |------------------|----------------------|----------------------|
 * | 1e-2             | 0.372                | 0.002                |
 * | 1e-3             | 0.488                | 0.007                |
 * | 1e-4             | 0.584                | 0.015                |
 * | 1e-5             | 0.664                | 0.023                |
 *
 * \subsection frontal_unconstrained_capture Frontal Unconstrained-Capture
 * Examples: selfies and ID document scans.
 *
 * \par ROC_FR
 * | False Match Rate | Similarity Threshold | False Non-Match Rate |
 * |------------------|----------------------|----------------------|
 * | 1e-2             | 0.322                | 0.005                |
 * | 1e-3             | 0.456                | 0.010                |
 * | 1e-4             | 0.566                | 0.019                |
 * | 1e-5             | 0.664                | 0.036                |
 *
 * \par ROC_FR_FAST
 * | False Match Rate | Similarity Threshold | False Non-Match Rate |
 * |------------------|----------------------|----------------------|
 * | 1e-2             | 0.349                | 0.015                |
 * | 1e-3             | 0.473                | 0.040                |
 * | 1e-4             | 0.570                | 0.080                |
 * | 1e-5             | 0.648                | 0.146                |
 *
 * \subsection unconstrained_capture Unconstrained-Capture
 * Examples: social media, photo journalism and webcams.
 *
 * \par ROC_FR
 * | False Match Rate | Similarity Threshold | False Non-Match Rate |
 * |------------------|----------------------|----------------------|
 * | 1e-2             | 0.315                | 0.007                |
 * | 1e-3             | 0.452                | 0.014                |
 * | 1e-4             | 0.563                | 0.030                |
 * | 1e-5             | 0.656                | 0.052                |
 *
 * \par ROC_FR_FAST
 * | False Match Rate | Similarity Threshold | False Non-Match Rate |
 * |------------------|----------------------|----------------------|
 * | 1e-2             | 0.344                | 0.022                |
 * | 1e-3             | 0.468                | 0.060                |
 * | 1e-4             | 0.564                | 0.125                |
 * | 1e-5             | 0.643                | 0.214                |
 *
 * \section functions_that_measure_similarity Functions That Measure Similarity
 * The following functions all measure similarity in the same way, but offer
 * interfaces tailored to different use cases.
 *
 * | Function                         | Description                      |
 * |----------------------------------|----------------------------------|
 * | \ref roc_compare_templates       | \copybrief roc_compare_templates |
 * | \ref roc_compare_galleries       | \copybrief roc_compare_galleries |
 * | \ref roc_search                  | \copybrief roc_search            |
 * | \ref roc_knn                     | \copybrief roc_knn               |
 */
typedef float roc_similarity;

#ifndef SWIG
/*!
 * \brief An invalid \ref roc_similarity.
 */
#define ROC_INVALID_SIMILARITY (-1.f)
#else // SWIG
%constant float ROC_INVALID_SIMILARITY = -1.f;
#endif // !SWIG

/*!
 * \brief Measure the similarity between two templates.
 *
 * Higher scores indicate greater similarity.
 * This function is a specialized version of \ref roc_compare_galleries.
 *
 * \note Comparisons involving many templates should make use of the other
 * \ref functions_that_measure_similarity as they will be much faster than
 * making repeated calls this this function.
 *
 * \section symmetry Symmetry
 * The returned \p similarity score is \em symmetric.
 * In other words, swapping the order of \p a and \p b will not change
 * \p similarity.
 *
 * \section empty_template Empty Template
 * A template is said to be \a empty if it has a \c NULL feature vector.
 * By convention, a comparison involving one or both empty templates results in
 * a \p similarity of \ref ROC_INVALID_SIMILARITY.
 *
 * \par Example
 * \code{C}
 * roc_template a = ...;
 * roc_template b = ...;
 * roc_similarity similarity;
 * roc_compare_templates(a, b, &similarity);
 * \endcode
 *
 * \param[in] a The first template to compare.
 * \param[in] b The second template to compare.
 * \param[out] similarity Similarity between \p a and \p b.
 * \remark This function is \ref thread-safe.
 */
ROC_EXPORT roc_error roc_compare_templates(const roc_template a,
                                           const roc_template b,
                                           roc_similarity *similarity);

/*!
 * \brief Measure the pairwise similarity between all templates in two
 *        galleries.
 *
 * This function is a generalized version of \ref roc_compare_templates and is
 * exposed in the \ref cli as \ref roc-compare.
 *
 * When \p a and \p b refer to the same gallery, \p similarities is called a
 * <i>self-similarity matrix</i>.
 *
 * \par Example
 * \code{C}
 * roc_const_gallery target = ...;
 * roc_const_gallery query = ...;
 * size_t target_size, query_size;
 * roc_size(target, &target_size);
 * roc_size(query, &query_size);
 * roc_similarity *similarity_matrix = (roc_similarity*) malloc(target_size *
 *                                                              query_size *
 *                                                      sizeof(roc_similarity));
 * roc_compare_galleries(a, b, similarities);
 * \endcode
 *
 * \param[in] target Gallery whose templates constitute the columns in
 *                   \p similaries.
 * \param[in] query Gallery whose templates constitute the rows in
 *                  \p similarities.
 * \param[out] similarity_matrix Buffer large enough to hold
 *                               <tt>roc_size(target) * roc_size(query)</tt>
 *                               similarity scores, populated in row-major
 *                               order.
 * \remark This function is \ref thread-safe and \ref parallel.
 */
ROC_EXPORT roc_error roc_compare_galleries(roc_const_gallery target,
                                           roc_const_gallery query,
                                           roc_similarity *similarity_matrix);

/*!
 * \brief Score-level fusion by computing a max.
 *
 * Use this function when you've made multiple comparisons involving the same
 * identity and wish to derive a single consolidated similarity score.
 *
 * \param[in] raw Similarity scores with which to perform score-level fusion.
 * \param[in] n Length of \p raw.
 * \param[out] fused Combined similarity score.
 * \remark This function is \ref thread-safe.
 */
ROC_EXPORT roc_error roc_fuse(roc_similarity *raw,
                              size_t n,
                              roc_similarity *fused);

/*!
 * \brief Query the remaining comparison limit.
 *
 * Some licenses of the ROC SDK include a limit to the number of comparisons
 * that can be made over the lifetime of the license.
 * The number of comparisons is the number of times \ref roc_compare_templates
 * or \ref roc_compare_galleries or \ref roc_search or \ref roc_knn has been
 * called.
 * This function returns the number of comparisons specified in the license
 * file \em less the number of comparisons used to date.
 *
 * \par Example
 * \code{C}
 * size_t comparison_limit;
 * roc_comparison_limit(&comparison_limit);
 * \endcode
 *
 * \param[out] comparison_limit The remaining number of comparisons available.
 * \remark This function is \ref thread-safe.
 * \see \ref roc_representation_limit \ref roc_template_limit
 */
ROC_EXPORT roc_error roc_comparison_limit(size_t *comparison_limit);

/** @} */ // end of comparison

/*!
 * \defgroup search Search
 * \brief Search a gallery.
 *
 * Retrieve a sorted list of the top candidate templates for a single probe with
 * \ref roc_search or multiple independent probes with \ref roc_knn.
 * Re-order search results based on commonly occuring \ref roc_person_id with
 * \ref roc_rerank.
 *
 * @{
 */

/*!
 * \brief Reference to a gallery template associated with a similarity to a
 *        probe.
 *
 * Retrieve the gallery template using \ref roc_at with
 * \ref roc_candidate::index.
 * Note that \ref roc_candidate::similarity is equal to the similarity returned
 * by \ref roc_compare_templates.
 */
typedef struct roc_candidate
{
    roc_template_index index; /*!< \brief Index of the candidate template in the
                                          gallery. */
    roc_similarity similarity; /*!< \brief Similarity of the candidate template
                                           to the probe. */
} roc_candidate;

/*!
 * \brief Ranked search for a probe template against a gallery of templates.
 *
 * The pre-allocated buffer \p candidates should be large enough to contain \p k
 * elements.
 * The \p candidates will be ordered by decreasing
 * \ref roc_candidate::similarity.
 * In the event that there are fewer than \p k candidates to return, the
 * trailing elements of \p candidates will be populated with
 * \ref roc_candidate::index of \ref ROC_INVALID_TEMPLATE_INDEX and
 * \ref roc_candidate::similarity of \ref ROC_INVALID_SIMILARITY.
 *
 * Note that this function is a special case of \ref roc_knn with a single
 * probe template and is exposed in the \ref cli as \ref roc-search.
 *
 * If you have multiple probes, it is more efficient to call \ref roc_knn once
 * than make repeated calls to \ref roc_search.
 * This is particularly true when \p gallery is large as the probes will be
 * batched together to minimize reads to main memory.
 *
 * \section number_of_candidates Number of Candidates
 * The \p k most similar templates in \p gallery to \p probe, ordered by
 * decreasing similarity.
 * The suggested default value for \p k is 20.
 *
 * \section min_search_similarity Minimum Similarity
 * The minimum similarity score required to add a candidate to the candidate
 * list.
 * The suggested default value for \p min_similarity is 0.
 *
 * \par Example
 * \code{C}
 * roc_const_gallery gallery = ...;
 * roc_template probe = ...;
 * roc_candidate candidates[20];
 * roc_search(gallery, probe, 20, 0, (roc_candidate*) &candidates);
 * \endcode
 *
 * \param[in] gallery Gallery to search against.
 * \param[in] probe Template to search for.
 * \param[in] k The desired number of candidates, see \ref number_of_candidates.
 * \param[in] min_similarity The desired minimum similarity of candidates, see
 *                           \ref min_search_similarity.
 * \param[out] candidates Pre-allocated buffer to contain the top matching
 *                        templates in \p gallery.
 * \remark This function is \ref thread-safe and \ref parallel.
 * \see \ref roc_compare_galleries \ref roc_rerank
 */
ROC_EXPORT roc_error roc_search(roc_const_gallery gallery,
                                const roc_template probe,
                                size_t k,
                                roc_similarity min_similarity,
                                roc_candidate *candidates);

/*!
 * \brief Construct the k-nearest neighbors graph of a gallery.
 *
 * The pre-allocated buffer \p neighbors should be large enough to hold
 * <tt>n = k*\ref roc_size (probes)</tt> elements.
 * For a \ref roc_template_index <tt>p</tt> in \p probes, its <tt>i</tt>-th
 * nearest neighbor in \p gallery is <tt>g = neighbors[p*k+i].index</tt>.
 *
 * In the common case where <tt>gallery == probe</tt>, \p neighbors can be used
 * as the input to \ref roc_cluster.
 *
 * \section search_generalization Search Generalization
 * This function is a generalized version of \ref roc_search with multiple
 * independent probe templates, as illustrated in the following code snippet.
 *
 * \code{C}
 * roc_const_gallery gallery = ...;
 * roc_const_gallery probes = ...;
 * size_t k = ...;
 * roc_similarity min_similarity = ...;
 * size_t gallery_size, probes_size;
 * roc_size(gallery, &gallery_size);
 * roc_size(probes, &probes_size);
 * roc_candidate *neighbors = (roc_candidate*) malloc(k * probes_size *
 *                                                    sizeof(roc_candidate));
 *
 * // This is identical ...
 * for (size_t i=0; i<probe_sizes; i++) {
 *     roc_template t;
 *     roc_at(probes, i, &t);
 *     roc_search(gallery, t, k, min_similarity, neighbors + i * k);
 *     roc_free_template(&t);
 * }
 *
 * // ... to this
 * roc_knn(gallery, probes, k, min_similarity, neighbors);
 * \endcode
 *
 * \section nearest_neighbors Nearest Neighbors
 * The \p k nearest neighbors to a template are the \p k most similar templates
 * in \p gallery to it, ordered by decreasing similarity.
 * The suggested default value for \p k is \c 20.
 *
  * \section min_knn_similarity Minimum Similarity
 * The minimum similarity score required to add a candidate to the nearest
 * neighbors graph.
 * The suggested default value for \p min_similarity is 0.
 *
 * \par Example
 * \code{C}
 * roc_const_gallery gallery = ...;
 * size_t k = ...;
 * roc_similarity min_similarity = ...;
 * size_t size;
 * roc_size(gallery, &size);
 * roc_candidate *neighbors = (roc_candidate*) malloc(k * size *
 *                                                    sizeof(roc_candidate));
 * roc_knn(gallery, gallery, k, min_similarity, neighbors);
 * \endcode
 *
 * \param[in] gallery The templates from which to find nearest neighbors.
 * \param[in] probes The templates for which to find nearest neighbors.
 * \param[in] k The number of nearest neighbors in \p gallery to retain, see
 *              \ref nearest_neighbors.
 * \param[in] min_similarity The desired minimum similarity of candidates, see
 *                           \ref min_knn_similarity.
 * \param[out] neighbors Pre-allocated buffer to store the nearest neighbors of
 *                       each template in \p probes.
 * \remark This function is \ref thread-safe and \ref parallel.
 * \see \ref roc_search \ref roc_rerank \ref roc_adjudicate_knn
 */
ROC_EXPORT roc_error roc_knn(roc_const_gallery gallery,
                             roc_const_gallery probes,
                             size_t k,
                             roc_similarity min_similarity,
                             roc_candidate *neighbors);

/*!
 * \brief Re-rank \ref roc_search or \ref roc_knn results based on commonly
 *        occuring \ref roc_person_id and \ref roc_template_index.
 *
 * \ref roc_search and \ref roc_knn order each \ref roc_candidate independently
 * based soley on its \ref roc_similarity score to the probe \ref roc_template.
 * This function re-orders \p candidates by considering commonly occuring
 * \ref roc_person_id and \ref roc_template_index in \p candidates.
 * After calling this function, the ordering is now based on the \em max of the
 * similarities for all \p candidates with the same \ref roc_person_id (or
 * \ref roc_template_index when \ref roc_person_id is \ref ROC_UNKNOWN_PERSON).
 * In the resulting \p candidates, two \ref roc_candidate with the same
 * \ref roc_person_id (or \ref roc_template_index) will occur at
 * adjacent indicies, ordered by \ref roc_candidate::similarity.
 * Note that the use of \ref roc_template_index only comes into play when
 * \p candidates is the ouput from \ref roc_knn with multiple probes (from the
 * same person) and \em the \p candidates have \ref ROC_UNKNOWN_PERSON for
 * \ref roc_person_id.
 *
 * \par Example
 * \code{C}
 * roc_const_gallery gallery = ...;
 * roc_template probe = ...;
 * roc_candidate candidates[20];
 * roc_search(gallery, probe, 20, 0, (roc_candidate*) &candidates);
 * roc_rerank(gallery, 20, (roc_candidate*) &candidates);
 * \endcode
 *
 * \param[in] gallery The gallery used in the prior call to \ref roc_search or
 *                    \ref roc_knn.
 * \param[in] k The length of \p candidates used in the prior call to
 *              \ref roc_search or \ref roc_knn.
 * \param[in,out] candidates Candidates list to re-order based on commonly
 *                           occuring \ref roc_template::person_id.
 * \remark This function is \ref thread-safe.
 * \see \ref roc_search \ref roc_knn \ref roc_fuse
 */
ROC_EXPORT roc_error roc_rerank(roc_const_gallery gallery,
                                size_t k,
                                roc_candidate *candidates);

/** @} */ // end of search

/*!
 * \defgroup tracking Tracking / Watchlisting
 * \brief Follow faces in videos.
 *
 * For \em offline tracking, where you have already built a gallery from a
 * single pre-recorded video, use \ref roc_track.
 *
 * For \em online tracking and watchlisting, where you are handling one of more
 * live streaming video feeds, use \ref roc_tracker and see
 * \ref roc_tracker_add_template for details.
 *
 * @{
 */

/*!
 * \brief Track faces across frames in a video.
 *
 * This is an alternative clustering algorithm that groups templates based on
 * temporal, spatial and facial similarity.
 * Two templates are assigned the same \ref roc_template::person_id if all of
 * the following are true:
 *  - Their roc_template::timestamp absolute difference is less than or equal
 *    to \p max_time_separation.
 *    The suggested default value for \p max_time_separation is \c 5000,
 *    corresponding to a five second separation.
 *  - Their detection intersection area divided by union area, or \a overlap,
 *    is greater than or equal to \p min_detection_overlap.
 *    The suggested default value for \p min_detection_overlap is \c 0.2.
 *  - Their facial similarity is greater than or equal to \p min_similarity.
 *    The suggested default value for \p min_similarity is \c 0.6.
 *
 * All templates in the gallery are assumed to belong to the same video, ordered
 * by increasing \ref roc_template::timestamp.
 * If templates are not ordered sequentially, not all possible comparisons
 * within \p max_time_separation will be considered.
 * This is typically not an issue if \p max_time_separation is significantly
 * larger than the error in the temporal ordering of templates.
 *
 * The tracking algorithm is not \em transitive.
 * If templates \c a, \c b and \c c are part of the same track, it may be the
 * case that \c a/b match and \c b/c match, but \c a/c does not match.
 *
 * Tracking results are stored persistently in the \ref roc_template::person_id
 * field for each template in \p gallery, and can be retrieved with
 * \ref roc_person_ids.
 * Templates that could not be grouped are each assigned a unique
 * \ref roc_person_id.
 *
 * This function may be called more than once to perform incremental tracking.
 * The existing tracks will be preserved provided the thresholds remain the
 * same.
 *
 * This function is exposed in the \ref cli as \ref roc-track.
 *
 * \section max_time_separation Maximum Time Separation
 * The maximum absolute time difference when greater than zero.
 * A \p max_time_separation of zero will be treated as
 * <tt>std::numeric_limits<roc_time>::max()</tt>, effectively disabling the
 * temporal tracking requirement.
 *
 * \section min_detection_overlap Minimum Detection Overlap
 * The minimum face detection bounding box overlap, between zero and one
 * inclusive.
 *
 * \section min_track_similarity Minimum Similarity
 * Minimum template similarity, between zero and one inclusive.
 *
 * \par Example
 * \code{C}
 * roc_gallery gallery = ...;
 * roc_track(gallery, 5000, 0.2, 0.6);
 * \endcode
 *
 * \param[in,out] gallery The gallery to track.
 * \param[in] max_time_separation Maximum absolute time difference, see
 *                                \ref max_time_separation.
 * \param[in] min_detection_overlap Minimum face detection bounding box overlap,
 *                                  see \ref min_detection_overlap.
 * \param[in] min_similarity Minimum template similarity, see
 *                           \ref min_track_similarity.
 * \remark This function is \ref thread-safe.
 * \see \ref roc_cluster
 */
ROC_EXPORT roc_error roc_track(roc_gallery gallery,
                               roc_time max_time_separation,
                               float min_detection_overlap,
                               roc_similarity min_similarity);

/*!
 * \brief Reasons for a \ref roc_event.
 */
typedef enum roc_event_reasons
{
    /*!
     * \brief There are no more events in the queue.
     */
    ROC_NO_EVENT = 0x0000,

    /*!
     * \brief Found a previously unseen person.
     *
     * The \ref roc_template::person_id for \ref roc_event::probe is be one more
     * than the previous maximum \ref roc_person_id.
     */
    ROC_NEW_TRACK = 0x0001,

    /*!
     * \brief The probe template associated with the event has changed.
     *
     * Implies \ref ROC_BETTER_PROBE or \ref ROC_BETTER_CANDIDATE.
     */
    ROC_NEW_PROBE = 0x0002,

    /*!
     * \brief The new \ref roc_event::quality is higher.
     *
     * Implies \ref ROC_NEW_PROBE.
     */
    ROC_BETTER_PROBE = 0x0004,

    /*! \brief The new \ref roc_event::candidate has higher similarity.
     *
     * Implies \ref ROC_NEW_PROBE.
     */
    ROC_BETTER_CANDIDATE = 0x0008,

    /*!
     * \brief The new event incorporates another now obsolete event with
     *        roc_event::previous_person_id.
     */
    ROC_MERGED_TRACKS = 0x0010,

    /*!
     * \brief The event has an earlier start time.
     *
     * Implies \ref ROC_MERGED_TRACKS.
     */
    ROC_NEW_START_TIME = 0x0020,

    /*!
     * \brief The event has a later end time.
     */
    ROC_NEW_STOP_TIME = 0x0040,

    /*!
     * \brief The most recent camera has changed.
     *
     * Implies \ref ROC_NEW_STOP_TIME.
     */
    ROC_NEW_CAMERA = 0x0080,

    /*!
     * \brief The candidate watchlist has changed.
     *
     * Implies \ref ROC_BETTER_CANDIDATE.
     */
    ROC_NEW_WATCHLIST = 0x0100,

    /*!
     * \brief Additional templates associated with an existing event.
     */
    ROC_MORE_TEMPLATES = 0x0200,

    /*!
     * \brief This is the last time an event will be reported for this person.
     */
    ROC_FINALIZED_TRACK = 0x0400
} roc_event_reasons;

/*!
 * \brief A combination of \ref roc_event_reasons.
 *
 * A \ref roc_event_type is a bit field of \ref roc_event_reasons.
 * To combine multiple \ref roc_event_reasons use the \c C \em bitwise-or \c |
 * operator.
 * To test for specific \ref roc_event_reasons use the \c C \em bitwise-and \c &
 * operator.
 */
typedef uint32_t roc_event_type;

/*!
 * \brief A summary of a person in one or more videos.
 *
 * \note For roc_event::start and roc_event::stop, time is measured from the
 *       call to \ref roc_new_tracker.
 */
typedef struct roc_event
{
    roc_event_type reasons; /*!< \brief Why this event was emitted. */
    roc_template probe; /*!< \brief Representative template for the event. */
    float quality; /*!< \brief \ref face_quality of \ref probe. */
    roc_person_id previous; /*!< \brief When \ref ROC_MERGED_TRACKS, this value
                                        holds the other events's
                                        \ref probe \ref roc_template::person_id.

                                 By convention \ref previous is greater than
                                 \ref probe \ref roc_template::person_id. */
    roc_candidate candidate; /*!< \brief Highest \ref roc_similarity match
                                         against one of the watchlists. */
    roc_gallery watchlist; /*!< \brief Watchlist corresponding to
                                       \ref candidate. */
    roc_time start; /*!< \brief Timestamp of the earliest template associated
                                with the event. */
    roc_time stop; /*!< \brief Timestamp of the latest template associated with
                               the event. */
    roc_camera_id camera; /*!< \brief Camera of the latest template associated
                                      with the event. */
    uint32_t count; /*!< \brief Number of templates associated with the
                                event. */
} roc_event;

/*!
 * \brief A tracker takes templates from one or more videos and outputs events.
 *
 * Initialize with \ref roc_new_tracker and free with \ref roc_free_tracker.
 * Add templates with \ref roc_tracker_add_template and retrieve events with
 * \ref roc_tracker_take_event.
 */
typedef struct roc_tracker_type *roc_tracker;

/*!
 * \brief Initialize a new tracker.
 *
 * After initializing a tracker, users should first call
 * \ref roc_tracker_set_tracking_parameters.
 * Optionally, if you wish to enable watchlisting, also call
 * \ref roc_tracker_set_watchlisting_parameters.
 * Optionally, if you wish to use \ref roc_tracker_add_image, also call
 * \ref roc_tracker_set_represent_parameters.
 *
 * Free \p tracker after last use with \ref roc_free_tracker.
 *
 * \par Example
 * \code{C}
 * roc_tracker tracker;
 * roc_new_tracker(&tracker);
 * \endcode
 *
 * \param[out] tracker Address of an unitialized tracker to initialize.
 * \remark This function is \ref reentrant.
 */
ROC_EXPORT roc_error roc_new_tracker(roc_tracker *tracker);

/*!
 * \brief Set tracker tracking parameters.
 *
 * \par Example
 * \code{C}
 * roc_tracker tracker = ...;
 * roc_tracker_set_tracking_parameters(tracker, ROC_BETTER_PROBE | ROC_BETTER_CANDIDATE | ROC_MERGED_TRACKS, 5000, 0.2, 0.6);
 * \endcode
 *
 * \param[in] tracker Tracker to set parameters.
 * \param[in] reasons See \ref reasons.
 * \param[in] max_time_separation See \ref max_time_separation.
 * \param[in] min_detection_overlap See \ref min_detection_overlap.
 * \param[in] min_tracking_similarity See \ref min_track_similarity.
 * \remark This function is \ref thread-safe.
 */
ROC_EXPORT roc_error roc_tracker_set_tracking_parameters(roc_tracker tracker,
                                                         roc_event_type reasons,
                                                         roc_time
                                                            max_time_separation,
                                                         float
                                                          min_detection_overlap,
                                                         roc_similarity
                                                       min_tracking_similarity);

/*!
 * \brief Set tracker watchlisting parameters.
 *
 * \note \p watchlists is assumed to remain valid until it is changed with
 *       \ref roc_tracker_set_watchlisting_parameters or tracking is stopped
 *       with \ref roc_free_tracker.
 *
 * \par Example
 * \code{C}
 * roc_tracker tracker = ...;
 * roc_gallery watchlist = ...;
 * roc_tracker_set_watchlisting_parameters(tracker, &watchlist, 1, 0.8);
 * \endcode
 *
 * \param[in] tracker Tracker to set parameters.
 * \param[in] watchlists Array of galleries to search \p template_ against, or
 *                       \c NULL.
 * \param[in] num_watchlists Length of \p watchlists, or \c 0.
 * \param[in] min_watchlist_similarity See \ref min_search_similarity, or \ref
 *                                     ROC_INVALID_SIMILARITY.
 * \remark This function is \ref thread-safe.
 */
ROC_EXPORT roc_error roc_tracker_set_watchlisting_parameters(roc_tracker
                                                                        tracker,
                                                             roc_gallery
                                                                    *watchlists,
                                                             int num_watchlists,
                                                             roc_similarity
                                                      min_watchlist_similarity);

/*!
 * \brief Set tracker template representation parameters.
 *
 * \par Example
 * \code{C}
 * roc_tracker tracker = ...;
 * roc_gallery watchlist = ...;
 * roc_tracker_set_watchlisting_parameters(tracker, &watchlist, 1, 0.8);
 * \endcode
 *
 * \param[in] tracker Tracker to set parameters.
 * \param[in] algorithm_id See \ref roc_represent.
 * \param[in] min_size See \ref roc_represent.
 * \param[in] k See \ref roc_represent.
 * \param[in] false_detection_rate See \ref roc_represent.
 * \remark This function is \ref thread-safe.
 */
ROC_EXPORT roc_error roc_tracker_set_represent_parameters(roc_tracker tracker,
                                                          roc_algorithm_id
                                                                   algorithm_id,
                                                          size_t min_size,
                                                          int k,
                                                          float
                                                          false_detection_rate);

/*!
 * \brief Add a template to a tracker.
 *
 * When a template is added to the tracker it may trigger zero or more
 * \ref roc_event.
 * Events are added to a queue and can be retrieved in-order by calling
 * \ref roc_tracker_take_event.
 *
 * Events are keyed off of the \ref roc_template::person_id of
 * \ref roc_event::probe, which is the unique identifier for a person in \p
 * tracker.
 *
 * The tracker makes a copy of \p template_, so the caller still owns this
 * value.
 *
 * \section reasons Event Reasons
 * There are different reasons an event might be tiggered.
 * See \ref roc_event_reasons for a list of all possible reasons.
 * The reasons you wish to be notified about should be combined into a
 * \ref roc_event_type and passed to this function as \p reasons.
 * An event with one or more \ref roc_event::reasons shared in common with
 * \p reasons will be added to the event queue.
 *
 * \section multiple_cameras Multiple Cameras
 * If you are tracking with multiple cameras, indicate which camera \p template_
 * came from with \p camera, otherwise use \ref ROC_UNKNOWN_CAMERA.
 * To be notified when a person enters a new camera, include \ref ROC_NEW_CAMERA
 * as part of reasons.
 *
 * \section tracking Tracking
 * See \ref roc_track for a description of the tracking algorithm
 * implementation.
 * The \ref roc_template::timestamp stored in \p template_ is ignored in favor
 * of the current system time so that multiple cameras need not have synced
 * clocks, and templates can be recieved out-of-order when processed in
 * parallel.
 * Many of \ref roc_event_reasons relate to tracking, but the most noteworthy
 * are \ref ROC_NEW_TRACK, \ref ROC_MERGED_TRACKS and \ref ROC_FINALIZED_TRACK.
 *
 * \section watchlisting Watchlisting
 * If you have one or more galleries of known identities to search \p template_
 * against, specify them with \p watchlists and \p num_watchlists.
 * The highest similarity watchlist candidate exceeding
 * \p min_watchlist_similarity will be reported.
 * To be notified of a watchlist hit, include \ref ROC_BETTER_CANDIDATE as part
 * of \p reasons.
 *
 * Retrieve a watchlist hit after this function call using
 * \ref roc_tracker_take_event.
 * The \ref roc_event::probe will be a copy of the template passed to this
 * function and \ref roc_event::candidate and \ref roc_event::watchlist will be
 * set.
 *
 * If you don't have a watchlist, pass \c NULL for \p watchlists, \c 0 for \p
 * num_watchlists, and \ref ROC_INVALID_SIMILARITY for
 * \p min_watchlist_similarity.
 *
 * \par Example
 * \code{C}
 * roc_tracker tracker = ...;
 * roc_template template_ = ...;
 * roc_tracker_add_template(tracker, template, ROC_UNKNOWN_CAMERA);
 * \endcode
 *
 * \param[in] tracker Tracker to add a template to.
 * \param[in] template_ Template to add to the tracker.
 * \param[in] camera See \ref multiple_cameras.
 * \param[in] timestamp Timestamp of the image used to generate the template or
 *                      \ref ROC_NO_TIMESTAMP to use the current system time.
 * \remark This function is \ref thread-safe.
 */
ROC_EXPORT roc_error roc_tracker_add_template(roc_tracker tracker,
                                              const roc_template template_,
                                              roc_camera_id camera,
                                              roc_time timestamp);

/*!
 * \brief Convenience function for generating and tracking all templates in an
 *        image.
 *
 * This function calls \ref roc_represent on \p image with the parameters set
 * by \ref roc_tracker_set_represent_parameters.
 * For each resulting template it calls \ref roc_tracker_add_template.
 *
 * \par Example
 * \code{C}
 * roc_tracker tracker = ...;
 * roc_image image = ...;
 * roc_tracker_add_image(tracker, image, ROC_UNKNOWN_CAMERA);
 * \endcode
 *
 * \param[in] tracker Tracker to add templates to.
 * \param[in] image Image to construct templates from.
 * \param[in] camera See \ref multiple_cameras.
 * \remark This function is \ref thread-safe.
 */
ROC_EXPORT roc_error roc_tracker_add_image(roc_tracker tracker,
                                           const roc_image image,
                                           roc_camera_id camera);

/*!
 * \brief Cast and return a \ref roc_tracker as a <tt>void*</tt>.
 *
 * Helper function for some of the API language wrappers that don't support this
 * type cast.
 *
 * \param[in] tracker Input tracker.
 * \remark This function is \ref thread-safe.
 */
ROC_EXPORT void *roc_tracker_cast(roc_tracker tracker);

#ifdef SWIG
%constant void roc_tracker_add_image_callback(void*, roc_error, roc_image, roc_time, roc_camera_id);
#else // !SWIG
/*!
 * \brief Alternative to \ref roc_tracker_add_image with signature type
 *        \ref roc_stream_callback.
 *
 * If \p error is \c NULL and \p image is valid this function calls
 * \ref roc_tracker_add_image, otherwise it does nothing.
 *
 * \par Example
 * \code{C}
 * roc_tracker tracker = ...;
 * roc_image image = ...;
 * roc_tracker_add_image(tracker, ROC_SUCCESS, image, ROC_UNKNOWN_CAMERA);
 * \endcode
 *
 * \param[in] tracker Tracker to add templates to.
 * \param[in] error Reported error value.
 * \param[in] image Image to construct templates from.
 * \param[in] camera See \ref multiple_cameras.
 * \remark This function is \ref thread-safe.
 */
ROC_EXPORT void roc_tracker_add_image_callback(void *tracker,
                                               roc_error error,
                                               roc_image image,
                                               roc_camera_id camera);
#endif // SWIG

/*!
 * \brief Query if the tracker has events.
 *
 * If the tracker has events they can be obtained one at a time with
 * \ref roc_tracker_take_event.
 *
 * This function blocks for up to \p wait milliseconds for an event if none are
 * available.
 * If \p wait is \c 0 this function does not block.
 *
 * \par Example
 * \code{C}
 * roc_tracker tracker = ...;
 * if (roc_tracker_has_events(tracker, 0))
 *     ...;
 * \endcode
 *
 * \param[in] tracker Tracker to query for events.
 * \param[in] wait Maximum milliseconds to wait for an event if one is not
 *                 immediately available.
 * \return \c true if \p tracker has events, \c false otherwise.
 * \remark This function is \ref thread-safe.
 * \see \ref roc_tracker_take_event
 */
ROC_EXPORT bool roc_tracker_has_events(roc_tracker tracker, roc_time wait);

/*!
 * \brief Obtain the next event from a tracker.
 *
 * If \p event \ref roc_event::reasons equals \ref ROC_NO_EVENT then there are
 * no more events, and the rest of the fields in \p event are unitialized.
 *
 * Free \p event \ref roc_event::probe after use with \ref roc_free_template.
 *
 * \par Example
 * \code{C}
 * roc_tracker tracker = ...;
 * roc_event;
 * while (true) {
 *     roc_tracker_take_event(tracker, &event);
 *     if (event.reasons == ROC_NO_EVENT)
 *         break;
 *     // ...
 *     roc_free_template(&event.probe);
 * }
 * \endcode
 *
 * \param[in] tracker Tracker to take event from.
 * \param[out] event Next event in \p tracker.
 * \remark This function is \ref thread-safe on \p tracker and \ref reentrant on
 *         \p event.
 * \see \ref roc_tracker_has_events
 */
ROC_EXPORT roc_error roc_tracker_take_event(roc_tracker tracker,
                                            roc_event *event);

/*!
 * \brief Free a tracker.
 *
 * Deallocate memory for \p tracker previously allocated with
 * \ref roc_new_tracker.
 *
 * \par Example
 * \code{C}
 * roc_tracker tracker = ...;
 * roc_free_tracker(tracker);
 * \endcode
 *
 * \param[in] tracker Tracker to free.
 * \return This function is \ref reentrant.
 */
ROC_EXPORT roc_error roc_free_tracker(roc_tracker tracker);

/** @} */ // end of tracking

/*!
 * \defgroup identity_discovery Identity Discovery
 * \brief Group a gallery into identities.
 *
 * Identities are managed using the \ref roc_template::person_id field.
 * Cluster faces into identities with \ref roc_cluster.
 * Downsample identities with \ref roc_consolidate.
 * Retrieve identities from a gallery with \ref roc_person_ids.
 * Apply ground truth with \ref roc_adjudicate_knn and
 * \ref roc_adjudicate_gallery.
 *
 * @{
 */

/*!
 * \brief Construct clusters from a k-nearest neighbors graph.
 *
 * Clustering groups templates by similarity.
 * This function modifies \p gallery to store clustering results persistently in
 * the \ref roc_template::person_id field for each template in \p gallery, which
 * can be retrieved by calling \ref roc_person_ids.
 * \ref roc_person_id values are assigned to clusters in order based on cluster
 * quality and size, with the largest and most internally similar cluster
 * assigned a \ref roc_template::person_id of zero.
 * The order of the templates in \p galery remains the same.
 *
 * The input to this function is the output from \ref roc_knn.
 *
 * This function is exposed in the \ref cli as \ref roc-cluster.
 *
 * \section aggressiveness Aggressiveness
 * The clustering \p aggressiveness is a value in the range <tt>[-1.0, 1.0]</tt>
 * that influences the number of clusters formed.
 * A value toward \c -1.0 favors more clusters (fewer templates per cluster).
 * Conversely, a value toward \c 1.0 favors fewer clusters (more templates per
 * cluster).
 * The suggested default value for \p aggressiveness is \c 0.0.
 *
 * \section adjudication Adjudication
 * To apply ground truth knowledge to clustering, call \ref roc_adjudicate_knn
 * on \p neighbors before this function and \ref roc_adjudicate_gallery on
 * \p gallery after this function.
 *
 * \par Example
 * \code{C}
 * roc_gallery gallery = ...;
 * size_t k = ...;
 * roc_candidate *neighbors = ...;
 * roc_cluster(gallery, k, neighbors, 0.0);
 * \endcode
 *
 * \param[in,out] gallery The gallery used to construct \p neighbors.
 * \param[in] k Number of nearest neighbors kept for each template in
 *              \p neighbors.
 * \param[in] neighbors Nearest neighbor graph computed from \ref roc_knn with
 *                      \p gallery.
 * \param[in] aggressiveness Clustering aggressiveness, see \ref aggressiveness.
 * \remark This function is \ref thread-safe.
 * \see \ref roc_extend \ref roc_track
 */
ROC_EXPORT roc_error roc_cluster(roc_gallery gallery,
                                 size_t k,
                                 const roc_candidate *neighbors,
                                 float aggressiveness);

/*!
 * \brief An adjudication is used to specify the identity match/non-match
 * relationship between two templates with absolute certainty.
 *
 * Template indicies \ref roc_adjudication::a and \ref roc_adjudication::b are
 * presumed to be unequal valid indicies into same gallery.
 *
 * An array of \ref roc_adjudication need not be exhaustive, but should be free
 * of contradictions: if <code>A=B</code> and <code>B=C</code> the software will
 * infer <code>A=C</code>, and it should not be the case that <code>A!=C</code>
 * is also specified.
 */
typedef struct roc_adjudication
{
    roc_template_index a; /*!< \brief First template index. */
    roc_template_index b; /*!< \brief Second template index. */
    bool same_person; /*!< \brief \c true if the templates are the same person,
                                  \c false if they are different people. */
} roc_adjudication;

/*!
 * \brief Update a KNN graph with ground truth identity information.
 *
 * Entries in \p neighbors that are known to be the same person based on
 * \p adjudications will be assigned a \ref roc_candidate::similarity of
 * <code>1</code>.
 * Entries that are known to be different people will be assigned a
 * \ref roc_candidate::similarity of <code>0</code> and
 * \ref roc_candidate::index of \ref ROC_INVALID_TEMPLATE_INDEX.
 * The candidate lists will then be re-sorted.
 *
 * \param gallery_size Size of gallery used to construct \p neighbors.
 * \param k Number of candidates for each template.
 * \param neighbors KNN graph to update.
 * \param adjudications Array of ground truth identity information.
 * \param num_adjudications Length of \p adjudications.
 * \remark This function is \ref reentrant.
 * \see \ref roc_knn \ref roc_adjudicate_gallery
 */
ROC_EXPORT roc_error roc_adjudicate_knn(size_t gallery_size,
                                        size_t k,
                                        roc_candidate *neighbors,
                                        const roc_adjudication *adjudications,
                                        size_t num_adjudications);

/*!
 * \brief Update a gallery with ground truth identity information.
 *
 * Update \ref roc_person_id entries in \p gallery to respect \p adjudications.
 * This function makes two passes through \p gallery.
 * The first pass checks for cases where two templates have the same
 * \ref roc_person_id but \p adjudications indicate they are different people,
 * and assigns \ref ROC_UNKNOWN_PERSON to the template with the higher index.
 * The second pass assigns the same \ref roc_person_id when \p adjudications
 * indicates they are the same person using the following rules:
 *  - If both templates are \ref ROC_UNKNOWN_PERSON, a new unique
 *    \ref roc_person_id is used.
 *  - The one template is \ref ROC_UNKNOWN_PERSON, the other \ref roc_person_id
 *    is used.
 *  - If neither template is \ref ROC_UNKNOWN_PERSON, the lower
 *    \ref roc_person_id is used.
 *
 * \param gallery Gallery to update identity information.
 * \param adjudications Array of ground truth identity information.
 * \param num_adjudications Length of \p adjudications.
 * \remark This function is \ref thread-safe.
 * \see \ref roc_cluster \ref roc_adjudicate_knn
 */
ROC_EXPORT roc_error roc_adjudicate_gallery(roc_gallery gallery,
                                            const roc_adjudication *adjudications,
                                            size_t num_adjudications);

/*!
 * \brief Extend a labeled gallery with additional templates based on
 *        similarity.
 *
 * For each template in \p probes, this function considers enrolling it to
 * \p gallery as follows. The mean similarity is computed between the probe and
 * each set of templates with the same \ref roc_person_id in \p gallery.
 * If the greatest mean similarity meets or exceeds \p threshold then the
 * template is enrolled to \p gallery with the associated \ref roc_person_id.
 *
 * If \p spawn is \c true, and the probe template does not meet the similarity
 * threshold, then the probe will be enrolled with a unique \ref roc_person_id
 * equal to one more than the maximum existing \ref roc_person_id in \p gallery.
 * Otherwise the probe won't be enrolled if it doesn't satisfy \p threshold.
 *
 * If the probe's roc_template::person_id does not equal \ref ROC_UNKNOWN_PERSON
 * then the probe will only be considered against the single set of templates in
 * \p gallery with the same \ref roc_person_id.
 *
 * Probes are considered one at a time, so that a later template in \p probes
 * may be assigned to the same \ref roc_person_id as an earlier probe that
 * spawned a new \ref roc_person_id.
 * Therefore, this function is not invariant to the order or templates in
 * \p probes.
 * Duplicate probes (those with a \ref roc_similarity of \c 1.0 to a template in
 * the gallery) will be ignored.
 *
 * Templates in \p gallery where roc_template::person_id equals
 * \ref ROC_UNKNOWN_PERSON will be ignored.
 *
 * This function is exposed in the \ref cli as \ref roc-consolidate.
 *
 * \par Example
 * \code{C}
 * roc_gallery gallery = ...;
 * roc_const_gallery probes = ...;
 * roc_extend(gallery, probes, 0.5, true);
 * \endcode
 *
 * \param[in,out] gallery The gallery to be extended by \p probes.
 * \param[in] probes The templates to consider adding to \p gallery.
 * \param[in] threshold The similarity required to add templates in \p probes to
 *                      \p gallery.
 * \param[in] spawn Add template as a new \ref roc_person_id if it does not
 *                  satisfy \p threshold.
 * \remark This function is \ref thread-safe.
 * \see \ref roc_cluster
 */
ROC_EXPORT roc_error roc_extend(roc_gallery gallery,
                                roc_const_gallery probes,
                                roc_similarity threshold,
                                bool spawn);

/*!
 * \brief Downsample a gallery by removing redundant templates for each
 *        \ref roc_template::person_id.
 *
 * For each set of templates with the same \ref roc_template::person_id in
 * \p gallery, this function greedily removes the "most redundant template"
 * until \em both of the following conditions are met:
 *  - The maximum pairwise similarity between all remaining templates is less
 *    than or equal to \p max_similarity. See \ref max_similarity.
 *  - The number of remaining templates is less than or equal to \p max_count.
 *    See \ref max_count.
 *
 * When searching for the "most redundant template" the pair of templates with
 * highest \ref roc_similarity is found, and in the pair the template with the
 * lesser \ref face_quality is removed.
 *
 * Templates with \ref roc_template::person_id == \ref ROC_UNKNOWN_PERSON in
 * \p gallery are retained automatically.
 * This function is generally called after \ref roc_cluster or \ref roc_track
 * to discard superfluous templates.
 *
 * This function is exposed in the \ref cli as \ref roc-consolidate.
 *
 * \note This function removes templates from \p gallery using \ref roc_remove.
 *       Therefore, it does not change the position or order of the templates in
 *       \p gallery.
 *
 * \section max_similarity Maximum Similarity
 * The maximum pairwise similarity between templates in a consolidated identity,
 * between zero and one inclusive.
 * After consolidation no two templates belonging to the same person will
 * have a similarity score greater than \p max_similarity.
 * The suggested default value for \p max_similarity is \c 0.9.
 *
 * \section max_count Maximum Count
 * The maximum number of templates in a consolidated identity, greater than
 * zero.
 * After consolidation no person will have more than \p max_count templates.
 * The suggested default value for \p max_count is \c 10.
 *
 * \par Example
 * \code{C}
 * roc_gallery gallery = ...;
 * roc_consolidate(gallery, 0.9, 10);
 * \endcode
 *
 * \param[in,out] gallery The gallery containing the persons to consolidate.
 * \param[in] max_similarity The maximum intra-person pairwise similarity, see
 *                           \ref max_similarity.
 * \param[in] max_count The maximum number of templates per person, see
 *                      \ref max_count.
 * \remark This function is \ref thread-safe.
 * \see \ref roc_consolidate_persons
 */
ROC_EXPORT roc_error roc_consolidate(roc_gallery gallery,
                                     roc_similarity max_similarity,
                                     size_t max_count);

/*!
 * \brief Downsample a gallery by removing redundant persons.
 *
 * For each set of templates with the same \ref roc_template::person_id in
 * \p gallery, this function greedily removes the "most redundant subject" until
 * the following condition is met:
 *  - The maximum pairwise similarity between all remaining subjects is less
 *    than or equal to \p max_similarity. The similarity between two subjects is
 *    the average similarity between their templates.
 *
 * When searching for the "most redundant subject" the pair of subjects with
 * highest average \ref roc_similarity is found, and in the pair the subject
 * with the smaller template count is removed.
 * If both subjects have the same template count, the one with the greater
 * \ref roc_person_id is removed.
 *
 * Templates with \ref roc_template::person_id == \ref ROC_UNKNOWN_PERSON in
 * \p gallery are retained automatically.
 *
 * This function is exposed in the \ref cli as \ref roc-consolidate.
 *
 * \note This function removes templates from \p gallery using \ref roc_remove.
 *       Therefore, it does not change the position or order of the templates in
 *       \p gallery.
 *
 * \par Example
 * \code{C}
 * roc_gallery gallery = ...;
 * roc_consolidate_persons(gallery, 0.9);
 * \endcode
 *
 * \param[in,out] gallery The gallery containing the persons to consolidate.
 * \param[in] max_similarity The maximum inter-person similarity.
 * \remark This function is \ref thread-safe.
 * \see \ref roc_consolidate
 */
ROC_EXPORT roc_error roc_consolidate_persons(roc_gallery gallery,
                                             roc_similarity max_similarity);

/*!
 * \brief Manually set a template's identity label.
 * \param[in] gallery The gallery containing the template for which to update
 *                    the \p person_id field.
 * \param[in] index Index of the template in \p gallery for which to update the
 *                  \p person_id field.
 * \param[in] person_id The new identity label for the template.
 * \remark This function is \ref thread-safe.
 * \see \ref roc_person_ids
 */
ROC_EXPORT roc_error roc_set_person_id(roc_gallery gallery,
                                       roc_template_index index,
                                       roc_person_id person_id);

/*!
 * \brief Retrieve the \ref roc_template::person_id for each template in a
 *        gallery.
 *
 * Used to obtain the results of a call to \ref roc_cluster, \ref roc_track,
 * \ref roc_consolidate.
 *
 * The \ref roc_person_id at index \c i in \p person_ids corresponds to the
 * <tt>i</tt>th template in \p gallery, retrievable by \ref roc_at.
 *
 * \par Example
 * \code{C}
 * roc_const_gallery gallery = ...;
 * size_t size;
 * roc_size(gallery, &size)
 * roc_person_id *person_ids = (roc_person_id*) malloc(size *
 *                                                     sizeof(roc_person_id));
 * roc_person_ids(gallery, person_ids);
 * \endcode
 *
 * \param[in] gallery The gallery to retrieve the \ref roc_template::person_id
 *                    list from.
 * \param[out] person_ids The \ref roc_template::person_id for each template in
 *                        \p gallery.
 * \remark This function is \ref thread-safe.
 * \see \ref roc_set_person_id
 */
ROC_EXPORT roc_error roc_person_ids(roc_const_gallery gallery,
                                    roc_person_id *person_ids);

/** @} */ // end of identity_discovery

/*!
 * \defgroup remote_galleries Remote Galleries
 * \brief Host a gallery file for remote access.
 *
 * Host a gallery file for remote access with \ref roc_serve.
 *
 * @{
 */

/*!
 * \brief Host a gallery file for remote access.
 *
 * When this function is executed with \p logging enabled, it will print
 * <tt>\<ip_address\>:\<port\></tt> with which to access the gallery file it
 * hosts remotely.
 * The gallery can be accessed remotely by using \ref roc_open_gallery with
 * <tt>\<ip_address\>:\<port\></tt> as the file name, and used in
 * \ref gallery_construction and \ref search functions.
 * When serving multiple galleries, each must be assigned to a unique port.
 *
 * Stop hosting the gallery with \ref roc_close_serve.
 *
 * This function is exposed in the \ref cli as \ref roc-serve.
 *
 * \section gallery_updating Gallery Updating
 * When \p update is enabled, a separate thread will monitor the parent folder
 * of the current gallery file for new gallery files ending with the <tt>.t</tt>
 * file extension.
 * When a new gallery file is discovered, a new gallery will be opened, and new
 * connections will automatically operate against it.
 * When all connections using the previous gallery are closed, the previous
 * gallery file will be deleted.
 *
 * \note Since it may take a while to copy the new gallery file, and this
 *       application may attempt to open it before the copy completes, users
 *       should first copy it elsewhere to the physical device, and then move it
 *       to the parent folder such that the transfer appears atomic.
 *
 * \par Example
 * \code{C}
 * roc_gallery_file gallery_file = ...;
 * roc_serve(gallery_file, 6688, false, true);
 * \endcode
 *
 * \param[in] gallery_file The gallery file to host for remote access.
 * \param[in] port Port to listen for incomming connections.
 * \param[in] update See \ref gallery_updating.
 * \param[in] logging Write connection information and usage to <tt>stdout</tt>.
 * \param[in] asynchronous If \c true, this function call will return
 *                         immediately after starting the server.
 *                         Otherwise this function call will not return until
 *                         \ref roc_close_serve is called.
 * \remark This function is \ref thread-safe.
 * \see \ref roc_close_serve
 */
ROC_EXPORT roc_error roc_serve(roc_gallery_file gallery_file,
                               int port,
                               bool update,
                               bool logging,
                               bool asynchronous);

/*!
 * \brief Stop hosting a gallery file for remote access.
 *
 * \par Example
 * \code{C}
 * roc_gallery_file gallery_file = ...;
 * roc_close_serve(gallery_file);
 * \endcode
 *
 * \param[in] gallery_file Gallery file to stop hosting.
 * \remark This function is \ref thread-safe.
 * \see \ref roc_serve
 */
ROC_EXPORT roc_error roc_close_serve(roc_gallery_file gallery_file);

/** @} */ // end of remote_galleries

#endif // !SWIG_ROC_VIDEO

#if defined(TARGET_OS_IPHONE) && TARGET_OS_IPHONE
#pragma clang pop
#endif // defined(TARGET_OS_IPHONE) && TARGET_OS_IPHONE

#ifdef __cplusplus
}
#endif

#endif // ROC_H
