//
//  RocWrapper.h
//  CBPAirEntry
//
//  Created by Kannan Seenivasagam on 6/21/19.
//  Copyright © 2019 Kannan Seenivasagam. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "roc.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RocWrapper : NSObject

+ (NSString *)loadRocLicense;
+ (float) verify:(const uint8_t *)tobecompared original:(const uint8_t *)original tobeimagelen:(size_t) tobeimagelen origimagelen:(size_t) origimagelen;
+ (NSDictionary *) faceCompare:(const uint8_t *)tobecompared original:(const uint8_t *)original tobeimagelen:(size_t) tobeimagelen origimagelen:(size_t) origimagelen;
+ (NSDictionary *) faceMetaData:(const uint8_t *)original origimagelen:(size_t) origimagelen;

@end

NS_ASSUME_NONNULL_END
