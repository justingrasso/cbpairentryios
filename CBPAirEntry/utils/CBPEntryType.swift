//
//  CBPEntryType.swift
//  CBPAirEntry
//
//  Created by Kannan Seenivasagam on 6/16/19.
//  Copyright © 2019 Kannan Seenivasagam. All rights reserved.
//

import Foundation

public enum CBPEntryType: String {
    case AIR = "AIR"
    case LAND = "LAND"
    case SEA = "SEA"
}
