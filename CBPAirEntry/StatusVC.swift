//
//  StatusVC.swift
//  CBPAirEntry
//
//  Created by Kannan Seenivasagam on 6/18/19.
//  Copyright © 2019 Kannan Seenivasagam. All rights reserved.
//

import UIKit
import SwiftyGif

class StatusVC: UIViewController {

    var statusType: String?
    var unwindTimer: Timer!
    var counter:Int?
    var globalMessage: String?
    
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var statusImgView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let width = UIScreen.main.bounds.size.width
        let height = UIScreen.main.bounds.size.height
        
        let imageViewBackground = UIImageView(frame: CGRect(x:0, y:0, width: width, height: height))
        imageViewBackground.image = UIImage(named: "bluebackground.png")
        
        // you can change the content mode:
        imageViewBackground.contentMode = UIView.ContentMode.scaleAspectFill
        
        self.view.addSubview(imageViewBackground)
        self.view.sendSubviewToBack(imageViewBackground)
        debugPrint("Global Message::",self.globalMessage);
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        if(statusType == "MATCH"){
            self.statusImgView.image = UIImage(named: "match");
            self.statusLbl.text = self.globalMessage;
            counter = 0;
            unwindTimer = Timer.scheduledTimer(timeInterval: 0.9, target: self, selector: #selector(unvindStatusView), userInfo: nil, repeats: true);
        }else if(statusType == "NOMATCH"){
            self.statusLbl.text = self.globalMessage;
            self.statusImgView.image = UIImage(named: "retake1");
            counter = 0;
            unwindTimer = Timer.scheduledTimer(timeInterval: 0.25, target: self, selector: #selector(unvindStatusView), userInfo: nil, repeats: true);
        }else if(statusType == "STOP_CAPTURE"){
            self.statusLbl.text = self.globalMessage;
            self.statusImgView.image = UIImage(named: "match");
            counter = 0;
            unwindTimer = Timer.scheduledTimer(timeInterval: 0.9, target: self, selector: #selector(unvindStatusView), userInfo: nil, repeats: true);
        }else if(statusType == "EYEGLASS"){
            
            var gif:UIImage
            do{
                gif = try UIImage(gifName: "anitest")
                self.statusImgView.setGifImage(gif)
            } catch {
                // Catch any other errors
                debugPrint("Grasso: can't read gif")
            }
            //self.statusImgView.image = UIImage(named: "glassesoff");
            counter = 0;
            unwindTimer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(unvindStatusView), userInfo: nil, repeats: true);
        }else if(statusType == "TAKEN"){
            self.statusImgView.image = UIImage(named: "match");
            self.statusLbl.text = "Photo Already Taken";
            counter = 0;
            unwindTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(unvindStatusView), userInfo: nil, repeats: true);
        }else if(statusType == "CLOSER"){
            self.statusImgView.image = UIImage(named: "MoveCloser");
            self.statusLbl.text = "Move Closer";
            counter = 0;
            unwindTimer = Timer.scheduledTimer(timeInterval: 0.9, target: self, selector: #selector(unvindStatusView), userInfo: nil, repeats: true);
        }else if(statusType == "RETAKE"){
            self.statusImgView.image = UIImage(named: "retake");
            counter = 0;
            unwindTimer = Timer.scheduledTimer(timeInterval: 0.25, target: self, selector: #selector(unvindStatusView), userInfo: nil, repeats: true);
        }
        
        
        
    }
   @objc func unvindStatusView(){
    if(counter == 0){
        counter = 1;
    }else{
        dismiss(animated: true, completion: nil);
    }
   }
   override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated);
    if(unwindTimer != nil) {
        debugPrint("Unwind Timer Invalidated....");
        unwindTimer.invalidate();
    }
   }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
